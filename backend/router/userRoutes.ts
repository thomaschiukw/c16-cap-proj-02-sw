import express from "express";
import { authController, userController } from "../server";

const userRoutes = express.Router();

userRoutes.get("/users", (req, res) => userController.getUsers(req, res));
userRoutes.get("/user/:id", (req, res) => userController.getUserByUserId(req, res));
userRoutes.get("/user/driver/:id/session-details", (req, res) => userController.getDriverSessionDetails(req, res));
userRoutes.get("/user/oncall-drivers/location", (req, res) => userController.getDriversLocation(req, res))
userRoutes.get("/user/onduty-driver/:id/location", (req, res) => userController.getDriverLocationBySessionId(req, res))
userRoutes.post("/login", (req, res) => authController.login(req, res));
userRoutes.post("/register", (req, res) => userController.register(req, res));
userRoutes.post("/register/driver", (req, res) => userController.driverRegister(req, res));
userRoutes.put("/user", (req, res) => userController.editUser(req, res));
userRoutes.put("/user/location", (req, res) => userController.changeDriverLocationByUserId(req, res));

export default userRoutes;