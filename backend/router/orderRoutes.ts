import express from "express";
import { orderController } from "../server";

const orderRoutes = express.Router();

orderRoutes.get("/order", (req, res) => orderController.getOrder(req, res));
orderRoutes.get("/order/:id", (req, res) => orderController.getOrderById(req, res));
// orderRoutes.get("/order/:id/searching", (req, res) => orderController.getSearchingOrderByOrderId(req, res));
orderRoutes.get("/order/active/user/:id", (req, res) => orderController.getActiveOrderByUserId(req, res));
orderRoutes.get("/order/searching/type/:type/capacity/:capacity/allowPet/:allowPet/disallowSmoke/:disallowSmoke", (req, res) => {
    orderController.getSearchingOrder(req, res);
});
orderRoutes.get("/order/pending/driver/:id", (req, res) => orderController.getDriverPendingOrder(req, res));
orderRoutes.get("/order/active/driver/:id", (req, res) => orderController.getActiveOrderByDriverId(req, res));
orderRoutes.get("/order/completed/user/:id", (req, res) => orderController.getUserHistoryOrder(req, res));
orderRoutes.get("/order/completed/driver/:id", (req, res) => orderController.getDriverHistoryOrder(req, res));

orderRoutes.post("/order", (req, res) => orderController.createOrder(req, res));

orderRoutes.put("/order/accept/driver/:driverid/order/:orderid", (req, res) => orderController.acceptOrder(req, res));
orderRoutes.put("/order/start/order/:id", (req, res) => orderController.startOrder(req, res));
orderRoutes.put("/order/complete/order/:id", (req, res) => orderController.completeOrder(req, res));

export default orderRoutes;