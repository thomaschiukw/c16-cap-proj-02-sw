import express from "express";
import { mapController } from "../server";

const mapRoutes = express.Router();

mapRoutes.get("/autocomplete/input/:input", (req, res) => mapController.autocomplete(req, res));
mapRoutes.get("/placeid/input/:input", (req, res) => mapController.retrievePlaceDetailsByPlaceId(req, res));

export default mapRoutes;