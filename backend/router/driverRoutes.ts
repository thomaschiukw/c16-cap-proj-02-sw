import express from "express";
import { driverController } from "../server";

const driveRoutes = express.Router();

// driveRoutes.get("/driver", (req, res) => {
//     console.log("[Routing] Call GET /driver");
//     driverController.getDriver(req, res);
// });

driveRoutes.get("/driver/:user_id", (req, res) => {
    console.log("[Routing] Call GET /driver id");
    driverController.getDriverByUserId(req, res);
});

driveRoutes.post("/driver/:user_id", (req, res) => {
    console.log("[Routing] Call POST /driver id");
    driverController.postDriverByUserId(req, res);
});

driveRoutes.post("/activate-session/:user_id", (req, res) => {
    console.log("[Routing] Activate-session POST /driver id");
    driverController.postActiveSession(req, res);
});

driveRoutes.post("/deactivate-session/:user_id", (req, res) => {
    console.log("[Routing] Deactivate-session POST /driver id");
    driverController.postDeactiveSession(req, res);
});

export default driveRoutes;