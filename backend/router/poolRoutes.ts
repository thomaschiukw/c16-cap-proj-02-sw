import express from "express";
import { poolController } from "../server";

const poolRoutes = express.Router();

poolRoutes.get("/pool-list/:id", (req, res) => {
    poolController.getPoolList(req, res);
});

poolRoutes.post("/join-pool/", (req, res) => {
    poolController.postJoinPool(req, res);
});


poolRoutes.get("/current-pool/:id", (req, res) => {
    poolController.getCurrentPool(req, res);
});

export default poolRoutes;