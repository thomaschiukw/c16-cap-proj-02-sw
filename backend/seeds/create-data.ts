import { Knex } from "knex";
import { hashPassword } from '../hash';
// import { add } from 'date-fns'
// import { OrderStatusEnum } from "../Enums/orderStatusEnum";

// const Chance = require('chance');
// const chance = new Chance();

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("users_orders").del();
    await knex("orders").del();
    await knex("users_lat_long").del();
    await knex("driver_sessions").del();
    await knex("driver_details").del();
    await knex("users").del();

    // Inserts seed entries
    let users: any = [];
    let driverSessions: any = [];
    let driverDetails: any = [];
    // let locations: any = [];
    // let orders: any = [];

    users.push(
        {
            username: 'admin',
            email: 'admin@admin.com',
            password: await hashPassword("admin"),
            phone: 12345678,
            is_driver: false
        },
        {
            username: 'user',
            email: 'user@user.com',
            password: await hashPassword("user"),
            phone: 99999999,
            is_driver: false
        },
        {
            username: 'driver221',
            email: 'driver@tecky.io',
            password: await hashPassword("driver"),
            phone: 87654321,
            is_driver: true
        },
        {
            username: 'imdriver',
            email: 'imdrivers@tecky.io',
            password: await hashPassword("driver"),
            phone: 87654321,
            is_driver: true
        },
        {
            username: 'realdriver',
            email: 'imdrdedeivers@tecky.io',
            password: await hashPassword("driver"),
            phone: 87654321,
            is_driver: true
        }


    )


    // for (let i = 0; i < 10; i++) {
    //     users.push({
    //         username: `realdriver${i}`,
    //         email: 'imdrdedeivers@tecky.io',
    //         password: await hashPassword("driver"),
    //         phone: 87654321,
    //         is_driver: true
    //     });
    // }
    for (let i = 0; i < 10; i++) {
        users.push({
            username: `realdriver${i}`,
            email: 'imdrdedeivers@tecky.io',
            password: await hashPassword("driver"),
            phone: 87654321,
            is_driver: true
        });
    }

    users.push(
        {
            username: 'kylemak',
            email: 'kylemak@tecky.io',
            password: await hashPassword("test1234"),
            phone: 91234521,
            is_driver: false
        },
        {
            username: 'thomaschiu',
            email: 'thomaschiu@tecky.io',
            password: await hashPassword("test1234"),
            phone: 53814841,
            is_driver: false
        },
        {
            username: 'chantaiman',
            email: 'chantaiman@tecky.io',
            password: await hashPassword("test1234"),
            phone: 90888987,
            is_driver: true
        },
        {
            username: 'chriswong',
            email: 'chriswong@tecky.io',
            password: await hashPassword("test1234"),
            phone: 98333333,
            is_driver: true
        }
    )
    const user_id = await knex.insert(users).into("users").returning('id');

    await knex.insert([
        {
            user_id: user_id[0],
            //sogo causewaybay
            //originCoordinate(pin):{lat:22.28042719999999,lng:114.1843032}
            lat: 22.28042719999999,
            long: 114.1843032
        },
        {
            user_id: user_id[1],
            lat: 22.2879098,
            long: 114.1517914
        },
        {
            user_id: user_id[2],
            lat: 22.3279098,
            long: 114.1817914
        },
        {
            user_id: user_id[3],
            lat: 22.3179098,
            long: 114.1717914
        },
        {
            user_id: user_id[4],
            lat: 22.3079098,
            long: 114.1617914
        },
        {
            user_id: user_id[5],
            lat: 22.2979098,
            long: 114.1517914
        },
        {
            user_id: user_id[6],
            lat: 22.3879098,
            long: 114.0917914
        }

    ]).into('users_lat_long')

    driverDetails.push(
        {
            user_id: user_id[2],
            name: 'dai lo',
            license: '123456',
            pet_ok: false,
            smoke_free: false,
        },
        {
            user_id: user_id[3],
            name: 'driver lee',
            license: '555555',
            pet_ok: true,
            smoke_free: true,
        },
        {
            user_id: user_id[4],
            name: '黃一心',
            license: '212121',
            pet_ok: true,
            smoke_free: true,
        }
        // ,
        // {
        //     user_id: user_id[5],
        //     name: 'bbbbb',
        //     license: '212121',
        //     pet_ok: true,
        //     smoke_free: true,
        // },
        // {
        //     user_id: user_id[6],
        //     name: 'ccccc',
        //     license: '212121',
        //     pet_ok: true,
        //     smoke_free: true,
        // }
    )

    for (let i = 0; i < 10; i++) {
        driverDetails.push({
            user_id: user_id[i + 5],
            name: `driver${i}`,
            license: `${i}12345`,
            pet_ok: true,
            smoke_free: true,

        });
    }

    driverDetails.push(
        {
            user_id: user_id[17],
            name: '陳大文',
            license: '123456',
            pet_ok: false,
            smoke_free: false,
        },
        {
            user_id: user_id[18],
            name: '黃一心',
            license: '555555',
            pet_ok: true,
            smoke_free: true,
        })
    // const driver_details_id =
    await knex.insert(driverDetails).into("driver_details").returning('id');

    driverSessions.push({
        user_id: user_id[2],
        type: 'red',
        hybrid: false,
        capacity: 4,
        rego: 'AB1234',
        active_session: true
    }, {
        user_id: user_id[3],
        type: 'green',
        hybrid: true,
        capacity: 5,
        rego: 'ZZ2222',
        active_session: false
    }, {
        user_id: user_id[4],
        type: 'red',
        hybrid: true,
        capacity: 5,
        rego: 'YYAKA',
        active_session: true
    });


    for (let i = 0; i < 10; i++) {
        driverSessions.push({
            user_id: user_id[i + 5],
            type: 'red',
            hybrid: true,
            capacity: 5,
            rego: 'YYAKA',
            active_session: true

        });
    }

    driverSessions.push({
        user_id: user_id[17],
        type: 'red',
        hybrid: false,
        capacity: 4,
        rego: 'AB1234',
        active_session: false
    }, {
        user_id: user_id[18],
        type: 'green',
        hybrid: true,
        capacity: 5,
        rego: 'ZZ2222',
        active_session: false
    })
    // , {
    //     user_id: user_id[5],
    //     type: 'red',
    //     hybrid: true,
    //     capacity: 5,
    //     rego: 'YYAKA',
    //     active_session: true
    // }, {
    //     user_id: user_id[6],
    //     type: 'red',
    //     hybrid: true,
    //     capacity: 5,
    //     rego: 'YYAKA',
    //     active_session: true
    // }


    // const driver_session_id = 
    await knex.insert(driverSessions).into("driver_sessions").returning('id');

    // console.log(driver_session_id);

    // orders.push({
    //     origin_place_id: 'ChIJx5i4buAABDQR_EkaiFTqjqE',
    //     origin_lat: 22.3047355,
    //     origin_lng: 114.1902873,
    //     origin_district: '黃埔花園',
    //     origin_description: '香港黃埔花園德安街黃埔花園第六期',
    //     destination_place_id: 'ChIJ-ZuOlFYABDQRT4Pk8HeffpM',
    //     destination_lat: 22.28042719999999,
    //     destination_lng: 114.1843032,
    //     destination_district: 'SOGO銅鑼灣',
    //     destination_description: '香港銅鑼灣SOGO',
    //     // capacity: 5,
    //     pool: false,
    //     hybrid: false,
    //     red_taxi: true,
    //     green_taxi: false,
    //     blue_taxi: false,
    //     cross_harbour_tunnel: true,
    //     eastern_harbour_crossing: false,
    //     western_harbour_crossing: false,
    //     has_pet: false,
    //     smoke_free: false,
    //     has_luggage: false,
    //     is_disabled: false,
    //     urgent: false,
    //     status: OrderStatusEnum.SEARCHING
    // }, {
    //     origin_place_id: 'ChIJ-ZuOlFYABDQRT4Pk8HeffpM',
    //     origin_lat: 22.28042719999999,
    //     origin_lng: 114.1843032,
    //     origin_district: 'SOGO銅鑼灣',
    //     origin_description: '香港銅鑼灣SOGO',
    //     destination_place_id: 'ChIJx5i4buAABDQR_EkaiFTqjqE',
    //     destination_lat: 22.3047355,
    //     destination_lng: 114.1902873,
    //     destination_district: '黃埔花園',
    //     destination_description: '香港黃埔花園德安街黃埔花園第六期',
    //     driver_session_id: driver_session_id[0],
    //     fare: 343,
    //     distance: 123.4,
    //     pool: true,
    //     pool_deadline: add(new Date(), { minutes: 10 }),

    //     hybrid: false,
    //     red_taxi: false,
    //     green_taxi: true,
    //     blue_taxi: false,
    //     cross_harbour_tunnel: true,
    //     eastern_harbour_crossing: false,
    //     western_harbour_crossing: false,
    //     has_pet: false,
    //     smoke_free: false,
    //     has_luggage: false,
    //     is_disabled: false,
    //     urgent: false,
    //     status: OrderStatusEnum.EXPIRED
    // }, {
    //     origin_place_id: 'ChIJx5i4buAABDQR_EkaiFTqjqE',
    //     origin_lat: 22.3047355,
    //     origin_lng: 114.1902873,
    //     origin_district: '黃埔花園',
    //     origin_description: '香港黃埔花園德安街黃埔花園第六期',
    //     destination_place_id: 'ChIJ-ZuOlFYABDQRT4Pk8HeffpM',
    //     destination_lat: 22.28042719999999,
    //     destination_lng: 114.1843032,
    //     destination_district: 'SOGO銅鑼灣',
    //     destination_description: '香港銅鑼灣SOGO',
    //     driver_session_id: driver_session_id[0],
    //     fare: 80.4,
    //     distance: 130.1,
    //     // capacity: 4,
    //     pool: true,
    //     pool_deadline: add(new Date(), { minutes: 10 }),
    //     hybrid: false,
    //     red_taxi: true,
    //     green_taxi: false,
    //     blue_taxi: false,
    //     cross_harbour_tunnel: false,
    //     eastern_harbour_crossing: false,
    //     western_harbour_crossing: false,
    //     has_pet: false,
    //     smoke_free: false,
    //     has_luggage: false,
    //     is_disabled: true,
    //     urgent: false,
    //     status: OrderStatusEnum.PENDING
    // }, {
    //     origin_place_id: 'ChIJx5i4buAABDQR_EkaiFTqjqE',
    //     origin_lat: 22.3047355,
    //     origin_lng: 114.1902873,
    //     origin_district: '黃埔花園',
    //     origin_description: '香港黃埔花園德安街黃埔花園第六期',
    //     destination_place_id: 'ChIJ-ZuOlFYABDQRT4Pk8HeffpM',
    //     destination_lat: 22.28042719999999,
    //     destination_lng: 114.1843032,
    //     destination_district: 'SOGO銅鑼灣',
    //     destination_description: '香港銅鑼灣SOGO',
    //     driver_session_id: driver_session_id[0],
    //     fare: 80.4,
    //     distance: 130.1,
    //     // capacity: 4,
    //     pool: true,
    //     pool_deadline: add(new Date(), { minutes: 10 }),
    //     hybrid: false,
    //     red_taxi: true,
    //     green_taxi: false,
    //     blue_taxi: false,
    //     cross_harbour_tunnel: false,
    //     eastern_harbour_crossing: false,
    //     western_harbour_crossing: false,
    //     has_pet: false,
    //     smoke_free: false,
    //     has_luggage: false,
    //     is_disabled: false,
    //     urgent: false,
    //     status: OrderStatusEnum.COMPLETE
    // })
    // const order_id = await knex.insert(orders).into("orders").returning('id');

    // await knex.insert([{
    //     order_id: order_id[0],
    //     user_id: user_id[0],
    //     headcount: 2,
    //     rating_polite: 5,
    //     rating_clean: 4,
    //     rating_punctual: 3,
    //     rating_driving: 4
    // }, {
    //     order_id: order_id[1],
    //     user_id: user_id[0],
    //     headcount: 1,
    //     rating_polite: 5,
    //     rating_clean: 4,
    //     rating_punctual: 3,
    //     rating_driving: 4,
    //     comment: 'good'
    // }, {
    //     order_id: order_id[1],
    //     user_id: user_id[1],
    //     headcount: 2,
    //     rating_polite: 5,
    //     rating_clean: 4,
    //     rating_punctual: 3,
    //     rating_driving: 4
    // }, {
    //     order_id: order_id[2],
    //     user_id: user_id[0],
    //     headcount: 2,
    //     rating_polite: 5,
    //     rating_clean: 4,
    //     rating_punctual: 3,
    //     rating_driving: 4
    // }, {
    //     order_id: order_id[3],
    //     user_id: user_id[0],
    //     headcount: 2,
    //     rating_polite: 5,
    //     rating_clean: 4,
    //     rating_punctual: 3,
    //     rating_driving: 4
    // }]).into('users_orders');
};
