export enum OrderStatusEnum {
    SEARCHING = 'searching',
    PENDING = 'pending',
    TRAVELLING = 'travelling',
    COMPLETE = 'complete',
    EXPIRED = 'expired'
}