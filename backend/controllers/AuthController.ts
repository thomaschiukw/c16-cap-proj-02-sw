import { Request, Response } from 'express';
import { UserService } from '../services/UserService';
// import { checkPassword } from '../hash';
// import jwtSimple from 'jwt-simple';
// import jwt from '../jwt';

export class AuthController {
    constructor(private userService: UserService) { }

    // login = async (req: Request, res: Response) => {
    //     try {
    //         if (!req.body.username || !req.body.password) {
    //             return res.status(400).json({ error: 'No username or password' });
    //         }
    //         const user = await this.userService.getUserByUsername(req.body.username);

    //         if (user[0] == null) {
    //             return res.status(401).json({ error: 'Invalid username or password' });
    //         }
    //         if (!await checkPassword(req.body.password, user[0].password)) {
    //             return res.status(401).json({ error: 'Invalid username or password' });
    //         }

    //         const payload = {
    //             id: user[0].id
    //         };

    //         const token = jwtSimple.encode(payload, jwt.jwtSecret);

    //         return res.json({ token: token });
    //     } catch (error) {
    //         console.error(error);
    //         return res.status(500).json({ error: 'Unknown error' });
    //     }
    // }

    login = async (req: Request, res: Response) => {
        try {
            if (!req.body.username || !req.body.password) {
                return res.status(400).json({ error: 'No username or password' });
            }

            const result = await this.userService.login(req.body.username, req.body.password);
            if (result.token) {
                return res.json(result);
            } else {
                return res.status(401).json(result);
            }
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Unknown error' });
        }
    }
}