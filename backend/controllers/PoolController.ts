import { Request, Response } from 'express';
import { PoolService } from '../services/PoolService';
// import { io } from '../socketio';

export class PoolController {
    constructor(private poolService: PoolService) { }



    getPoolList = async (req: Request, res: Response) => {

        const { id } = req.params

        const result = await this.poolService.getPoolList(parseInt(id));
        return res.json(result);
    }

    postJoinPool = async (req: Request, res: Response) => {

        const { userId, orderId, headcount } = req.body

        const result = await this.poolService.postJoinPool(parseInt(userId), parseInt(orderId), parseInt(headcount));
        return res.json(result);
    }

    getCurrentPool = async (req: Request, res: Response) => {

        const { id } = req.params

        const result = await this.poolService.getCurrentPool(parseInt(id));
        return res.json(result);
    }

}