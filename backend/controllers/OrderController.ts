import { Request, Response } from 'express';
import { OrderService } from '../services/OrderService';
import { io } from '../socketio';

export class OrderController {
    constructor(private orderService: OrderService) { }

    getOrder = async (req: Request, res: Response) => {
        const result = await this.orderService.getOrders();
        return res.json(result);
    }

    getOrderById = async (req: Request, res: Response) => {
        const { id } = req.params;
        const result = await this.orderService.getOrderById(parseInt(id))
        return res.json(result);
    }

    // getSearchingOrderByOrderId = async (req: Request, res: Response) => {
    //     const { id } = req.params;
    //     const result = await this.orderService.getOrderById(parseInt(id))
    //     return res.json(result);
    // }

    getActiveOrderByUserId = async (req: Request, res: Response) => {
        const { id } = req.params;

        const result = await this.orderService.getActiveOrderByUserId(parseInt(id));

        if (result) {
            return res.json(result);
        } else {
            return res.status(401).json(result);
        }
    }

    getSearchingOrder = async (req: Request, res: Response) => {
        const { type, capacity, allowPet, disallowSmoke } = req.params;
        // console.table(req.params);

        let allowPetBoo;
        if (allowPet === "true") {
            allowPetBoo = true;
        } else {
            allowPetBoo = false;
        }

        let disallowSmokeBoo;
        if (disallowSmoke === "true") {
            disallowSmokeBoo = true;
        } else {
            disallowSmokeBoo = false;
        }

        const result = await this.orderService.getSearchingOrder(type, parseInt(capacity), allowPetBoo, disallowSmokeBoo);
        if (result) {
            return res.json(result);
        } else {
            return res.status(401).json(result);
        }
    }

    getDriverPendingOrder = async (req: Request, res: Response) => {
        const { id } = req.params;
        const result = await this.orderService.getDriverPendingOrder(parseInt(id));
        return res.json(result);
    }

    getActiveOrderByDriverId = async (req: Request, res: Response) => {
        const { id } = req.params;
        const result = await this.orderService.getActiveOrderByDriverId(parseInt(id));
        return res.json(result);
    }

    acceptOrder = async (req: Request, res: Response) => {
        const driverId = req.params.driverid;
        const orderId = req.params.orderid;

        const result = await this.orderService.acceptOrder(parseInt(driverId), parseInt(orderId))
        io.emit("order-accepted");

        return res.json(result);
    }

    createOrder = async (req: Request, res: Response) => {
        try {
            const {
                origin_place_id,
                origin_lat,
                origin_lng,
                origin_district,
                origin_description,
                destination_place_id,
                destination_lat,
                destination_lng,
                destination_district,
                destination_description,
                fare,
                distance,
                pool,
                hybrid,
                red_taxi,
                green_taxi,
                blue_taxi,
                cross_harbour_tunnel,
                eastern_harbour_crossing,
                western_harbour_crossing,
                has_pet,
                smoke_free,
                has_luggage,
                is_disabled,
                urgent,
                headcount,
                user_id
            } = req.body;

            const result = await this.orderService.createOrder(
                origin_place_id,
                origin_lat,
                origin_lng,
                origin_district,
                origin_description,
                destination_place_id,
                destination_lat,
                destination_lng,
                destination_district,
                destination_description,
                fare,
                distance,
                pool,
                hybrid,
                red_taxi,
                green_taxi,
                blue_taxi,
                cross_harbour_tunnel,
                eastern_harbour_crossing,
                western_harbour_crossing,
                has_pet,
                smoke_free,
                has_luggage,
                is_disabled,
                urgent,
                headcount,
                user_id
            );

            if (result.success) {
                io.emit("order-created");
                return res.json(result);
            } else {
                return res.status(401).json(result);
            }
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Unknown error' });
        }
    }

    startOrder = async (req: Request, res: Response) => {
        const orderId = req.params.id;

        const result = await this.orderService.startOrder(parseInt(orderId))
        io.emit("order-started");

        return res.json(result);
    }

    completeOrder = async (req: Request, res: Response) => {
        const orderId = req.params.id;

        const result = await this.orderService.completeOrder(parseInt(orderId));
        io.emit("order-completed");

        return res.json(result);
    }

    getUserHistoryOrder = async (req: Request, res: Response) => {
        const { id } = req.params

        const result = await this.orderService.getUserHistoryOrder(parseInt(id));

        return res.json(result);
    }

    getDriverHistoryOrder = async (req: Request, res: Response) => {
        const { id } = req.params

        const result = await this.orderService.getDriverHistoryOrder(parseInt(id));

        return res.json(result);
    }

    // getPoolList = async (req: Request, res: Response) => {

    //     const { id } = req.params

    //     const result = await this.orderService.getPoolList(parseInt(id));
    //     return res.json(result);
    // }
}