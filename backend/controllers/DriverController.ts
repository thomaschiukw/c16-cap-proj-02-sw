import { Request, Response } from 'express';
import { DriverService } from '../services/DriverService';
import { io } from '../socketio';

export class DriverController {
    constructor(private driverService: DriverService) { }

    // getDriver = async (req: Request, res: Response) => {
    //     const result = await this.driverService.getDriver()
    //     return res.json(result);
    // }

    getDriverByUserId = async (req: Request, res: Response) => {
        const { user_id } = req.params;
        const parsedINTuser_id = parseInt(user_id)
        const result = await this.driverService.getDriverByUserId(parsedINTuser_id)
        return res.json(result);
    }

    postDriverByUserId = async (req: Request, res: Response) => {
        try {
            const {
                userID,
                email,
                phone,
                name,
                license,
                pet_ok,
                smoke_free,
                isRedTaxi,
                isGreenTaxi,
                isBlueTaxi,
                hybrid,
                capacity,
                rego, } = req.body;

            const result = await this.driverService.postDriverByUserId(parseInt(userID), email,
                parseInt(phone), name, license, pet_ok, smoke_free, isRedTaxi, isGreenTaxi, isBlueTaxi, hybrid, parseInt(capacity), rego)

            return res.json(result);

        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Unknown error' });
        }
    }

    postActiveSession = async (req: Request, res: Response) => {
        try {
            const { user_id } = req.params
            const {
                isRedTaxi,
                isGreenTaxi,
                isBlueTaxi,
                hybrid,
                capacity,
                rego } = req.body;
                console.table(req.body)

            const result = await this.driverService.postActiveSession(parseInt(user_id), isRedTaxi, isGreenTaxi, isBlueTaxi, hybrid, parseInt(capacity), rego)

            io.emit("session-activated");
            return res.json(result);

        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Unknown error' });
        }
    }

    postDeactiveSession = async (req: Request, res: Response) => {
        try {
            const { user_id } = req.params

            const result = await this.driverService.postDeactiveSession(parseInt(user_id))

            io.emit("session-deactivated");
            return res.json(result);

        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Unknown error' });
        }
    }
}