import { Request, Response } from 'express';
import axios from 'axios';
import { env } from '../env';

export class MapController {
    constructor() { }

    autocomplete = async (req: Request, res: Response) => {
        interface Autocomplete {
            predictions: [];
            status: string | "OK";
        }

        try {
            const input = req.params.input;
            // console.log('input =', input);

            const fetchRes = await axios.get<Autocomplete>(encodeURI(`https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${input}&components=country:hk&language=zh-HK&key=${env.GOOGLE_MAPS_API_KEY}`));
            const result = fetchRes.data;
            if (result.status === "OK") {
                return res.json(result);
            } else {
                return res.status(400).json(result)
            }
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Unknown error' });
        }

    }

    retrievePlaceDetailsByPlaceId = async (req: Request, res: Response) => {
        interface PlaceId {
            html_attributions: []
            result: [];
            status: string;
        }

        try {
            const input = req.params.input;
            // console.log('input =', input);

            const fetchRes = await axios.get<PlaceId>(encodeURI(`https://maps.googleapis.com/maps/api/place/details/json?place_id=${input}&language=zh-HK&key=${env.GOOGLE_MAPS_API_KEY}`));
            const result = fetchRes.data;
            // console.log(result.result["address_components"]);
            
            if (result.status === "OK") {
                return res.json(result);
            } else {
                return res.status(400).json(result)
            }
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Unknown error' });
        }

    }
}