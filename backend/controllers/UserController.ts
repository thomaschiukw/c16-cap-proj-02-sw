import { Request, Response } from 'express';
import { UserService } from '../services/UserService';
import { io } from '../socketio';

export class UserController {
    constructor(private userService: UserService) { }

    getUsers = async (req: Request, res: Response) => {
        const result = await this.userService.getUsers()
        return res.json(result);
    }

    getUserByUserId = async (req: Request, res: Response) => {
        const { id } = req.params;
        const parsedINTuser_id = parseInt(id)
        const result = await this.userService.getUserById(parsedINTuser_id)
        return res.json(result);
    }

    editUser = async (req: Request, res: Response) => {
        try {
            const { userID, username, email, phone } = req.body;
            console.log(req.body);

            const result = await this.userService.editUser(parseInt(userID), username, email, parseInt(phone))

            return res.json(result);
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Unknown error' });
        }
    }

    getDriverSessionDetails = async (req: Request, res: Response) => {
        try {
            const { id } = req.params;
            const result = await this.userService.getDriverSessionDetails(parseInt(id));

            if (result) {
                return res.json(result);
            } else {
                return res.status(401).json(result);
            }
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Unknown error' });
        }
    }

    getDriversLocation = async (req: Request, res: Response) => {
        const result = await this.userService.getDriversLocation();
        return res.json(result);
    }

    getDriverLocationBySessionId = async (req: Request, res: Response) => {
        const { id } = req.params;
        const result = await this.userService.getDriverLocationBySessionId(parseInt(id));
        return res.json(result);
    }

    changeDriverLocationByUserId = async (req: Request, res: Response) => {
        const { id, lat, lng } = req.body;
        const result = await this.userService.changeDriverLocationByUserId(parseInt(id), parseFloat(lat), parseFloat(lng));
        io.emit("driver-location-changed");
        return res.json(result);
    }

    register = async (req: Request, res: Response) => {
        try {
            const { username, email, password, phone } = req.body;

            const result = await this.userService.register(username, email, password, parseInt(phone));
            if (result.success) {
                return res.json(result);
            } else {
                return res.status(401).json(result);
            }
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Unknown error' });
        }
    }

    driverRegister = async (req: Request, res: Response) => {
        try {
            const { driverName, license, username, email, password, phone, isPetOk, isSmokeFree } = req.body;
            const result = await this.userService.driverRegister(driverName, license, username, email, password, parseInt(phone), isPetOk, isSmokeFree);
            if (result.success) {
                return res.json(result);
            } else {
                return res.status(401).json(result);
            }
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Unknown error' });
        }
    }
}