import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('driver_sessions');
    if (!hasTable) {
        await knex.schema.createTable('driver_sessions', (table) => {
            table.increments();
            table.integer("user_id").unsigned().notNullable();
            table.foreign('user_id').references('users.id');
            table.string("type").notNullable();
            table.boolean('hybrid').notNullable();
            table.integer("capacity").notNullable();
            table.string("rego").notNullable();
            table.boolean('active_session').notNullable();
            // table.float("lat");
            // table.float("long");
            // table.timestamp("date").notNullable();
            table.timestamps(false, true);
        });
    }
};



export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('driver_sessions');
}

