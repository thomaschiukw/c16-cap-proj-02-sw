import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('users_lat_long');
    if (!hasTable) {
        await knex.schema.createTable('users_lat_long', (table) => {
            table.increments();
            table.integer("user_id").unsigned().notNullable();
            table.foreign('user_id').references('users.id');
            table.float("lat");
            table.float("long");
            table.timestamps(false, true);
        });
    }
};



export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('users_lat_long');
}

