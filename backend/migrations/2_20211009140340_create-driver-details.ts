import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('driver_details');
    if (!hasTable) {
        await knex.schema.createTable('driver_details', (table) => {
            table.increments();
            table.integer("user_id").unsigned().notNullable();
            table.foreign('user_id').references('users.id');
            table.string("name").notNullable();
            table.string("license").notNullable();
            table.boolean('pet_ok').notNullable();
            table.boolean('smoke_free').notNullable();
            table.timestamps(false, true);
        });
    }
};



export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('driver_details');
}

