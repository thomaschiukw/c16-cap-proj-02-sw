import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('users_orders');
    if (!hasTable) {
        await knex.schema.createTable('users_orders', (table) => {
            table.increments();
            table.integer("order_id").unsigned().notNullable();
            table.foreign('order_id').references('orders.id');
            table.integer("user_id").unsigned().notNullable();
            table.foreign('user_id').references('users.id');
            table.integer("headcount").notNullable();
            // table.float("lat").notNullable();
            // table.float("long").notNullable();
            table.integer("rating_polite");
            table.integer("rating_clean");
            table.integer("rating_punctual");
            table.integer("rating_driving");
            table.string("comment");
            table.timestamps(false, true);
        });
    }
};



export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('users_orders');
}

