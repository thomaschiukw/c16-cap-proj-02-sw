import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('orders');
    if (!hasTable) {
        await knex.schema.createTable('orders', (table) => {
            table.increments();
            table.string("origin_place_id").notNullable();
            table.float("origin_lat").notNullable();
            table.float("origin_lng").notNullable();
            table.string("origin_district").notNullable();
            table.string("origin_description").notNullable();
            table.string("destination_place_id").notNullable();
            table.float("destination_lat").notNullable();
            table.float("destination_lng").notNullable();
            table.string("destination_district").notNullable();
            table.string("destination_description").notNullable();
            table.integer("driver_session_id").unsigned();
            table.foreign('driver_session_id').references('driver_sessions.id');
            table.decimal("fare");
            table.decimal("distance");
            // table.integer("capacity");
            table.boolean('pool').notNullable();
            table.timestamp('pool_deadline');
            table.boolean('hybrid').notNullable();
            table.boolean('red_taxi').notNullable();
            table.boolean('green_taxi').notNullable();
            table.boolean('blue_taxi').notNullable();
            table.boolean('cross_harbour_tunnel').notNullable();
            table.boolean('eastern_harbour_crossing').notNullable();
            table.boolean('western_harbour_crossing').notNullable();
            table.boolean('has_pet').notNullable();
            table.boolean('smoke_free').notNullable();
            table.boolean('has_luggage').notNullable();
            table.boolean('is_disabled').notNullable();
            table.boolean('urgent').notNullable();
            table.string("status").notNullable();
            table.string("remarks");
            table.timestamps(false, true);
        });
    }
};



export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('orders');
}

