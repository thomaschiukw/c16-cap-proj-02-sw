import { Knex } from 'knex';
import { OrderStatusEnum } from '../Enums/orderStatusEnum';
// import { TaxiTypesEnum } from '../Enums/taxiTypesEnum';
// import { add } from 'date-fns'


export class PoolService {
    constructor(private knex: Knex) { };


    getPoolList = async (id: number) => {
        try {
            // const result = await this.knex.raw(`select o.* from orders o inner join users_orders uo on o.id=uo.order_id where pool=${true} where uo.user_id=${id};`)
            const orders = await this.knex.select("o.*")
                .sum('uo.headcount as headcount_sum')
                .from('orders as o')
                .innerJoin('users_orders as uo', 'o.id', 'uo.order_id')
                .where('pool', true)
                .whereNot('uo.user_id', id)
                .where('pool_deadline', '>', new Date())
                // .having('headcount_sum', '<', 4)
                .whereIn('status', [OrderStatusEnum.PENDING, OrderStatusEnum.SEARCHING])
                .groupBy('o.id').
                orderBy('pool_deadline', 'desc')
                ;
            const result = orders.filter(order => order.headcount_sum < 4)
            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    postJoinPool = async (user_id: number, order_id: number, headcount: number) => {
        try {
            const result = await this.knex.insert([
                {
                    order_id,
                    user_id,
                    headcount
                }

            ]).into('users_orders')


            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    getCurrentPool = async (id: number) => {
        try {



            const order = await this.knex.select("o.id")
                .from('orders as o')
                .innerJoin('users_orders as uo', 'o.id', 'uo.order_id')
                .where('pool', true)
                .where('uo.user_id', id)
                .whereIn('status', [OrderStatusEnum.PENDING, OrderStatusEnum.SEARCHING, OrderStatusEnum.TRAVELLING])
                .returning('id')
                ;

            if (order.length > 0) {
                const result = await this.knex.select("o.*", 'u.username', 'uo.headcount')
                    // .sum('uo.headcount as headcount_sum')
                    .from('orders as o')
                    .innerJoin('users_orders as uo', 'o.id', 'uo.order_id')
                    .innerJoin('users as u', 'u.id', 'uo.user_id')
                    .where('o.id', order[0].id)

                    // .groupBy('o.id')
                    ;

                // if (order.length > 0) {
                //     const result = await this.knex.select("o.*")
                //         .sum('uo.headcount as headcount_sum')
                //         .from('orders as o')
                //         .innerJoin('users_orders as uo', 'o.id', 'uo.order_id')
                //         .where('o.id', order[0].id)

                //         .groupBy('o.id')
                //         ;


                return result;

            }
            return []

        } catch (error) {
            return { success: false, msg: error.message };
        }
    }
}