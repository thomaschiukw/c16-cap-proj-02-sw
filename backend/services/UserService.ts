import { Knex } from 'knex';
import { checkPassword, hashPassword } from '../hash';
import jwtSimple from 'jwt-simple';
import jwt from '../jwt';

export class UserService {
    constructor(private knex: Knex) { };

    async getUsers() {
        try {
            const result = await this.knex.select("*").from('users');
            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    async getUserByUsername(username: string) {
        const result = await this.knex.select("*").from('users').where('username', username);
        console.log("result", result)
        return result;
    }

    async getUserById(user_id: number) {
        try {
            //TODO: avoid Sql injection 
            if (typeof user_id !== "number") {
                return { success: false, msg: "User id incorrect" };
            }

            const result_set = await this.knex.select("username", "email", "password", "phone").from('users').where('id', user_id);

            // console.log("result_set",result_set)

            if (result_set) {
                return result_set[0];
            } else {
                return { success: false, msg: "User not found" };
            }
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    async editUser(
        user_id: number,
        username: string,
        email: string,
        phone: number) {

        try {
            await this.knex.from('users').where('id', user_id).update({ username: username, email: email, phone: phone })
            return { success: true };
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    async getDriverSessionDetails(userId: number) {
        try {
            const result = await this.knex
                .select('ds.*', 'dd.pet_ok', 'dd.smoke_free')
                .from('users as u')
                .innerJoin('driver_sessions as ds', 'u.id', 'ds.user_id')
                .innerJoin('driver_details as dd', 'u.id', 'dd.user_id')
                .where('ds.active_session', true)
                .where('u.id', `${userId}`);

            return result;
        } catch (error) {
            return { success: false, error: error.message };
        }
    }

    async getDriversLocation() {
        try {
            const result = await this.knex
                .select("u.id as userid", "l.lat as lat", "l.long as lng", "ds.type as type", "ds.rego as rego", "o.status")
                .from('users as u')
                .innerJoin('users_lat_long as l', 'u.id', 'l.user_id')
                .innerJoin('driver_sessions as ds', 'u.id', 'ds.user_id')
                .where('ds.active_session', true)
                .where('u.is_driver', true)
                .fullOuterJoin('orders as o', 'ds.id', 'o.driver_session_id')
                // .where('o.status', null)
            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    async getDriverLocationBySessionId(id: number) {
        try {
            const result = await this.knex
                .select("u.id as userid", "l.lat as lat", "l.long as lng", "ds.type as type", "ds.rego as rego")
                .from('users as u')
                .innerJoin('users_lat_long as l', 'u.id', 'l.user_id')
                .innerJoin('driver_sessions as ds', 'u.id', 'ds.user_id')
                .where('u.is_driver', true)
                .where('ds.id', id);
            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    async changeDriverLocationByUserId(id: number, lat: number, lng: number) {
        try {
            // console.log({ id, lat, lng });
            await this.knex
                .from('users_lat_long')
                .where('user_id', id)
                .update({ lat: lat, long: lng, updated_at: new Date() })
            return { success: true };

        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    async login(username: string, password: string) {
        const user = await this.knex.select("*").from('users').where('username', username);

        if (user[0] == null) {
            return { error: 'Invalid username or password' };
        }
        if (!await checkPassword(password, user[0].password)) {
            return { error: 'Invalid username or password' };
        }
        const payload = {
            id: user[0].id,
            username: user[0].username,
            isDriver: user[0].is_driver
        };

        const token = jwtSimple.encode(payload, jwt.jwtSecret);
        return { token: token };
    }

    register = async (username: string, email: string, password: string, phone: number) => {
        try {
            const hashedPassword = await hashPassword(password);

            const sameNameUsers = await this.knex.select("*").from('users').where('username', username);
            if (sameNameUsers.length > 0) {
                return { success: false, error: "The username has been used already!" };
            }

            await this.knex("users").insert([{
                username: username,
                email: email,
                password: hashedPassword,
                phone: phone,
                is_driver: false
            }]);

            return { success: true };
        } catch (error) {
            console.error(error);
            return { success: false, error };
        }
    }

    driverRegister = async (driverName: string, license: string, username: string, email: string, password: string, phone: number, isPetOk: boolean, isSmokeFree: boolean) => {
        try {
            const hashedPassword = await hashPassword(password);

            const sameNameUsers = await this.knex.select("*").from('users').where('username', username);
            if (sameNameUsers.length > 0) {
                return { success: false, error: "The username has been used already!" };
            }

            const driver_id = await this.knex("users").insert([{
                username: username,
                email: email,
                password: hashedPassword,
                phone: phone,
                is_driver: true
            }]).returning('id');

            await this.knex("driver_details").insert([{
                user_id: driver_id[0],
                name: driverName,
                license: license,
                pet_ok: isPetOk,
                smoke_free: isSmokeFree
                // name: driverName,
                // rego: rego,
                // username: username,
                // email: email,
                // password: hashedPassword,
                // phone: phone,
                // is_driver: true
            }]);

            return { success: true };
        } catch (error) {
            console.error(error);
            return { success: false, error };
        }
    }
}