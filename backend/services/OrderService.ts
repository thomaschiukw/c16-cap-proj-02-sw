import { Knex } from 'knex';
import { OrderStatusEnum } from '../Enums/orderStatusEnum';
import { TaxiTypesEnum } from '../Enums/taxiTypesEnum';
import { add } from 'date-fns'

export class OrderService {
    constructor(private knex: Knex) { };

    getOrders = async () => {
        try {
            const result = await this.knex.select("*").from('orders').orderBy("created_at", "desc");
            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    getOrderById = async (id: number) => {
        try {
            const result = await this.knex.select("orders.*")
                .from('orders')
                .where('orders.id', id);
            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    getActiveOrderByUserId = async (id: number) => {
        try {
            const result = await this.knex.select("orders.*", "users_orders.*", "driver_details.name", "driver_sessions.rego")
                .from('users_orders')
                .where('users_orders.user_id', id)
                .whereIn('orders.status', [OrderStatusEnum.SEARCHING,OrderStatusEnum.PENDING, OrderStatusEnum.TRAVELLING])
                .innerJoin('orders', 'orders.id', 'users_orders.order_id')
                .leftJoin('driver_sessions', 'orders.driver_session_id', 'driver_sessions.id')
                .leftJoin('users AS drivers', 'drivers.id', 'driver_sessions.user_id')
                .leftJoin('driver_details', 'drivers.id', 'driver_details.user_id')
                ;

                console.log("id", id);
                
            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    // async getOrderByUsername(username: string) {
    //     const result = await this.knex.select("*").from('orders').where('username', username);
    //     return result;

    getSearchingOrder = async (type: string, capacity: number, allowPet: boolean, disallowSmoke: boolean) => {

        let taxiType;
        if (type === TaxiTypesEnum.RED) {
            taxiType = type + '_taxi';
        } else if (type === TaxiTypesEnum.GREEN) {
            taxiType = type + '_taxi';

        } else if (type === TaxiTypesEnum.BLUE) {
            taxiType = type + '_taxi';
        }

        try {
            if (allowPet && disallowSmoke) {
                // console.log('TEST');

                const result = await this.knex
                    .select('o.*')
                    .sum('uo.headcount as headcount_sum')

                    .from('orders as o')
                    .rightJoin('users_orders as uo', 'o.id', 'uo.order_id')
                    .where('o.status', OrderStatusEnum.SEARCHING)
                    .where(`o.${taxiType}`, true)
                    .whereBetween('uo.headcount', [1, capacity])
                    .groupBy('o.id')
                    .orderBy('o.created_at', 'desc');

                // console.log(result);

                return result;
            }

            if (!allowPet && disallowSmoke) {
                const result = await this.knex
                    .select('o.*')
                    .sum('uo.headcount as headcount_sum')
                    .from('orders as o')
                    .rightJoin('users_orders as uo', 'o.id', 'uo.order_id')
                    .where('o.status', OrderStatusEnum.SEARCHING)
                    .where(`o.${taxiType}`, true)
                    .where('o.has_pet', false)
                    .whereBetween('uo.headcount', [1, capacity])
                    .groupBy('o.id')
                    .orderBy('o.created_at', 'desc');
                return result;
            }

            if (allowPet && !disallowSmoke) {
                const result = await this.knex
                    .select('o.*')
                    .sum('uo.headcount as headcount_sum')
                    .from('orders as o')
                    .rightJoin('users_orders as uo', 'o.id', 'uo.order_id')
                    .where('o.status', OrderStatusEnum.SEARCHING)
                    .where(`o.${taxiType}`, true)
                    .where('o.smoke_free', false)
                    .whereBetween('uo.headcount', [1, capacity])
                    .groupBy('o.id')
                    .orderBy('o.created_at', 'desc');
                return result;
            }

            if (!allowPet && !disallowSmoke) {
                const result = await this.knex
                    .select('o.*')
                    .sum('uo.headcount as headcount_sum')
                    .from('orders as o')
                    .rightJoin('users_orders as uo', 'o.id', 'uo.order_id')
                    .where('o.status', OrderStatusEnum.SEARCHING)
                    .where(`o.${taxiType}`, true)
                    .where('o.has_pet', false)
                    .where('o.smoke_free', false)
                    .whereBetween('uo.headcount', [1, capacity])
                    .groupBy('o.id')
                    .orderBy('o.created_at', 'desc');
                return result;
            }
            return;
        } catch (error) {
            console.error(error.message, "\n", error.hint);
            return { success: false, msg: error.message };
        }
    }

    getDriverPendingOrder = async (driverId: number) => {
        try {
            const result = await this.knex.select("orders.*")
                .from('orders')
                .where('orders.status', OrderStatusEnum.PENDING)
                .where('driver_sessions.active_session', true)
                .where('users.id', driverId)
                .innerJoin('driver_sessions', 'orders.driver_session_id', 'driver_sessions.id')
                .innerJoin('users', 'users.id', 'driver_sessions.user_id');


            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    getActiveOrderByDriverId = async (driverId: number) => {
        try {
            const result = await this.knex.select("orders.*")
                .from('orders')
                .where('orders.status', OrderStatusEnum.PENDING)
                .orWhere('orders.status', OrderStatusEnum.TRAVELLING)
            // .innerJoin('driver_sessions', 'orders.driver_session_id', 'driver_sessions.id')
            // .innerJoin('users', 'users.id', 'driver_sessions.user_id');
            // .where('driver_sessions.active_session', true)
            // .where('users.id', driverId)
            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    acceptOrder = async (driverId: number, orderId: number) => {
        try {
            const driverSession = await this.knex
                .select('ds.*')
                .from('users as u')
                .innerJoin('driver_sessions as ds', 'u.id', 'ds.user_id')
                .where('ds.active_session', true)
                .where('u.id', `${driverId}`);
            // console.log(driverSession);

            // return driverSession;
            const result = await this.knex('orders')
                .where({ id: orderId })
                .where('status', OrderStatusEnum.SEARCHING)
                .update({
                    driver_session_id: driverSession[0].id,
                    status: OrderStatusEnum.PENDING,
                    updated_at: new Date()
                }, ['id', 'status']);

            if (result.length) {
                return result;
            } else {
                return { success: false, msg: "ORDER TAKEN" };
            }
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    createOrder = async (
        origin_place_id: string,
        origin_lat: number,
        origin_lng: number,
        origin_district: string,
        origin_description: string,
        destination_place_id: string,
        destination_lat: number,
        destination_lng: number,
        destination_district: string,
        destination_description: string,
        fare: number,
        distance: number,
        pool: boolean,
        hybrid: boolean,
        red_taxi: boolean,
        green_taxi: boolean,
        blue_taxi: boolean,
        cross_harbour_tunnel: boolean,
        eastern_harbour_crossing: boolean,
        western_harbour_crossing: boolean,
        has_pet: boolean,
        smoke_free: boolean,
        has_luggage: boolean,
        is_disabled: boolean,
        urgent: boolean,
        headcount: number,
        user_id: number
    ) => {
        try {
            let orderId = []
            if (pool) {
                orderId = await this.knex("orders").insert([{
                    origin_place_id,
                    origin_lat,
                    origin_lng,
                    origin_district,
                    origin_description,
                    destination_place_id,
                    destination_lat,
                    destination_lng,
                    destination_district,
                    destination_description,
                    fare,
                    distance,
                    pool,
                    hybrid,
                    red_taxi,
                    green_taxi,
                    blue_taxi,
                    cross_harbour_tunnel,
                    eastern_harbour_crossing,
                    western_harbour_crossing,
                    has_pet,
                    smoke_free,
                    has_luggage,
                    is_disabled,
                    urgent,
                    status: OrderStatusEnum.SEARCHING,
                    pool_deadline: add(new Date(), { minutes: 10 })
                }]).returning('id');
            } else {
                orderId = await this.knex("orders").insert([{
                    origin_place_id,
                    origin_lat,
                    origin_lng,
                    origin_district,
                    origin_description,
                    destination_place_id,
                    destination_lat,
                    destination_lng,
                    destination_district,
                    destination_description,
                    fare,
                    distance,
                    pool,
                    hybrid,
                    red_taxi,
                    green_taxi,
                    blue_taxi,
                    cross_harbour_tunnel,
                    eastern_harbour_crossing,
                    western_harbour_crossing,
                    has_pet,
                    smoke_free,
                    has_luggage,
                    is_disabled,
                    urgent,
                    status: OrderStatusEnum.SEARCHING
                }]).returning('id');
            }
            await this.knex("users_orders").insert([{
                order_id: orderId[0],
                user_id: user_id,
                headcount: headcount
            }]);
            return { success: true };
        } catch (error) {
            console.error(error);
            return { success: false, error };
        }
    }

    startOrder = async (orderId: number) => {
        try {
            const result = await this.knex('orders')
                .where({ id: orderId })
                .update({
                    status: OrderStatusEnum.TRAVELLING,
                    updated_at: new Date()
                }, ['id', 'status']);
            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    completeOrder = async (orderId: number) => {
        try {
            const result = await this.knex('orders')
                .where({ id: orderId })
                .update({
                    status: OrderStatusEnum.COMPLETE,
                    updated_at: new Date()
                }, ['id', 'status']);
            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    getUserHistoryOrder = async (id: number) => {
        try {

            // const result = await this.knex.select("orders.*", "driver_details.name", "driver_sessions.rego")
            //     .from('orders')
            //     .where('orders.status', OrderStatusEnum.COMPLETE)
            //     .orWhere('orders.status', OrderStatusEnum.EXPIRED)
            //     .where('users_orders.user_id', id)
            //     .innerJoin('users_orders', 'orders.id', 'users_orders.order_id')
            //     .leftJoin('driver_sessions', 'orders.driver_session_id', 'driver_sessions.id')
            //     .leftJoin('users AS drivers', 'drivers.id', 'driver_sessions.user_id')
            //     .leftJoin('driver_details', 'drivers.id', 'driver_details.user_id')

            const result = await this.knex.select("o.*", "driver_details.name", "driver_sessions.rego")
                .sum('uo.headcount as headcount_sum')

                .from('orders as o')
                .where('o.status', OrderStatusEnum.COMPLETE)
                .orWhere('o.status', OrderStatusEnum.EXPIRED)
                .where('uo.user_id', id)
                .innerJoin('users_orders as uo', 'o.id', 'uo.order_id')
                .leftJoin('driver_sessions', 'o.driver_session_id', 'driver_sessions.id')
                .leftJoin('users AS drivers', 'drivers.id', 'driver_sessions.user_id')
                .leftJoin('driver_details', 'drivers.id', 'driver_details.user_id')
                .groupBy('o.id', 'driver_details.name', "driver_sessions.rego")
                .orderBy('o.created_at', 'desc');
            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    getDriverHistoryOrder = async (id: number) => {
        try {

            const result = await this.knex.select("o.*")
                .sum('uo.headcount as headcount_sum')

                .from('orders as o')
                .where('o.status', OrderStatusEnum.COMPLETE)
                .orWhere('o.status', OrderStatusEnum.EXPIRED)
                .where('users.id', id)
                .innerJoin('driver_sessions', 'o.driver_session_id', 'driver_sessions.id')
                .innerJoin('users', 'users.id', 'driver_sessions.user_id')

                .rightJoin('users_orders as uo', 'o.id', 'uo.order_id')

                .groupBy('o.id')
                .orderBy('o.created_at', 'desc');
            // console.log('order serv: ', result);

            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    // getPoolList = async (id: number) => {
    //     try {
    //         console.log(new Date());

    //         const result = await this.knex.select("o.*", "uo.user_id")
    //             .from('orders as o')
    //             .innerJoin('users_orders as uo', 'o.id', 'uo.order_id')
    //             .where('pool', true)
    //             .whereNot('uo.user_id', id)
    //             .where('pool_deadline', '>', new Date())

    //             .whereIn('status', [OrderStatusEnum.PENDING, OrderStatusEnum.SEARCHING])
    //             ;
    //         // .orWhere('status', OrderStatusEnum.SEARCHING)

    //         return result;
    //     } catch (error) {
    //         return { success: false, msg: error.message };
    //     }
    // }
}