import { Knex } from 'knex';
import { TaxiTypesEnum } from '../Enums/taxiTypesEnum';

export class DriverService {
    constructor(private knex: Knex) { };

    async getDriver() {
        try {
            const result = await this.knex.select("*").from('driver_details');
            return result;
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    async getDriverByUserId(user_id: number) {
        try {
            //TODO: avoid Sql injection 
            if (typeof user_id !== "number") {
                return { success: false, msg: "User id incorrect" };
            }

            const result_set = await this.knex.raw("select u.email, u.phone , dd.name , dd.license, dd.pet_ok, dd.smoke_free, ds.type, ds.hybrid, ds.capacity, ds.rego,ds.active_session from users u left join driver_details dd on u.id = dd.user_id left join driver_sessions ds on u.id = ds.user_id where u.id = " + user_id + " order by ds.updated_at desc limit 1;");

            if (result_set && result_set.rowCount === 1) {
                return result_set.rows[0];
            } else {
                return { success: false, msg: "Driver not found" };
            }
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    async postDriverByUserId(
        user_id: number,
        email: string,
        phone: number,
        name: string,
        license: string,
        pet_ok: boolean,
        smoke_free: boolean,
        isRedTaxi: boolean,
        isGreenTaxi: boolean,
        isBlueTaxi: any,
        hybrid: boolean,
        capacity: number,
        rego: string,
    ) {
        try {
            let type: string = "";
            if (isRedTaxi) {
                type = TaxiTypesEnum.RED
            } else if (isGreenTaxi) {
                type = TaxiTypesEnum.GREEN
            } else if (isBlueTaxi) {
                type = TaxiTypesEnum.BLUE
            }
            await this.knex.from('users').where('id', user_id).update({ email: email, phone: phone })
            await this.knex.from('driver_details').where('user_id', user_id)
                .update({ name: name, license: license, pet_ok: pet_ok, smoke_free: smoke_free })
            await this.knex.from('driver_sessions').where('user_id', user_id)
                .update({ type: type, hybrid: hybrid, capacity: capacity, rego: rego })

            return { success: true };
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    async postActiveSession(
        id: number,
        isRedTaxi: boolean,
        isGreenTaxi: boolean,
        isBlueTaxi: boolean,
        hybrid: boolean,
        capacity: number,
        rego: string,
    ) {     
        try {
            let type = "";
            if (isRedTaxi) {
                type = TaxiTypesEnum.RED
            } else if (isGreenTaxi) {
                type = TaxiTypesEnum.GREEN
            } else if (isBlueTaxi) {
                type = TaxiTypesEnum.BLUE
            }

            return await this.knex("driver_sessions").insert([{
                user_id: id,
                type: type,
                hybrid: hybrid,
                capacity: capacity,
                rego: rego,
                active_session: true

            }])
                .then(function (result) {
                    return { success: true, msg: "Activate driver Session success" };
                })
                .catch(function (err) {
                    return { success: false, msg: "Fail to Change Active Session" };
                })
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }

    async postDeactiveSession(
        user_id: number,
    ) {
        try {
            return await this.knex("driver_sessions").where('user_id', user_id).update({ active_session: false })

                .then(function (result) {
                    return { success: true, msg: "Deactivate driver session success" };
                })
                .catch(function (err) {
                    return { success: false, msg: "Fail to Change Active Session" };
                })
        } catch (error) {
            return { success: false, msg: error.message };
        }
    }
}