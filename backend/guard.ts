import { NextFunction, Request, Response } from 'express';
import { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import jwt from './jwt';
import { UserService } from './services/UserService';
import { User } from './model';

export const createIsLoggedIn = (permit: Bearer, userService: UserService) => {
    return async (req: Request<User>, res: Response, next: NextFunction) => {
        try {
            const token = permit.check(req);
            // console.log('token =', token);

            if (!token) {
                return res.status(401).json({ msg: "Permission Denied" });
            }
            const payload = jwtSimple.decode(token, jwt.jwtSecret);
            // Querying Database is not compulsory
            const user = await userService.getUserById(payload.id);
            // console.log('user =', user);

            if (user) {
                req.user = user[0];
                return next();
            } else {
                return res.status(401).json({ msg: "Permission Denied" });
            }
        } catch (e) {
            console.error(e);
            return res.status(401).json({ msg: "Permission Denied" });
        }
    }
}