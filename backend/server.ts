import express from 'express';
import expressSession from "express-session";
import http from 'http';
import { Server as SocketIO } from 'socket.io';
import { setSocketIO } from './socketio';
import { Bearer } from 'permit';
import { env } from './env';
import { knex } from './db';
import { logger } from "./logger";

import { createIsLoggedIn } from './guard';

import userRoutes from './router/userRoutes';
import orderRoutes from './router/orderRoutes';
import poolRoutes from './router/poolRoutes';
import driveRoutes from './router/driverRoutes';
import mapRoutes from './router/mapRoutes';
// import reviewRoutes from './router/reviewRoutes';

import { UserController } from './controllers/UserController';
import { AuthController } from './controllers/AuthController';
import { OrderController } from './controllers/OrderController';
import { PoolController } from './controllers/PoolController';
import { MapController } from './controllers/MapController';
import { DriverController } from './controllers/DriverController';
// import { ReviewController } from './controllers/ReviewController';

import { UserService } from './services/UserService';
import { OrderService } from './services/OrderService';
import { DriverService } from './services/DriverService';
import { PoolService } from './services/PoolService';
// import { ReviewService } from './services/reviewService';


export const app = express();
const server = new http.Server(app);
const io = new SocketIO(server);
setSocketIO(io);

export const sessionMiddleware = expressSession({
    secret: 'Taxi Pool',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
});

app.use(sessionMiddleware);
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const permit = new Bearer({
    query: "access_token"
});

const userService = new UserService(knex);
export const authController = new AuthController(userService);
export const mapController = new MapController();
export const userController = new UserController(userService);




const orderService = new OrderService(knex);
export const orderController = new OrderController(orderService);

const driverService = new DriverService(knex);
export const driverController = new DriverController(driverService);

const poolService = new PoolService(knex);
export const poolController = new PoolController(poolService);

// const reviewService = new ReviewService(knex);
// export const reviewController = new ReviewController(ReviewService);

const isLoggedIn = createIsLoggedIn(permit, userService);

app.use(userRoutes);
app.use(mapRoutes);
app.use(poolRoutes);
app.use(orderRoutes);
app.use(driveRoutes);

app.get('/authorised', isLoggedIn, (req, res) => {
    res.json({ success: true });
})

const PORT = env.PORT;

server.listen(PORT, () => {
    logger.info(`Listening at http://localhost:${PORT}/`);
});