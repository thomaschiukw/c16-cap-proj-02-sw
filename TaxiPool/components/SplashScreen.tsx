import React from "react";
import { StyleSheet } from "react-native";
import Spinner from "react-native-loading-spinner-overlay";

export default function SplashScreen() {
    return (
        <Spinner
            visible={true}
            overlayColor={'rgba(150, 150, 150, 0.5)'}
            color="white"
            textContent={'載入中...'}
            textStyle={styles.spinnerTextStyle}
        />
    )
}

const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: 'white',
        fontSize: 20,
    },
});