import React from "react";
import { View, Text, StyleSheet } from "react-native";

export default function PermissionScreen() {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>No Permission</Text>
            <Text>Please allow location services to use this app</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 43,
        fontWeight: 'bold',
        color: '#566246',
    },
})