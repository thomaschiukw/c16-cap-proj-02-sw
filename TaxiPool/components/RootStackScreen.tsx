import React from 'react';
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { LoginStackScreen } from "./LoginStackScreen";
import LoadingIndicator from './LoadingIndicator';

export type RootStackScreenParamList = {
    Root: undefined
    Modal: undefined
};

const RootStack = createNativeStackNavigator<RootStackScreenParamList>();

export function RootStackScreen() {
    return (
        <RootStack.Navigator
            screenOptions={{
                headerShown: false
            }}
        >
            <RootStack.Group>
                <RootStack.Screen name="Root" component={LoginStackScreen} />
            </RootStack.Group>
            <RootStack.Group screenOptions={{ presentation: 'modal' }}>
                <RootStack.Screen name="Modal" component={LoadingIndicator} />
            </RootStack.Group>
        </RootStack.Navigator>
    )
};