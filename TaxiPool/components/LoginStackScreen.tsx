import * as React from 'react';
import { createNativeStackNavigator, NativeStackScreenProps } from "@react-navigation/native-stack";
import LoginLoadingScreen from '../screens/users/authentication/LoginLoadingScreen';
import LoginScreen from "../screens/users/authentication/LoginScreen";
import RegisterScreen from '../screens/users/authentication/RegisterScreen';
import UserHomeTab from '../screens/users/components/UserHomeTab';
import DriverHomeTab from '../screens/drivers/components/DriverHomeTab';
import PermissionScreen from './PermissionScreen';

export type LoginStackScreenParamList = {
    Loading: undefined;
    Login: undefined;
    Register: { isDriver: boolean };
    UserHome: undefined;
    DriverHome: undefined;
    Permission: undefined;
};

export type RegisterScreenProp = NativeStackScreenProps<LoginStackScreenParamList, 'Register'>;

const Stack = createNativeStackNavigator<LoginStackScreenParamList>();

export function LoginStackScreen() {
    return (
        <Stack.Navigator
            initialRouteName="Loading"
            screenOptions={{
                headerBackVisible: false,
                gestureEnabled: false,
                headerShown: false
            }}
        >
            <Stack.Screen
                name="Loading"
                component={LoginLoadingScreen}
            />
            <Stack.Screen
                name="Login"
                component={LoginScreen}
            />
            <Stack.Screen
                name="Register"
                component={RegisterScreen}
                options={{
                    gestureEnabled: true,
                    headerBackVisible: true
                }}
            />
            <Stack.Screen
                name="UserHome"
                component={UserHomeTab}
            />
            <Stack.Screen
                name="DriverHome"
                component={DriverHomeTab}
            />
            <Stack.Screen
                name="Permission"
                component={PermissionScreen}
            />
        </Stack.Navigator>
    );
};