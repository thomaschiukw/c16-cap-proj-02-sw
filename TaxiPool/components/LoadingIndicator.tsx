import React from "react";
import { ActivityIndicator, Modal, StyleSheet, View } from "react-native";
import * as Progress from 'react-native-progress';
import Spinner from 'react-native-loading-spinner-overlay';

export default function LoadingIndicator() {
    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={true}
            onRequestClose={() => { console.log('close modal') }}>
            <View style={styles.modalBackground}>
                {/* <View style={styles.activityIndicatorWrapper}>
                    <ActivityIndicator
                        animating={true} />
                </View> */}
                {/* <View style={styles.activityIndicatorWrapper}> */}
                {/* <Spinner
                visible={true}
                overlayColor={'rgba(150, 150, 150, 0.5)'}
                color="white"
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
            /> */}
                {/* </View> */}
                {/* <Progress.Bar progress={0.3} width={200} />
            <Progress.Pie progress={0.4} size={50} />
            <Progress.Circle size={30} indeterminate={true} />
            <Progress.CircleSnail
                color={['red', 'green', 'blue']}
            /> */}
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000040'
    },
    activityIndicatorWrapper: {
        backgroundColor: '#FFFFFF',
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
});

// const styles = StyleSheet.create({
//     spinnerTextStyle: {
//         color: 'white',
//         fontSize: 20,
//     },
//     activityIndicatorWrapper: {
//         backgroundColor: 'rgba(0, 0, 0, 0.05)',
//         height: 200,
//         width: 200,
//         borderRadius: 10,
//         // display: 'flex',
//         // alignItems: 'center',
//         // justifyContent: 'space-around',
//         // flex: 1,
//         // alignItems: 'center',
//         // flexDirection: 'column',
//         // justifyContent: 'space-around',
//     }
// });