import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { RootStackScreen } from './components/RootStackScreen';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { io } from "socket.io-client";
//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';

export const socket = io(`${REACT_APP_BACKEND_URL}`);

export default function App() {
    socket.on("connect", () => {
        // console.log("socketID =", socket.connected);
    });

    return (
        <SafeAreaProvider>
            <NavigationContainer>
                <RootStackScreen />
            </NavigationContainer>
        </SafeAreaProvider>
    );
};