//@ts-expect-error
import { GOOGLE_MAPS_API_KEY } from '@env';
//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import Geolocation from '@react-native-community/geolocation';
import React, { useEffect, useState } from "react";
import { SubmitHandler, useForm } from 'react-hook-form';
import { Dimensions, Keyboard, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from "react-native";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useDispatch, useSelector } from "react-redux";
import { socket } from '../../../App';
import SplashScreen from '../../../components/SplashScreen';
import { DectivateSession, fetchDriverInfo } from '../../../redux/driver/thunk';
import { fetchDriverActiveOrder } from '../../../redux/driverOrder/thunk';
import { RootState } from "../../../store";
import DriverMapInfo from '../map/DriverMapInfo';
import GoWork from '../setting/GoWork';

export type DeactiveSessionFormInput = {
    active_session: boolean,
}

export default function DriverMapScreen() {
    const dispatch = useDispatch();
    const mapView = React.useRef<MapView | null>(null) as React.MutableRefObject<MapView | null>;;
    const { width, height } = Dimensions.get('window');
    const aspectRatio = width / height;

    const originCoord = useSelector((state: RootState) => state.map.selectedOrigin.coordinates);
    const destinationCoord = useSelector((state: RootState) => state.map.selectedDestination.coordinates);
    const userInfo = useSelector((state: RootState) => state.driver);
    const driverId = useSelector((state: RootState) => state.auth.user?.id);
    const driverActiveOrder = useSelector((state: RootState) => state.driverOrder.activeOrder);

    const [isLoading, setIsLoading] = useState(true);
    const [lat, setLat] = useState(22.25);
    const [long, setLong] = useState(114.1524);
    const [latitudeDelta, setLatitudeDelta] = useState(0.005);
    const [longitudeDelta, setLongitudeDelta] = useState(latitudeDelta * aspectRatio);
    // const [modalVisible, setModalVisible] = useState(false);
    // const [active_session, setActive_session] = useState<boolean>(userInfo.active_session);

    // console.log("action", userInfo.active_session);

    useEffect(() => {
        if (driverId) {
            dispatch(fetchDriverActiveOrder(driverId));
            dispatch(fetchDriverInfo(driverId))
        }
    }, [])

    useEffect(() => {
        if (userInfo.active_session !== undefined) {
            setIsLoading(false);
        }
    }, [userInfo])

    useEffect(() => {
        Geolocation.getCurrentPosition(info => {
            if (!originCoord && !destinationCoord) {
                setLat(info.coords.latitude);
                setLong(info.coords.longitude);
            }
        });
    }, [])

    useEffect(() => {
        socket.on("order-started", () => {
            if (driverId) {
                // console.log("order-started");
                dispatch(fetchDriverActiveOrder(driverId));
            }
        })

        socket.on("order-completed", () => {
            if (driverId) {
                // console.log("order-completed");
                dispatch(fetchDriverActiveOrder(driverId));
            }
        })


        socket.on("session-activated", () => {
            // console.log("session-activated")
            if (driverId) {
                dispatch(fetchDriverActiveOrder(driverId));
            }
        })


        socket.on("session-deactivated", () => {
            // console.log("session-deactivated")
            if (driverId) {
                dispatch(fetchDriverActiveOrder(driverId));
            }
        })
    }, [])

    function updateCurrentLocation() {
        Geolocation.getCurrentPosition(info => {
            mapView.current?.animateToRegion({ // Takes a region object as parameter
                longitude: info.coords.longitude,
                latitude: info.coords.latitude,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005 * aspectRatio
            }, 1000);
        });
    }

    function animateToCurrentRoute() {
        if (driverActiveOrder.length !== 0) {
            const x = (driverActiveOrder[0].origin_lng + driverActiveOrder[0].destination_lng) / 2;
            const y = ((driverActiveOrder[0].origin_lat + driverActiveOrder[0].destination_lat) / 2);
            const xDelta = (Math.sqrt(Math.pow((driverActiveOrder[0].origin_lng - driverActiveOrder[0].destination_lng), 2))) + 0.03;
            const yDelta = (Math.sqrt(Math.pow((driverActiveOrder[0].origin_lat - driverActiveOrder[0].destination_lat), 2))) + 0.05;

            mapView.current?.animateToRegion({ // Takes a region object as parameter
                longitude: x,
                latitude: y,
                latitudeDelta: xDelta,
                longitudeDelta: yDelta
            }, 1000);
        }
    }

    function animateToBearing() {
        Geolocation.getCurrentPosition(info => {
            console.log(info)
            if (info.coords.heading && info.coords.altitude) {
                mapView.current?.animateCamera({
                    center: {
                        latitude: info.coords.latitude,
                        longitude: info.coords.longitude,
                    },
                    heading: info.coords.heading,
                    altitude: info.coords.altitude,
                    zoom: 18
                }, { duration: 1000 })
            } else {
                mapView.current?.animateCamera({
                    center: {
                        latitude: info.coords.latitude,
                        longitude: info.coords.longitude,
                    },
                    zoom: 18
                }, { duration: 1000 })
            }
        });
    }

    const { register, setValue, handleSubmit, control, reset, formState: { errors } } = useForm<DeactiveSessionFormInput>({
        defaultValues: {
            active_session: false,
        }
    })

    const onSubmit: SubmitHandler<DeactiveSessionFormInput> = data => dispatch(DectivateSession(data));

    return (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <View style={styles.container}>
                {isLoading && <SplashScreen />}
                {isLoading == false &&
                    <MapView
                        ref={mapView}
                        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                        region={{
                            latitude: lat,
                            longitude: long,
                            latitudeDelta: latitudeDelta,
                            longitudeDelta: longitudeDelta,
                        }}
                        showsUserLocation={true}
                        followsUserLocation={true}
                        showsMyLocationButton={false}
                        showsCompass={true}
                        showsBuildings={false}
                        loadingEnabled={true}
                        mapPadding={{
                            top: 160,
                            left: 0,
                            bottom: 0,
                            right: 0
                        }}
                        style={styles.map}
                        onUserLocationChange={async (e) => {
                            try {
                                // console.log(e.nativeEvent.coordinate)
                                const formData = {
                                    id: driverId,
                                    lat: e.nativeEvent.coordinate.latitude,
                                    lng: e.nativeEvent.coordinate.longitude
                                }

                                await fetch(`${REACT_APP_BACKEND_URL}/user/location`, {
                                    method: "PUT",
                                    headers: {
                                        "Content-Type": "application/json; charset=utf-8",
                                    },
                                    body: JSON.stringify(formData),
                                })
                            } catch (error) {
                                console.error();
                                return error;
                            }
                        }}
                    >
                        {driverActiveOrder.length !== 0 && <MapViewDirections
                            origin={`place_id:${driverActiveOrder[0].origin_place_id}`}
                            destination={`place_id:${driverActiveOrder[0].destination_place_id}`}
                            apikey={GOOGLE_MAPS_API_KEY}
                            strokeWidth={5}
                            strokeColor="purple"
                            onReady={result => {
                                // animateToCurrentRoute();
                            }}
                        />}
                        {driverActiveOrder.length !== 0 && <Marker
                            coordinate={{ latitude: driverActiveOrder[0].origin_lat, longitude: driverActiveOrder[0].origin_lng }}
                            title={'出發地'}
                            pinColor={'red'}
                        />}
                        {driverActiveOrder.length !== 0 && <Marker
                            coordinate={{ latitude: driverActiveOrder[0].destination_lat, longitude: driverActiveOrder[0].destination_lng }}
                            title={'目的地'}
                            pinColor={'green'}
                        />}
                    </MapView>}

                <View style={{
                    position: "absolute",
                    top: 0,
                    width: "100%",
                }}>
                    <DriverMapInfo />
                </View>

                <View style={{
                    position: "absolute",
                    bottom: 0,
                    width: "100%",
                }}>
                    <View style={styles.btnContainer}>

                        {/*If driver.active_session !== true, show 收工button 
                            If driver.active_session == true, show 開工button */}
                        {userInfo.active_session === false &&
                            <TouchableOpacity
                            // onPress={() => {
                            //     setModalVisible(true)
                            // }}
                            >
                                <GoWork />
                            </TouchableOpacity>}

                        {userInfo.active_session === true && driverActiveOrder.length === 0 &&
                            <TouchableOpacity
                                onPress={async () => {
                                    // setActive_session(false)
                                    await handleSubmit(onSubmit)();
                                    // console.log("setActive_session", active_session);
                                    // console.log("ran deactivate");
                                }}
                                activeOpacity={0.8}
                                style={styles.btnElem}>
                                {/* <MaterialIcons name="stop"
                                    style={styles.btnIcon}
                                /> */}
                                <Text style={styles.workIcon}>收工</Text>
                            </TouchableOpacity>
                        }

                        <TouchableOpacity
                            onPress={animateToCurrentRoute}
                            activeOpacity={0.8}
                            style={styles.btnElem}>
                            <MaterialIcons name="directions"
                                style={styles.btnIcon}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                updateCurrentLocation();
                            }}
                            onLongPress={() => {
                                animateToBearing();
                            }}
                            activeOpacity={0.8}
                            style={styles.btnElem}>
                            <MaterialIcons name="navigation"
                                style={styles.btnIcon}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View >
        </TouchableWithoutFeedback >
    )
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        height: '100%',
    },
    btnContainer: {
        alignItems: "flex-end",
        borderRadius: 100
    },
    btnElem: {
        marginHorizontal: 10,
        marginBottom: 10,
        backgroundColor: 'rgba(25,114,232,1)',
        borderRadius: 200,
    },
    btnIcon: {
        bottom: 0,
        right: 0,
        fontSize: 25,
        padding: 15,
        color: 'white'
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "green",
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
    },
    workIcon: {
        bottom: 0,
        right: 0,
        fontSize: 20,
        paddingHorizontal: 8,
        paddingVertical: 13,
        color: 'white',
        fontWeight: 'bold',
    }
});