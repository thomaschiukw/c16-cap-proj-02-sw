import React, { useEffect, useState } from "react";
import { NavigationContainer, NavigationProp, StackActions, useNavigation } from "@react-navigation/native";
import { RootStackScreenParamList } from "../../../components/RootStackScreen";
import { View, Text, Button, StyleSheet, TouchableOpacity } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { fetchLogout } from "../../../redux/authentication/thunk";
import { RootState } from "../../../store";
import SplashScreen from "../../../components/SplashScreen";
import { LoginStackScreenParamList } from "../../../components/LoginStackScreen";

export function DriverLogout() {
    const dispatch = useDispatch();
    const navigation: NavigationProp<LoginStackScreenParamList> = useNavigation();
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
    const isLoading = useSelector((state: RootState) => state.loading.isLoading);

    useEffect(() => {
        if (isLoading === -1) {
            if (!isAuthenticated) {
                navigation.dispatch(StackActions.replace('Login'))
            }
        }
    }, [isLoading]);
    return (
        <View style={styles.container}>
            {/* {isLoading === 1 && <SplashScreen />}
            <Button
                title="Driver Logout"
                onPress={() => dispatch(fetchLogout())}
            /> */}
            <TouchableOpacity
                style={styles.button}
                onPress={() => dispatch(fetchLogout())}
            >{isLoading === 1 && <SplashScreen />}
                <Text style={styles.buttonText}>登出</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        alignItems: "center",
        backgroundColor: "#566246",
        paddingVertical: 20,
        width: "90%",
        alignSelf: "center",
        borderRadius: 10,
    },
    buttonText: {
        textAlign: 'center',
        color: '#f1f2eb',
        fontWeight: 'bold',
    },
});