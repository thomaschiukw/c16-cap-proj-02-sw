import * as React from 'react';
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import DriverSetting from './DriverSetting';
import DriverSettingChange from './DriverSettingChange';

export type DriverSettingStackScreenParamList = {
    設定主頁: undefined;
    更改設定: undefined;
};

interface CustomTabBarLabelStyle {
    fontSize: number;
    fontWeight: "bold" | "normal" | "100" | "200" | "300" | "400" | "500" | "600" | "700" | "800" | "900" | undefined;
};

const customTabBarLabelStyle: CustomTabBarLabelStyle = {
    fontSize: 7.5,
    fontWeight: "500"
};

const Stack = createNativeStackNavigator<DriverSettingStackScreenParamList>();

export function DriverSettingStackScreen() {
    return (
        <Stack.Navigator
            initialRouteName="設定主頁"
        >
            <Stack.Screen
                name="設定主頁"
                component={DriverSetting}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="更改設定"
                component={DriverSettingChange}
                options={{ 
                    title: "更改設定" ,
                    headerBackVisible: true,
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
};

