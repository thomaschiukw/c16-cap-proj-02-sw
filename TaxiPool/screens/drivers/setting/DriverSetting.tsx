import React, { Component, useEffect } from 'react';
import { StyleSheet, Text, View, Pressable, ScrollView } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { NavigationProp, useNavigation, TabActions } from '@react-navigation/native'
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../store';
import { fetchDriverInfo } from '../../../redux/driver/thunk';
import SplashScreen from '../../../components/SplashScreen';
import { DriverLogout } from '../home/DriverLogout';
import { DriverSettingStackScreenParamList } from './DriverSettingStackScreen';

export default function DriverSetting() {
    const dispatch = useDispatch();
    const isLoading = useSelector((state: RootState) => state.loading.isLoading);
    const userID = useSelector((state: RootState) => state.auth.user?.id);
    const userInfo = useSelector((state: RootState) => state.driver);

    function updateDriverInfo() {
        if (userID) {
            dispatch(fetchDriverInfo(userID));
        }
    }

    useEffect(() => {
        updateDriverInfo()
    }, [dispatch, userInfo])

    const navigation: NavigationProp<DriverSettingStackScreenParamList> = useNavigation();
    const goToChangeSetting = () => {
        navigation.navigate('更改設定');
    }

    return (
        <ScrollView keyboardDismissMode='interactive' style={styles.mainContainer}>
            {isLoading === 1 && <SplashScreen />}
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.headerContent}>
                        <Text style={styles.name}>{userInfo.name}</Text>
                        <Text style={styles.userInfo}>{userInfo.email}</Text>
                        <Text style={styles.userInfo}>{userInfo.phone}</Text>
                    </View>
                </View>

                <View style={styles.body}>
                    <View style={styles.item}>
                        <View style={styles.iconContent}>
                            <MaterialCommunityIcons name="card-text" style={styles.icon} />
                        </View>

                        <View style={styles.infoContentTitle}>
                            <Text style={styles.info}>駕駛執照</Text>
                        </View>

                        <View style={styles.infoContentDesc}>
                            <Text style={styles.info}>{userInfo.license}</Text>
                        </View>
                    </View>


                    <View style={styles.item}>
                        <View style={styles.iconContent}>
                            <MaterialCommunityIcons name="taxi" style={styles.icon} />
                        </View>

                        <View style={styles.infoContentTitle}>
                            <Text style={styles.info}>的士種類</Text>
                        </View>

                        <View style={styles.infoContentDesc}>
                            <Text style={styles.info}>
                                {userInfo.type == "red" ? "紅的" : ""}
                                {userInfo.type == "green" ? "綠的" : ""}
                                {userInfo.type == "blue" ? "藍的" : ""}
                            </Text>
                        </View>
                    </View>

                    <View style={styles.item}>
                        <View style={styles.iconContent}>
                            <MaterialCommunityIcons name="card-text" style={styles.icon} />
                        </View>

                        <View style={styles.infoContentTitle}>
                            <Text style={styles.info}>車牌號碼</Text>
                        </View>

                        <View style={styles.infoContentDesc}>
                            <Text style={styles.info}>{userInfo.rego}</Text>
                        </View>
                    </View>

                    <View style={styles.item}>
                        <View style={styles.iconContent}>
                            <MaterialCommunityIcons name="power-plug" style={styles.icon} />
                        </View>

                        <View style={styles.infoContentTitle}>
                            <Text style={styles.info}>能源</Text>
                        </View>

                        <View style={styles.infoContentDesc}>
                            <Text style={styles.info}>{userInfo.hybrid ? "混能" : "非混能"}</Text>
                        </View>
                    </View>

                    <View style={styles.item}>
                        <View style={styles.iconContent}>
                            <MaterialCommunityIcons name="account-group" style={styles.icon} />
                        </View>

                        <View style={styles.infoContentTitle}>
                            <Text style={styles.info}>容量</Text>
                        </View>


                        <View style={styles.infoContentDesc}>
                            <Text style={styles.info}>{userInfo.capacity} 人車</Text>
                        </View>
                    </View>

                    <View style={styles.item}>
                        <View style={styles.iconContent}>
                            <MaterialCommunityIcons name="paw" style={styles.icon} />
                        </View>

                        <View style={styles.infoContentTitle}>
                            <Text style={styles.info}>攜帶寵物上車</Text>
                        </View>

                        <View style={styles.infoContentDesc}>
                            <Text style={styles.info}>{userInfo.pet_ok ? "可攜帶寵物上車" : "不可攜帶寵物上車"}</Text>
                        </View>
                    </View>

                    <View style={styles.item}>
                        <View style={styles.iconContent}>
                            <MaterialCommunityIcons name="smoking" style={styles.icon} />
                        </View>

                        <View style={styles.infoContentTitle}>
                            <Text style={styles.info}>吸煙習慣</Text>
                        </View>

                        <View style={styles.infoContentDesc}>
                            <Text style={styles.info}>{userInfo.smoke_free ? "吸煙司機" : "不吸煙司機"}</Text>
                        </View>
                    </View>
                </View>

                <Pressable style={styles.button}
                    onPress={() => { { !userInfo.active_session ? goToChangeSetting() : null } }}>
                    <View style={!userInfo.active_session ? { ...styles.button, ...styles.activeBtn } : { ...styles.button, ...styles.inactiveBtn }}>
                        <Text style={styles.buttonText}>修改設定</Text>
                    </View>
                </Pressable>

                <DriverLogout />

            </View>
        </ScrollView >
    );
}


const styles = StyleSheet.create({
    container: {
        width: "90%",
        alignSelf: "center",
    },
    header: {
        backgroundColor: "#f1f2eb",
    },
    headerContent: {
        padding: '5%',
        alignItems: 'center',
    },
    name: {
        fontSize: 30,
        color: "#566246",
        fontWeight: 'bold',
        paddingVertical: 20,
    },
    userInfo: {
        fontSize: 16,
        color: "#566246",
        fontWeight: '600',
    },
    body: {
        // backgroundColor: "yellow",
        height: '45%',
        marginBottom: '20%',
    },
    item: {
        flexDirection: 'row',
    },
    infoContent: {
        flex: 1,
        alignItems: 'flex-start',
        // paddingLeft: 5
    },
    iconContent: {
        flex: 1,
        alignItems: 'center',
        paddingRight: 5,
    },
    infoContentTitle: {
        flex: 2,
        paddingRight: 20,
    },
    infoContentDesc: {
        flex: 3,
    },
    icon: {
        height: 30,
        marginTop: 20,
        // marginHorizontal: 10,
        fontSize: 25,
        color: "#4a4a48",
    },
    info: {
        marginTop: 20,
        color: "#4a4a48",
        fontSize: 16,
    },
    buttonText: {
        color: 'white',
        textAlign: 'center'
    },
    button: {
        borderRadius: 10,
        width: "95%",
        paddingVertical: 10,
        alignSelf: 'center',
    },
    activeBtn: {
        backgroundColor: '#566246',
    },
    inactiveBtn: {
        backgroundColor: '#999999'
    },
    mainContainer: {
        // backgroundColor: 'orange',
    }
});
