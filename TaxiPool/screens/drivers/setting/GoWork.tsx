import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Switch, TextInput, TouchableOpacity, TouchableWithoutFeedback, Modal, Keyboard, Alert, Pressable } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useDispatch, useSelector } from "react-redux";
import { useForm, SubmitHandler, Controller } from 'react-hook-form';
import { ActivateSession } from "../../../redux/driver/thunk";
import { RootState } from '../../../store';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { DriverHomeTabParamList } from '../components/DriverHomeTab';
import { color } from 'react-native-elements/dist/helpers';

export type SessionFormInput = {
    // type: string,
    isRedTaxi: boolean,
    isGreenTaxi: boolean,
    isBlueTaxi: boolean,
    rego: string,
    hybrid: boolean,
    capacity: string,
    active_session: boolean,
}

export default function GoWork() {
    const dispatch = useDispatch();
    // const isLoading = useSelector((state: RootState) => state.loading.isLoading);
    // const userID = useSelector((state: RootState) => state.auth.user?.id);
    const userInfo = useSelector((state: RootState) => state.driver);

    // function updateDriverInfo() {
    //     if (userID) {
    //         // dispatch(fetchDriverInfo(userID));
    //     }
    // }

    // useEffect(() => {
    //     updateDriverInfo()
    //     console.log("userID", userID);

    // }, [dispatch, userInfo])

    // useEffect(() => {
    //     if (userInfo.type == "red") {
    //         selectRedTaxi()
    //         setValue("type", "red")
    //     } else if (userInfo.type == "green") {
    //         selectGreenTaxi()
    //         setValue("type", "green")
    //     } else if (userInfo.type == "blue") {
    //         selectBlueTaxi()
    //         setValue("type", "blue")
    //     }
    // }, [dispatch])

    // const [type, setType] = useState<string>(userInfo.type);
    const [isRedTaxi, setIsRedTaxi] = useState<boolean>();
    const [isGreenTaxi, setIsGreenTaxi] = useState<boolean>();
    const [isBlueTaxi, setIsBlueTaxi] = useState<boolean>();
    // const [rego, setRego] = useState<string>(userInfo.rego);
    const [hybrid, setHybrid] = useState<boolean>();
    const [capacity, setCapacity] = useState<string>(userInfo.capacity);

    // const [isOrderLoading, setIsOrderLoading] = useState(0);
    // const [confirmModalVisible, setConfirmModalVisible] = useState(false);
    // const [successModalVisible, setSuccessModalVisible] = useState(false);
    // const [failModalVisible, setFailModalVisible] = useState(false);
    const [workBtnModalVisible, setWorkBtnModalVisible] = useState(false);
    const [noTaxi, setNoTaxi] = useState(false);
    // const driverId = useSelector((state: RootState) => state.auth.user?.id);
    // const [active_session, setActive_session] = useState<boolean>(userInfo.active_session);

    // useEffect(() => {
    //     if (isOrderLoading == 1) {
    //         setConfirmModalVisible(!confirmModalVisible);
    //     }
    //     if (isOrderLoading == -1) {
    //         setSuccessModalVisible(!successModalVisible);
    //     }
    // }, [isOrderLoading])

    // useEffect(() => {
    //     if (driverId) {
    //         dispatch(fetchDriverActiveOrder(driverId));
    //         dispatch(fetchDriverInfo(driverId))
    //     }
    // }, [setActive_session])

    function selectRedTaxi() {
        setIsGreenTaxi(false);
        setValue("isGreenTaxi", false)
        setIsBlueTaxi(false);
        setValue("isBlueTaxi", false)
        // if (!isGreenTaxi && !isBlueTaxi) {
        setIsRedTaxi(true);
        setValue("isRedTaxi", true)
        // setType("red")
        // }
    }

    function selectGreenTaxi() {
        setIsRedTaxi(false);
        setValue("isRedTaxi", false)
        setIsBlueTaxi(false);
        setValue("isBlueTaxi", false)
        // if (!isRedTaxi && !isBlueTaxi) {
        setIsGreenTaxi(true);
        setValue("isGreenTaxi", true)
        // setType("green")
        // }
    }

    function selectBlueTaxi() {
        setIsGreenTaxi(false);
        setValue("isGreenTaxi", false)
        setIsRedTaxi(false);
        setValue("isRedTaxi", false)
        // if (!isGreenTaxi && !isRedTaxi) {
        setIsBlueTaxi(true);
        setValue("isBlueTaxi", true)
        // setType("blue")
        // }
    }

    const { register, setValue, handleSubmit, control, reset, formState: { errors } } = useForm<SessionFormInput>({
        defaultValues: {
            // type: type,
            isRedTaxi: isRedTaxi,
            isGreenTaxi: isGreenTaxi,
            isBlueTaxi: isBlueTaxi,
            rego: userInfo.rego,
            hybrid: userInfo.hybrid,
            capacity: userInfo.capacity,
            active_session: false,
        }
    })
    const onSubmit: SubmitHandler<SessionFormInput> = data => dispatch(ActivateSession(data));

    const navigation: NavigationProp<DriverHomeTabParamList> = useNavigation();
    // const goToMap = () => {
    //     userID && dispatch(fetchDriverInfo(userID))
    //     // navigation.navigate("地圖")
    // }
    return (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>

            <View style={styles.centeredViewUp}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={workBtnModalVisible}

                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        setWorkBtnModalVisible(!workBtnModalVisible);
                    }}
                >

                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <View style={styles.questionView}>
                                    <Text style={styles.questionText}>開工前</Text>
                                    <Text style={styles.questionText}>請輸入開工資料</Text>

                                    <View>
                                        <View style={styles.container}>

                                            <View style={styles.body}>
                                                <View style={styles.item}>
                                                    <View style={styles.iconContent}>
                                                        <MaterialCommunityIcons name="taxi" style={styles.icon} />
                                                    </View>

                                                    <View style={styles.infoContent}>
                                                        <Text style={styles.info}>的士種類</Text>
                                                    </View>

                                                    <View style={styles.taxiTypeContent}>
                                                        <View style={styles.taxiTypesBox}>
                                                            <TouchableHighlight style={styles.taxiType}
                                                                underlayColor={''}
                                                                onPress={() => {
                                                                    // let updatedState = !isRedTaxi;
                                                                    // setValue("isRedTaxi", updatedState);
                                                                    // setIsRedTaxi(updatedState);
                                                                    selectRedTaxi();
                                                                }}
                                                            >
                                                                <View style={isRedTaxi ? styles.iconSelected : styles.iconDeselected}>
                                                                    <MaterialCommunityIcons name="taxi" color="red" style={styles.icon} />
                                                                    <Text style={styles.iconLabel}>紅的</Text>
                                                                </View>
                                                            </TouchableHighlight>
                                                            <TouchableHighlight style={styles.taxiType}
                                                                underlayColor={''}
                                                                onPress={() => {
                                                                    // let updatedState = !isGreenTaxi;
                                                                    // setValue("isGreenTaxi", updatedState);
                                                                    // setIsGreenTaxi(updatedState);
                                                                    selectGreenTaxi();
                                                                }}
                                                            >
                                                                <View style={isGreenTaxi ? styles.iconSelected : styles.iconDeselected}>
                                                                    <MaterialCommunityIcons name="taxi" color="green" style={styles.icon} />
                                                                    <Text style={styles.iconLabel}>綠的</Text>
                                                                </View>
                                                            </TouchableHighlight>
                                                            <TouchableHighlight style={styles.taxiType}
                                                                underlayColor={''}
                                                                onPress={() => {
                                                                    // let updatedState = !isBlueTaxi;
                                                                    // setValue("isBlueTaxi", updatedState);
                                                                    // setIsBlueTaxi(updatedState);
                                                                    selectBlueTaxi();
                                                                }}
                                                            >
                                                                <View style={isBlueTaxi ? styles.iconSelected : styles.iconDeselected}>
                                                                    <MaterialCommunityIcons name="taxi" color="skyblue" style={styles.icon} />
                                                                    <Text style={styles.iconLabel}>藍的</Text>
                                                                </View>
                                                            </TouchableHighlight>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', }}>
                                                            {noTaxi && <Text style={styles.errText}>"請點擊您的的士種類"</Text>}
                                                            <Text> </Text>
                                                        </View>

                                                    </View>
                                                </View>

                                                <View style={styles.item}>
                                                    <View style={styles.iconContent}>
                                                        <MaterialCommunityIcons name="card-text" style={styles.icon} />
                                                    </View>

                                                    <View style={styles.infoContent}>
                                                        <Text style={styles.info}>車牌號碼</Text>
                                                    </View>

                                                    <View style={styles.inputTextBox}><Controller
                                                        control={control}
                                                        render={({ field: { onChange, onBlur, value } }) => (
                                                            <TextInput
                                                                style={styles.inputTextContent}
                                                                onBlur={onBlur}
                                                                onChangeText={value => onChange(value)}
                                                                value={value}
                                                                autoCapitalize={"characters"}
                                                            />
                                                        )}
                                                        name="rego"
                                                        rules={{ required: true }}
                                                    /><View style={{ flexDirection: 'row' }}>
                                                            {errors.rego && <Text style={styles.errText}>"請輸入您的車牌號碼"</Text>}
                                                            <Text> </Text></View>
                                                    </View>
                                                </View>

                                                <View style={styles.item}>
                                                    <View style={styles.iconContent}>
                                                        <MaterialCommunityIcons name="power-plug" style={styles.icon} />
                                                    </View>

                                                    <View style={styles.infoContent}>
                                                        <Text style={styles.info}>能源</Text>
                                                    </View>

                                                    <View style={styles.booleanOption}>
                                                        <Text style={{ ...styles.info, textAlign: 'right' }}>非混合</Text>
                                                    </View>
                                                    <Switch style={styles.inputBooleanContent}
                                                        onValueChange={(bool) => {
                                                            let updatedState = bool;
                                                            setValue("hybrid", updatedState)
                                                            setHybrid(updatedState);
                                                        }}
                                                        value={hybrid} />
                                                    <View style={styles.booleanOption}>
                                                        <Text style={{ ...styles.info, textAlign: 'left' }}>混合</Text>
                                                    </View>
                                                </View>

                                                <View style={styles.item}>
                                                    <View style={styles.iconContent}>
                                                        <MaterialCommunityIcons name="account-group" style={styles.icon} />
                                                    </View>

                                                    <View style={styles.infoContent}>
                                                        <Text style={styles.info}>容量</Text>
                                                    </View>

                                                    <View style={styles.booleanOption}>
                                                        <Text style={{ ...styles.info, textAlign: 'right' }}>四人車</Text>
                                                    </View>

                                                    <Switch style={styles.inputBooleanContent}
                                                        onValueChange={(bool) => {
                                                            let updatedState = bool == true ? "5" : "4"
                                                            setValue("capacity", updatedState)
                                                            setCapacity(updatedState);
                                                        }}
                                                        value={capacity == "4" ? false : true}
                                                    />
                                                    <View style={styles.booleanOption}>
                                                        <Text style={{ ...styles.info, textAlign: 'left' }}>五人車</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>

                                    </View>

                                    <View style={{ flexDirection: "row" }}>
                                        <TouchableOpacity
                                            style={[styles.button, styles.buttonClose]}
                                            onPress={async () => {
                                                // setValue("active_session", false)
                                                // setIsOrderLoading(1);
                                                if (!isRedTaxi && !isGreenTaxi && !isBlueTaxi) {
                                                    setNoTaxi(true);
                                                } else {
                                                    await handleSubmit(onSubmit)();
                                                    // setWorkBtnModalVisible(!workBtnModalVisible)
                                                }
                                                // console.log("setActive_session", active_session);

                                                // console.log("ran activate");
                                                // setIsOrderLoading(-1);
                                                // goToMap();
                                            }}
                                        >
                                            <Text style={styles.textStyle}>開工</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity
                                            style={[styles.button, styles.buttonClose]}
                                            onPress={async () => {
                                                setWorkBtnModalVisible(!workBtnModalVisible)
                                            }}
                                        >
                                            <Text style={styles.textStyle}>返回</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>


                <Pressable
                    style={[styles.btnElem, styles.buttonOpen]}
                    onPress={() => setWorkBtnModalVisible(true)}
                >
                    {/* <MaterialIcons name="play-arrow" color="black"
                        style={styles.btnIcon}></MaterialIcons> */}
                    <Text style={styles.btnIcon}>開工</Text>
                </Pressable>


            </View >
        </TouchableWithoutFeedback >
    );
}


const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
    },
    header: {
        backgroundColor: "#f1f2eb",
    },
    headerContent: {
        // padding: 30,
        alignItems: 'center',
    },
    name: {
        fontSize: 30,
        color: "#566246",
        fontWeight: 'bold',
        // paddingVertical: 20,
    },
    userInfo: {
        fontSize: 16,
        color: "#566246",
        fontWeight: '600',
    },
    body: {
        backgroundColor: "white",
        height: 270,
        width: 300,
        alignItems: 'center',
        justifyContent: 'center',
    },
    item: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconContent: {
        flex: 1,
        alignItems: 'center',
        paddingHorizontal: 2,
    },
    input_row: {
        flexDirection: 'row'
    },
    infoContent: {
        flex: 2,
        margin: 5,
    },
    taxiTypeContent: {
        flex: 3,
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 10,
    },
    taxiTypesBox: {
        flexDirection: 'row',
    },
    inputTextBox: {
        marginTop: 20,
        // flex: 3,
        alignItems: 'center',

    },
    inputTextContent: {
        backgroundColor: '#f1f2eb',
        borderRadius: 5,
        borderColor: "#566246",
        borderWidth: 1,
        justifyContent: 'center',
        textAlign: 'center',
        height: 40,
        width: 150,
    },
    booleanOption: {
        flex: 1,
    },
    inputBooleanContent: {
        flex: 1,
    },
    info: {
        color: "#4a4a48",
        justifyContent: 'center',
    },
    buttonText: {
        color: 'white',
        textAlign: 'center'
    },
    button: {
        backgroundColor: '#566246',
        borderRadius: 10,
        marginHorizontal: 5,
        paddingVertical: 10
    },
    categoryLabel: {
        fontWeight: "bold",
        marginHorizontal: 10
    },
    categoryContainer: {
        flexDirection: "row",
        marginVertical: 10,
        flexWrap: "wrap"
    },
    iconSelected: {
        opacity: 1,
        alignItems: 'flex-end',
    },
    iconDeselected: {
        opacity: 0.2,

        alignItems: 'flex-end',
    },
    icon: {
        width: 30,
        height: 30,
        marginTop: 15,
        fontSize: 20,
        color: "#4a4a48",
        // backgroundColor: 'yellow',
    },
    iconLabel: {
        fontWeight: "600",
        color: "black"
    },
    taxiType: {
        alignItems: 'center',
        marginHorizontal: 15,
    },
    modalCenteredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        // marginTop: 10
    },
    modalView: {
        backgroundColor: "rgba(82, 82, 82, 0.85)",
        borderRadius: 10,
        paddingVertical: '100%',
        paddingHorizontal: '100%',
        justifyContent: "center",
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    modalButtonRow: {
        flexDirection: "row",
    },
    modalButton: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        margin: 5,
    },
    modalButtonOpen: {
        backgroundColor: "#F194FF",
    },
    modalButtonClose: {
        backgroundColor: "#566246",
    },
    modalTextStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    spinnerTextStyle: {
        color: 'white',
        fontSize: 20,
    },
    centeredViewUp: {
        // flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    centeredView: {
        // flex: 1,
        justifyContent: "center",
        alignItems: "center",
        // marginTop: 22,
        // backgroundColor: 'white',
    },
    questionView: {
        backgroundColor: 'white',
        width: 350,
        height: 450,
        justifyContent: "center",
        alignItems: "center",
        padding: 10,
        borderRadius: 10,
        marginBottom: 150,
    },
    questionText: {
        fontSize: 25,
        color: 'black',
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 20,
    },
    buttonClose: {
        backgroundColor: "#566246",
        width: 150,
        height: 60,
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
    },
    buttonOpen: {
        backgroundColor: 'rgba(215,0,0,1)',
        // width: 150,
        // height: 60,
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
    },
    btnElem: {
        marginHorizontal: 10,
        marginBottom: 10,
        backgroundColor: 'rgba(200,0,232,1)',
        borderRadius: 500,
    },
    btnIcon: {
        bottom: 0,
        right: 0,
        fontSize: 20,
        fontWeight: 'bold',
        paddingHorizontal: 8,
        paddingVertical: 13,
        color: 'black'
    },
    errText: {
        color: 'red',
    }
});


