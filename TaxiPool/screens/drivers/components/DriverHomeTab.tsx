import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { DriverEventStackScreen } from "../orders/DriverEventStackScreen";
import { DriverSettingStackScreen } from "../setting/DriverSettingStackScreen";
import { DriverHistoryStackScreen } from "../orders/DriverHistoryStackScreen";
import DriverMapScreen from "../home/DriverMapScreen";
import { Platform } from "react-native";
import RatingHistoryList from "../home/RatingHistoryList";

export type DriverHomeTabParamList = {
    地圖: undefined;
    接單: undefined;
    記錄: undefined;
    評價: undefined;
    設定: undefined;
    Test: undefined;
}

const Tab = createBottomTabNavigator<DriverHomeTabParamList>();

interface CustomTabBarLabelStyle {
    fontSize: number;
    fontWeight: "bold" | "normal" | "100" | "200" | "300" | "400" | "500" | "600" | "700" | "800" | "900" | undefined;
};

interface CustomTabBarIcon {
    size: number | undefined;
};

let customTabBarLabelStyle: CustomTabBarLabelStyle;
if (Platform.OS === 'ios') {
    customTabBarLabelStyle = {
        fontSize: 8,
        fontWeight: "500"
    };
} else if (Platform.OS === 'android') {
    customTabBarLabelStyle = {
        fontSize: 14,
        fontWeight: "500"
    };
}

const customTabBarIcon: CustomTabBarIcon = {
    size: 30
};

export default function DriverHomeTab() {
    return (
        <Tab.Navigator
            initialRouteName={"地圖"}
        >
            <Tab.Screen
                name="地圖"
                component={DriverMapScreen}
                options={{
                    headerShown: false,
                    tabBarLabelStyle: {
                        fontSize: customTabBarLabelStyle.fontSize,
                        fontWeight: customTabBarLabelStyle.fontWeight
                    },
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="map" color={color} size={customTabBarIcon.size} />
                    ),
                    unmountOnBlur: true
                }}
            />
            <Tab.Screen
                name="接單"
                component={DriverEventStackScreen}
                options={{
                    headerShown: false,
                    tabBarLabelStyle: {
                        fontSize: customTabBarLabelStyle.fontSize,
                        fontWeight: customTabBarLabelStyle.fontWeight
                    },
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="taxi" color={color} size={customTabBarIcon.size} />
                    ),
                    unmountOnBlur: true
                }}
            />

            <Tab.Screen
                name="記錄"
                component={DriverHistoryStackScreen}
                options={{
                    headerShown: false,
                    tabBarLabelStyle: {
                        fontSize: customTabBarLabelStyle.fontSize,
                        fontWeight: customTabBarLabelStyle.fontWeight
                    },
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="history" color={color} size={customTabBarIcon.size} />
                    )
                }}
            />
            <Tab.Screen
                name="評價"
                component={RatingHistoryList}
                options={{
                    headerShown: false,
                    tabBarLabelStyle: {
                        fontSize: customTabBarLabelStyle.fontSize,
                        fontWeight: customTabBarLabelStyle.fontWeight
                    },
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="comment-account-outline" color={color} size={customTabBarIcon.size} />
                    )
                }}
            />
            <Tab.Screen
                name="設定"
                component={DriverSettingStackScreen}
                options={{
                    headerShown: false,
                    tabBarLabelStyle: {
                        fontSize: customTabBarLabelStyle.fontSize,
                        fontWeight: customTabBarLabelStyle.fontWeight
                    },
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="account-settings" color={color} size={customTabBarIcon.size} />
                    ),
                }}
            />
        </Tab.Navigator>
    );
}