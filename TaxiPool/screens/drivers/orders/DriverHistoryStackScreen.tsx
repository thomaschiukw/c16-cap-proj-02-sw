import * as React from 'react';
import { createNativeStackNavigator, NativeStackScreenProps } from "@react-navigation/native-stack";
// import DriverHistoryList from './DriverHistoryList';
import DriverHistoryList from './DriverHistoryList';
import DriverEventListDetail from './DriverEventListDetail';
interface Order {
    id: number;
    origin_place_id: string;
    origin_lat: number;
    origin_lng: number;
    origin_district: string;
    origin_description: string;
    destination_place_id: string;
    destination_lat: number;
    destination_lng: number;
    destination_district: string;
    destination_description: string;
    driver_session_id?: any;
    fare: string;
    distance: string;
    pool: boolean;
    pool_deadline?: any;
    hybrid: boolean;
    red_taxi: boolean;
    green_taxi: boolean;
    blue_taxi: boolean;
    cross_harbour_tunnel: boolean;
    eastern_harbour_crossing: boolean;
    western_harbour_crossing: boolean;
    has_pet: boolean;
    smoke_free: boolean;
    has_luggage: boolean;
    is_disabled: boolean;
    urgent: boolean;
    status: string;
    remarks?: any;
    created_at: Date;
    updated_at: Date;
    name?: string;
    rego?: string;
}

export type DriverHistoryStackScreenParamList = {
    History: undefined;
    HistoryDetail: { order: Order };
    // Accepted: undefined;
};

export type DriverHistoryListDetailProp = NativeStackScreenProps<DriverHistoryStackScreenParamList, 'HistoryDetail'>;

const Stack = createNativeStackNavigator<DriverHistoryStackScreenParamList>();

export function DriverHistoryStackScreen() {
    return (
        <Stack.Navigator
            initialRouteName="History"

        >
            <Stack.Screen
                name="History"
                component={DriverHistoryList}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="HistoryDetail"
                component={DriverEventListDetail}
                options={{ title: "" }}

            />
            {/* <Stack.Screen
                name="Accepted"
                component={DriverAcceptedDetail}
            // options={{ title: "Back" }}

            /> */}
        </Stack.Navigator>
    );
};

