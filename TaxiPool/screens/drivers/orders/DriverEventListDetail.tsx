//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import React, { useState, useEffect } from "react";
import { ScrollView, StyleSheet, Text, TouchableOpacity, View, Modal, Pressable, Alert } from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { SafeAreaView } from "react-native-safe-area-context";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from '../../../store';
import { fetchAcceptOrder } from "../../../redux/driverOrder/thunk";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { DriverEventListDetailProp } from "./DriverEventStackScreen";
import { NavigationProp, TabActions, useNavigation, useIsFocused } from "@react-navigation/native";
import { StackActions } from "@react-navigation/routers";
import SplashScreen from "../../../components/SplashScreen";
import { DriverInfoState } from "../../../redux/driver/reducer";
import { failed } from '../../../redux/driverOrder/action';


export default function DriverEventListDetail({ route }: DriverEventListDetailProp) {
    const dispatch = useDispatch();
    const navigation: NavigationProp<DriverEventListDetailProp> = useNavigation();
    const order = route.params!.order;

    const driverId = useSelector((state: RootState) => state.auth.user?.id);
    const userInfo = useSelector((state: RootState) => state.driver);
    const isOrderAccepted = useSelector((state: RootState) => state.driverOrder.activeOrder);
    const [modalVisible, setModalVisible] = useState(false);
    const [isLoading, setIsLoading] = useState(0);

    // useEffect(() => {
    //     setIsLoading(1)
    //     if (driverId != undefined) {
    //         dispatch(fetchDriverPendingOrder(driverId));
    //     }
    // }, [isFocused])

    // useEffect(() => {
    //     if (orders.length > 0) {
    //         navigation.dispatch(StackActions.replace('Accepted'));
    //     }
    //     setIsLoading(-1)
    // }, [orders])

    // useEffect(() => {
    //     if (isOrderTaken === "ORDER TAKEN") {
    //         navigation.goBack();
    //     } else if (isOrderTaken === "ORDER ACCEPTED") {
    //         navigation.dispatch(TabActions.jumpTo('地圖'));
    //     }
    // }, [isOrderTaken])

    useEffect(() => {
        if (isOrderAccepted.length !== 0) {
            setIsLoading(-1)
            navigation.dispatch(TabActions.jumpTo('地圖'));
        }
    }, [isOrderAccepted])

    async function updateSearchingOrder(userInfo: DriverInfoState) {
        try {
            // console.log('userInfo: ', userInfo);
            setIsLoading(1)
            const res = await fetch(`${REACT_APP_BACKEND_URL}/order/searching/type/${userInfo.type}/capacity/${userInfo.capacity}/allowPet/${userInfo.pet_ok?.toString()}/disallowSmoke/${userInfo.smoke_free?.toString()}`);
            const result = await res.json();
            // console.log(result);
            if (result.length == 0) {
                dispatch(failed('@@driver_order/ACCEPT_ORDER_FAILED', 'ORDER TAKEN ALREADY'));
                navigation.goBack();
            } else {
                if (driverId) {
                    dispatch(fetchAcceptOrder(driverId, order.id))
                }
            }
        } catch (error) {
            console.log(error);
        }
    }


    return (
        // <SafeAreaView edges={['top', 'left', 'right']} style={styles.container}>
        <ScrollView style={styles.container}>
            {isLoading === 1 && <SplashScreen />}
            {
                <View style={styles.innerContainer}>
                    <View style={styles.locationContainer}>
                        <View style={styles.locationLabelContainer}>
                            <Text style={styles.locationLabel}>起點</Text>
                        </View>
                        <Text style={styles.location}>{order.origin_description}</Text>
                    </View>
                    <View style={styles.locationContainer}>
                        <View style={styles.locationLabelContainer}>
                            <Text style={styles.locationLabel}>目的地</Text>
                        </View>
                        <Text style={styles.location}>{order.destination_description}</Text>
                    </View>

                    <View
                        style={styles.divider}
                    />

                    <View style={styles.section}>
                        <View><Text style={styles.sectionTitle}>訂單詳細</Text></View>
                        <View style={styles.categoryContainer}>
                            <Text style={styles.categoryLabel}>狀態</Text>
                            <View style={styles.categoryElem}>
                                <Text style={styles.text}>
                                    {order.status == 'searching' && <Text>待司機接單</Text>}
                                    {order.status == 'pending' && <Text>等待司機接客</Text>}
                                    {order.status == 'travelling' && <Text>駕駛中</Text>}
                                    {order.status == 'complete' && <Text>已完成</Text>}
                                </Text>
                            </View>
                        </View>
                        {order.pool &&
                            <View style={styles.categoryContainer}>
                                <Text style={styles.categoryLabel}>共乘人數</Text>
                                <View style={styles.categoryElem}>
                                    <Text style={styles.text}>
                                        {order.headcount_sum}
                                    </Text>
                                </View>
                            </View>}


                    </View>

                    <View style={styles.section}>
                        <View><Text style={styles.sectionTitle}>乘客要求</Text></View>
                        <View style={styles.categoryContainer}>
                            <Text style={styles.categoryLabel}>海底隧道</Text>
                            <View style={styles.categoryElem}>
                                {order.cross_harbour_tunnel || order.eastern_harbour_crossing || order.western_harbour_crossing ?
                                    <View></View> : <Text style={styles.text}>不適用</Text>}

                                {order.cross_harbour_tunnel && <View style={styles.iconSelected}>
                                    <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>紅隧</Text>
                                </View>}
                                {order.eastern_harbour_crossing && <View style={styles.iconSelected}>
                                    <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>東隧</Text>
                                </View>}
                                {order.western_harbour_crossing && <View style={styles.iconSelected}>
                                    <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>西隧</Text>
                                </View>}
                            </View>
                        </View>

                        <View style={styles.categoryContainer}>
                            <Text style={styles.categoryLabel}>其他要求</Text>
                            <View style={styles.categoryElem}>
                                {order.has_pet || order.smoke_free || order.has_luggage || order.is_disabled || order.urgent ?
                                    <View></View> : <Text style={styles.text}> 無特別要求</Text>}


                                {order.has_pet && <View style={styles.iconSelected}>
                                    <MaterialIcons name="pets" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>寵物</Text>
                                </View>}

                                {order.smoke_free && <View style={styles.iconSelected}>
                                    <MaterialIcons name="smoke-free" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>不吸煙</Text>
                                </View>}

                                {order.has_luggage && <View style={styles.iconSelected}>
                                    <MaterialIcons name="luggage" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>行李</Text>
                                </View>}

                                {order.is_disabled && <View style={styles.iconSelected}>
                                    <MaterialIcons name="wheelchair-pickup" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>傷殘人仕</Text>
                                </View>}

                                {order.urgent && <View style={styles.iconSelected}>
                                    <MaterialCommunityIcons name="run-fast" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>緊急</Text>
                                </View>}


                            </View>
                        </View>


                    </View>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => {
                            setModalVisible(!modalVisible);
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.modalText}>確認接單?</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Pressable
                                        style={styles.button}
                                        onPress={() => {
                                            if (driverId !== undefined) {
                                                if (driverId) {
                                                    updateSearchingOrder(userInfo);
                                                }
                                                setModalVisible(!modalVisible)
                                            }
                                        }}
                                    >
                                        <Text style={styles.textStyle}>確認</Text>
                                    </Pressable>
                                    <Pressable
                                        style={styles.button}
                                        onPress={() => {
                                            setModalVisible(!modalVisible)
                                        }}
                                    >
                                        <Text style={styles.textStyle}>取消</Text>
                                    </Pressable>
                                </View>
                            </View>
                        </View>
                    </Modal>
                    {order.status == 'searching' &&
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => {
                                setModalVisible(!modalVisible)
                            }}
                            activeOpacity={0.6}
                        >
                            <Text style={styles.buttonText}>接單</Text>
                        </TouchableOpacity>
                    }
                </View>
            }
        </ScrollView>
        // </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    innerContainer: {
        margin: 15
    },
    locationLabelContainer: {
        backgroundColor: '#a4c2a5',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#566246',
        color: '#4a4a48',
        paddingVertical: 10,
        width: 80,
    },
    locationLabel: {
        fontWeight: 'bold',
        textAlign: 'center'
    },
    location: {
        paddingVertical: 15,
        color: '#4a4a48',
        fontSize: 28,
        flexWrap: 'wrap',
        fontWeight: 'bold',
    },
    locationContainer: {
        marginHorizontal: 10,
        // backgroundColor: '#a4c2a5',
        // flexDirection: 'column'
    },
    categoryContainer: {
        marginBottom: 10
    },
    categoryLabel: {
        fontWeight: "bold",
        marginHorizontal: 10,
        paddingVertical: 10,
        color: '#4a4a48',
        fontSize: 20
    },
    categoryElem: {
        flexDirection: "row",
        // marginVertical: 5,
        flexWrap: "wrap"
    },
    section: {
        backgroundColor: '#d8dad3',
        marginVertical: 10,
        borderRadius: 10,
        padding: 10,
    },
    sectionTitle: {
        backgroundColor: '#f1f2eb',
        borderRadius: 10,
        color: '#4a4a48',
        paddingVertical: 5,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 25,
    },
    divider: {

        borderBottomColor: '#566246',
        borderBottomWidth: 1,
        opacity: 0.7,
        margin: 10
    },
    iconSelected: {
        opacity: 1,
        marginBottom: 10,
    },
    iconDeselected: {
        opacity: 0.2,
        marginBottom: 15,
    },
    icon: {
        marginHorizontal: 10,
        fontSize: 50,
        color: '#4a4a48'
    },
    iconLabel: {
        textAlign: "center",
        fontWeight: "600",
        color: "black",
        fontSize: 17
    },
    button: {
        alignItems: "center",
        backgroundColor: "#566246",
        padding: 10,
        margin: 5,
        borderRadius: 10,

    },
    buttonText: {
        textAlign: 'center',
        color: '#f1f2eb',
        fontWeight: 'bold',
    },
    inputContainer: {
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        flexDirection: 'row',
        flexWrap: "wrap",
    },
    input: {
        backgroundColor: 'white',
        marginVertical: 5,
        height: 40,
        width: '75%',
        borderRadius: 5,
    },
    buttons: {
        backgroundColor: '#566246',
        margin: 5,
        padding: '3%',
        borderRadius: 10,
        width: 250,
    },
    text: {
        paddingLeft: 20
    },
    //modal
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        paddingHorizontal: 35,
        paddingVertical: 15,

        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    // modalButton: {
    //     backgroundColor: '#a4c2a5'
    // },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});