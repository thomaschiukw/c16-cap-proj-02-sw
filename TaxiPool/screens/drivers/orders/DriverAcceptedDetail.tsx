import React, { useState } from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { SafeAreaView } from "react-native-safe-area-context";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from '../../../store';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { DriverEventListDetailProp } from "../orders/DriverEventStackScreen";
import { NavigationProp, useNavigation } from "@react-navigation/core";
import SplashScreen from "../../../components/SplashScreen";

export default function DriverAcceptedDetail() {
    // const dispatch = useDispatch();
    // const driverId = useSelector((state: RootState) => state.auth.user?.id);
    // const navigation: NavigationProp<DriverEventListDetailProp> = useNavigation();
    const orders = useSelector((state: RootState) => state.driverOrder.activeOrder);
    const [isLoading, setIsLoading] = useState(-1);

    // console.log('accept detail: ', order);

    // useEffect(() => {
    //     // dispatch(fetchDriverSession(driverId))
    //     if (driverId != undefined) {
    //         dispatch(fetchDriverPendingOrder(driverId));
    //         // console.log(orders);

    //         // if (!(orders.length > 0)) {
    //         //     navigation.dispatch(StackActions.replace('接單'));

    //         // }
    //     }
    // }, [dispatch])

    // useEffect(() => {
    //     if (orders.length == 0) {

    //         navigation.dispatch(StackActions.replace('Event'));

    //     }
    //     // else {
    //     //     navigation.dispatch(StackActions.replace('接單'));

    //     // }
    // }, [orders])


    return (
        <SafeAreaView edges={['top', 'left', 'right']} style={styles.container}>
            {/* {isLoading >= 0 && <SplashScreen />} */}
            {/* {isLoading === 1 && <Text>Loading...</Text>} */}
            {orders.length > 0 &&
                <ScrollView style={styles.container}>
                    {isLoading <= -1 && orders[0].id != undefined && <View style={styles.innerContainer}>
                        <View style={styles.locationContainer}>
                            <View style={styles.locationLabelContainer}>
                                <Text style={styles.locationLabel}>起點</Text>
                            </View>
                            <Text style={styles.location}>{orders[0].origin_description}</Text>
                        </View>
                        <View style={styles.locationContainer}>
                            <View style={styles.locationLabelContainer}>
                                <Text style={styles.locationLabel}>目的地</Text>
                            </View>
                            <Text style={styles.location}>{orders[0].destination_description}</Text>
                        </View>

                        {/* <View style={styles.divider} /> */}

                        <View style={styles.section}>
                            <View><Text style={styles.sectionTitle}>訂單詳細</Text></View>
                            <View style={styles.categoryContainer}>
                                <Text style={styles.categoryLabel}>狀態</Text>
                                <View style={styles.categoryElem}>
                                    <Text style={styles.text}>
                                        {orders[0].status == 'searching' && <Text>待接單</Text>}
                                        {orders[0].status == 'pending' && <Text>等待司機接客</Text>}
                                        {orders[0].status == 'travelling' && <Text>駕駛中</Text>}
                                        {orders[0].status == 'complete' && <Text>已完成</Text>}
                                        {orders[0].status == 'expired' && <Text>過時</Text>}
                                    </Text>
                                </View>
                            </View>

                        </View>

                        <View style={styles.section}>
                            <View><Text style={styles.sectionTitle}>乘客要求</Text></View>
                            <View style={styles.categoryContainer}>
                                <Text style={styles.categoryLabel}>海底隧道</Text>
                                <View style={styles.categoryElem}>
                                    {orders[0].cross_harbour_tunnel || orders[0].eastern_harbour_crossing || orders[0].western_harbour_crossing ?
                                        <View></View> : <Text style={styles.text}>不適用</Text>}

                                    {orders[0].cross_harbour_tunnel && <View style={styles.iconSelected}>
                                        <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>紅隧</Text>
                                    </View>}
                                    {orders[0].eastern_harbour_crossing && <View style={styles.iconSelected}>
                                        <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>東隧</Text>
                                    </View>}
                                    {orders[0].western_harbour_crossing && <View style={styles.iconSelected}>
                                        <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>西隧</Text>
                                    </View>}
                                </View>
                            </View>

                            <View style={styles.categoryContainer}>
                                <Text style={styles.categoryLabel}>其他要求</Text>
                                <View style={styles.categoryElem}>
                                    {orders[0].has_pet || orders[0].smoke_free || orders[0].has_luggage || orders[0].is_disabled || orders[0].urgent ?
                                        <View></View> : <Text style={styles.text}> 無特別要求</Text>}


                                    {orders[0].has_pet && <View style={styles.iconSelected}>
                                        <MaterialIcons name="pets" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>寵物</Text>
                                    </View>}

                                    {orders[0].smoke_free && <View style={styles.iconSelected}>
                                        <MaterialIcons name="smoke-free" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>不吸煙</Text>
                                    </View>}

                                    {orders[0].has_luggage && <View style={styles.iconSelected}>
                                        <MaterialIcons name="luggage" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>行李</Text>
                                    </View>}

                                    {orders[0].is_disabled && <View style={styles.iconSelected}>
                                        <MaterialIcons name="wheelchair-pickup" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>傷殘人仕</Text>
                                    </View>}

                                    {orders[0].urgent && <View style={styles.iconSelected}>
                                        <MaterialCommunityIcons name="run-fast" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>緊急</Text>
                                    </View>}


                                </View>
                            </View>
                        </View>
                    </View>}
                </ScrollView >
            }
        </SafeAreaView >

        // <SafeAreaView edges={['top', 'left', 'right']} style={styles.container}>
        //     <ScrollView style={styles.container}>
        //         <View style={styles.innerContainer}>
        //             <View style={styles.categoryContainer}>
        //                 <Text style={styles.locationLabel}>起點</Text>
        //                 <Text style={styles.location}>{orders[0].origin_description}</Text>
        //             </View>
        //             <View style={styles.categoryContainer}>
        //                 <Text style={styles.locationLabel}>目的地</Text>
        //                 <Text style={styles.location}>{orders[0].destination_description}</Text>
        //             </View>
        //             <View
        //                 style={{
        //                     borderBottomColor: '#d8dad3',
        //                     borderBottomWidth: 1,
        //                 }}
        //             />

        //             <View style={styles.categoryElem}>
        //                 <Text style={styles.categoryLabel}>海底隧道</Text>
        //                 <View style={styles.categoryContainer}>
        //                     {orders[0].cross_harbour_tunnel || orders[0].eastern_harbour_crossing || orders[0].western_harbour_crossing ?
        //                         <View></View> : <Text style={{ paddingLeft: 20 }}>不適用</Text>}

        //                     {orders[0].cross_harbour_tunnel ? <View style={styles.iconSelected}>
        //                         <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
        //                         <Text style={styles.iconLabel}>紅隧</Text>
        //                     </View> : <View></View>}
        //                     {orders[0].eastern_harbour_crossing ? <View style={styles.iconSelected}>
        //                         <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
        //                         <Text style={styles.iconLabel}>東隧</Text>
        //                     </View> : <View></View>}
        //                     {orders[0].western_harbour_crossing ? <View style={styles.iconSelected}>
        //                         <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
        //                         <Text style={styles.iconLabel}>西隧</Text>
        //                     </View> : <View></View>}


        //                 </View>
        //             </View>

        //             <View style={styles.categoryElem}>
        //                 <Text style={styles.categoryLabel}>其他</Text>
        //                 <View style={styles.categoryContainer}>
        //                     {orders[0].has_pet || orders[0].smoke_free || orders[0].has_luggage || orders[0].is_disabled || orders[0].urgent ?
        //                         <View></View> : <Text style={{ paddingLeft: 20 }}> 無特別要求</Text>}


        //                     {orders[0].has_pet ? <View style={styles.iconSelected}>
        //                         <MaterialIcons name="pets" color="black" style={styles.icon} />
        //                         <Text style={styles.iconLabel}>寵物</Text>
        //                     </View> : <View></View>}

        //                     {orders[0].smoke_free ? <View style={styles.iconSelected}>
        //                         <MaterialIcons name="smoke-free" color="black" style={styles.icon} />
        //                         <Text style={styles.iconLabel}>不吸煙</Text>
        //                     </View> : <View></View>}

        //                     {orders[0].has_luggage ? <View style={styles.iconSelected}>
        //                         <MaterialIcons name="luggage" color="black" style={styles.icon} />
        //                         <Text style={styles.iconLabel}>行李</Text>
        //                     </View> : <View></View>}

        //                     {orders[0].is_disabled ? <View style={styles.iconSelected}>
        //                         <MaterialIcons name="wheelchair-pickup" color="black" style={styles.icon} />
        //                         <Text style={styles.iconLabel}>傷殘人仕</Text>
        //                     </View> : <View></View>}

        //                     {orders[0].urgent ? <View style={styles.iconSelected}>
        //                         <MaterialCommunityIcons name="run-fast" color="black" style={styles.icon} />
        //                         <Text style={styles.iconLabel}>緊急</Text>
        //                     </View> : <View></View>}


        //                 </View>
        //             </View>


        //         </View>
        //     </ScrollView>

        // </SafeAreaView >

    )
}

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//     },
//     innerContainer: {
//         margin: 20
//     },
//     locationLabel: {
//         backgroundColor: '#a4c2a5',
//         borderRadius: 10,
//         borderWidth: 1,
//         borderColor: '#566246',
//         color: '#4a4a48',
//         paddingVertical: 15,
//         width: 100,
//         textAlign: 'center'
//     },
//     location: {
//         padding: 15,
//         color: '#4a4a48'
//     },
//     categoryElem: {
//         marginBottom: 10
//     },
//     categoryLabel: {
//         fontWeight: "bold",
//         marginHorizontal: 10,
//         paddingVertical: 10,
//         color: '#4a4a48'
//     },
//     categoryContainer: {
//         flexDirection: "column",
//         marginVertical: 5,
//         flexWrap: "wrap"
//     },
//     iconSelected: {
//         opacity: 1,
//         marginBottom: 10,
//     },
//     iconDeselected: {
//         opacity: 0.2,
//         marginBottom: 15,
//     },
//     icon: {
//         marginHorizontal: 10,
//         fontSize: 50,
//         color: '#4a4a48'
//     },
//     iconLabel: {
//         textAlign: "center",
//         fontWeight: "600",
//         color: "black"
//     },
//     button: {
//         alignItems: "center",
//         backgroundColor: "#566246",
//         padding: 10,
//         marginVertical: 5,
//         borderRadius: 10,
//     },
//     buttonText: {
//         textAlign: 'center',
//         color: '#f1f2eb',
//         fontWeight: 'bold',
//     },
//     inputContainer: {
//         marginTop: 5,
//         marginBottom: 5,
//         marginLeft: 10,
//         flexDirection: 'row',
//         flexWrap: "wrap",
//     },
//     input: {
//         backgroundColor: 'white',
//         marginVertical: 5,
//         height: 40,
//         width: '75%',
//         borderRadius: 5,
//     },
//     buttons: {
//         backgroundColor: '#566246',
//         margin: 5,
//         padding: '3%',
//         borderRadius: 10,
//         width: 250,
//     },
//     //modal
//     centeredView: {
//         flex: 1,
//         justifyContent: "center",
//         alignItems: "center",
//         marginTop: 22
//     },
//     modalView: {
//         margin: 20,
//         backgroundColor: "white",
//         borderRadius: 20,
//         paddingHorizontal: 35,
//         paddingVertical: 15,

//         alignItems: "center",
//         shadowColor: "#000",
//         shadowOffset: {
//             width: 0,
//             height: 2
//         },
//         shadowOpacity: 0.25,
//         shadowRadius: 4,
//         elevation: 5
//     },
//     // modalButton: {
//     //     backgroundColor: '#a4c2a5'
//     // },
//     textStyle: {
//         color: "white",
//         fontWeight: "bold",
//         textAlign: "center"
//     },
//     modalText: {
//         marginBottom: 15,
//         textAlign: "center"
//     }
// });
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    innerContainer: {
        margin: 15
    },
    locationLabelContainer: {
        backgroundColor: '#a4c2a5',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#566246',
        color: '#4a4a48',
        paddingVertical: 10,
        width: 80,
    },
    locationLabel: {
        fontWeight: 'bold',
        textAlign: 'center'
    },
    location: {
        paddingVertical: 15,
        color: '#4a4a48',
        fontSize: 28,
        flexWrap: 'wrap',
        fontWeight: 'bold',
    },
    locationContainer: {
        marginHorizontal: 10,
        // backgroundColor: '#a4c2a5',
        // flexDirection: 'column'
    },
    categoryContainer: {
        marginBottom: 10
    },
    categoryLabel: {
        fontWeight: "bold",
        marginHorizontal: 10,
        paddingVertical: 10,
        color: '#4a4a48',
        fontSize: 20
    },
    categoryElem: {
        flexDirection: "row",
        // marginVertical: 5,
        flexWrap: "wrap"
    },
    divider: {
        borderBottomColor: '#566246',
        borderBottomWidth: 1,
        opacity: 0.7,
        margin: 10
    },
    section: {
        backgroundColor: '#d8dad3',
        marginVertical: 10,
        borderRadius: 10,
        padding: 10,
    },
    sectionTitle: {
        backgroundColor: '#f1f2eb',
        borderRadius: 10,
        color: '#4a4a48',
        paddingVertical: 5,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 25,
    },
    iconSelected: {
        opacity: 1,
        marginBottom: 10,
        marginHorizontal: 20
    },
    iconDeselected: {
        opacity: 0.2,
        marginBottom: 15,
    },
    icon: {
        marginHorizontal: 10,
        fontSize: 50,
        color: '#4a4a48'
    },
    iconLabel: {
        textAlign: "center",
        fontWeight: "600",
        color: "black",
        fontSize: 17,
    },
    button: {
        alignItems: "center",
        backgroundColor: "#566246",
        padding: 10,
        marginVertical: 5,
        borderRadius: 10,
    },
    buttonText: {
        textAlign: 'center',
        color: '#f1f2eb',
        fontWeight: 'bold',
    },
    inputContainer: {
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        flexDirection: 'row',
        flexWrap: "wrap",
    },
    input: {
        backgroundColor: 'white',
        marginVertical: 5,
        height: 40,
        width: '75%',
        borderRadius: 5,
    },
    buttons: {
        backgroundColor: '#566246',
        margin: 5,
        padding: '3%',
        borderRadius: 10,
        width: 250,
    },
    text: {
        marginHorizontal: 20
    },
});