import { NavigationProp, StackActions, useNavigation } from "@react-navigation/core";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import SplashScreen from "../../../components/SplashScreen";
import { fetchDriverActiveOrder, fetchSearchingOrder } from "../../../redux/driverOrder/thunk";
import { RootState } from "../../../store";
import { DriverEventStackScreenParamList } from "./DriverEventStackScreen";

export default function DriverEventLoadingScreen() {
    const dispatch = useDispatch();
    const navigation: NavigationProp<DriverEventStackScreenParamList> = useNavigation();
    const driverId = useSelector((state: RootState) => state.auth.user?.id);
    const driverActiveOrder = useSelector((state: RootState) => state.driverOrder.activeOrder);
    const userInfo = useSelector((state: RootState) => state.driver);
    const [rendered, setRendered] = useState(0);
    useEffect(() => {
        if (driverId) {
            dispatch(fetchDriverActiveOrder(driverId));
        }
        setRendered(state => state + 1);
    }, [])

    useEffect(() => {
        if (rendered === 1) {
            if (userInfo.active_session === true) {
                if (driverActiveOrder.length === 0) {
                    navigation.dispatch(StackActions.replace('Event'));
                } else {
                    navigation.dispatch(StackActions.replace('Accepted'));
                }
            } else {
                navigation.dispatch(StackActions.replace('Denied'));
            }

        }

    }, [driverActiveOrder])

    return (
        <SplashScreen />
    )
}