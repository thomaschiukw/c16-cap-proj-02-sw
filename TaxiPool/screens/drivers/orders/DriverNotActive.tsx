import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";

export default function DriverNotActive() {
    return (
        <View style={styles.container}>
            <Image style={styles.noResultLogo} source={require('../../src/notWorking.png')} />
            <Text>你還未開工</Text>
            <Text>請點擊地圖頁的開工按鈕</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignSelf: "center",
        justifyContent: "center",
        alignItems: 'center',
    },
    noResultLogo: {
        width: 200,
        height: 200,
        resizeMode: 'contain',
        alignSelf: "center",
    }
})