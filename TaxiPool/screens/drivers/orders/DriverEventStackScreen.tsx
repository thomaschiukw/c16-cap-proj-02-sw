import * as React from 'react';
import { createNativeStackNavigator, NativeStackScreenProps } from "@react-navigation/native-stack";
import DriverEventList from './DriverEventList';
import DriverEventListDetail from './DriverEventListDetail';
import DriverAcceptedDetail from './DriverAcceptedDetail';
import DriverEventLoadingScreen from './DriverEventLoadingScreen';
import DriverNotActive from './DriverNotActive';

export type DriverEventStackScreenParamList = {
    Loading: undefined;
    Event: undefined;
    Detail: undefined | { order: SearchingOrder };
    Accepted: undefined;
    Current: undefined;
    Denied: undefined;
};

export interface SearchingOrder {
    id: number
    origin_place_id: string
    origin_lat: number
    origin_lng: number
    origin_district: string
    origin_description: string
    destination_place_id: string
    destination_lat: number
    destination_lng: number
    destination_district: string
    destination_description: string
    driver_session_id: any
    fare: string
    distance: string
    pool: boolean
    pool_deadline: any
    hybrid: boolean
    red_taxi: boolean
    green_taxi: boolean
    blue_taxi: boolean
    cross_harbour_tunnel: boolean
    eastern_harbour_crossing: boolean
    western_harbour_crossing: boolean
    has_pet: boolean
    smoke_free: boolean
    has_luggage: boolean
    is_disabled: boolean
    urgent: boolean
    status: string
    remarks: any
    created_at: string
    updated_at: string
    headcount_sum: string
}

export type DriverEventListDetailProp = NativeStackScreenProps<DriverEventStackScreenParamList, 'Detail'>;

const Stack = createNativeStackNavigator<DriverEventStackScreenParamList>();

export function DriverEventStackScreen() {
    return (
        <Stack.Navigator
            initialRouteName="Loading"

        >
            <Stack.Screen
                name="Loading"
                component={DriverEventLoadingScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="Event"
                component={DriverEventList}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="Detail"
                component={DriverEventListDetail}
                options={{ title: "" }}

            />
            <Stack.Screen
                name="Accepted"
                component={DriverAcceptedDetail}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="Denied"
                component={DriverNotActive}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
};