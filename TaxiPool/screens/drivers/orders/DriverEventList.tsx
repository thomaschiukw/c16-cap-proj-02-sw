//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { DriverEventStackScreenParamList, SearchingOrder } from './DriverEventStackScreen';
import { RootState } from '../../../store';
import { socket } from '../../../App';
import SplashScreen from '../../../components/SplashScreen';
import { SafeAreaView } from 'react-native-safe-area-context';
import NoResult from '../../noResult/noResult';
import { fetchSearchingOrder } from '../../../redux/driverOrder/thunk';

export default function DriverEventList() {
    const dispatch = useDispatch();
    const navigation: NavigationProp<DriverEventStackScreenParamList> = useNavigation();
    const [orderView, setorderView] = useState([true, false, false]);

    // const userId = useSelector((state: RootState) => state.auth.user?.id);
    // const userInfo = useSelector((state: RootState) => state.driver);
    const driverOrdersError = useSelector((state: RootState) => state.driverOrder.error);
    const searchingOrder: SearchingOrder[] = useSelector((state: RootState) => state.driverOrder.searchingOrders);
    const [rendered, setRendered] = useState(0);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        dispatch(fetchSearchingOrder());
        setRendered(state => state + 1);
    }, [])

    useEffect(() => {
        if (rendered === 1) {
            setIsLoading(false);
        }
    }, [rendered])

    useEffect(() => {
        socket.on("order-created", async () => {
            dispatch(fetchSearchingOrder());
        })

        socket.on("order-accepted", async () => {
            dispatch(fetchSearchingOrder());
        })
    }, [])

    // async function fetchSearchingOrder(userInfo: DriverInfoState) {
    //     try {
    //         // console.log('userInfo: ', userInfo);
    //         const res = await fetch(`${REACT_APP_BACKEND_URL}/order/searching/type/${userInfo.type}/capacity/${userInfo.capacity}/allowPet/${userInfo.pet_ok?.toString()}/disallowSmoke/${userInfo.smoke_free?.toString()}`);

    //         const result = await res.json();
    //         await setSearchingOrder(result);
    //         setRendered(state => state + 1);
    //     } catch (error) {
    //         console.log(error);
    //     }
    // }

    return (
        <SafeAreaView style={styles.container}>
            {isLoading && <SplashScreen />}
            {!isLoading && searchingOrder.length === 0 && <NoResult />}
            {!isLoading && searchingOrder.length !== 0 && !driverOrdersError &&
                <ScrollView>
                    <View style={styles.innerContainer}>
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity
                                // underlayColor={''}
                                onPress={() => setorderView([true, false, false])}
                            >
                                <View style={orderView[0] ? styles.selectedFilterBtn : styles.deselectedFilterBtn}                           >
                                    <Text style={orderView[0] ? styles.iconSelected : styles.iconDeselected}>全部</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                // underlayColor={''}
                                onPress={() => setorderView([false, true, false])}
                            >
                                <View style={orderView[1] ? styles.selectedFilterBtn : styles.deselectedFilterBtn}                           >
                                    <Text style={orderView[1] ? styles.iconSelected : styles.iconDeselected}>共乘</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                // underlayColor={''}
                                onPress={() => setorderView([false, false, true])}
                            >
                                <View style={orderView[2] ? styles.selectedFilterBtn : styles.deselectedFilterBtn}                           >
                                    <Text style={orderView[2] ? styles.iconSelected : styles.iconDeselected}>非共乘</Text>
                                </View>
                            </TouchableOpacity>

                        </View>

                        {orderView[0] &&
                            searchingOrder.map((order) => (
                                <TouchableOpacity
                                    key={order.id}
                                    activeOpacity={0.7}
                                    onPress={() => {
                                        navigation.navigate('Detail', { order: order })
                                    }}
                                >

                                    <View style={styles.eventRow}>
                                        <View style={styles.locationCol}>

                                            <View style={styles.locationRow}>
                                                <Text style={styles.direction}>由 </Text>
                                                <Text style={styles.locationName}>{order.origin_district}</Text>
                                            </View>
                                            <View
                                                style={styles.divider}
                                            />
                                            <View style={styles.locationRow}>
                                                <Text style={styles.direction}>到 </Text>
                                                <Text style={styles.locationName}>{order.destination_district}</Text>
                                            </View>
                                        </View>

                                        <View style={styles.arrowBox}><Text style={styles.arrow}>{'>'}</Text></View>
                                    </View>

                                </TouchableOpacity>
                            ))
                        }
                        {orderView[1] &&
                            searchingOrder.map((order) => (
                                order.pool && <TouchableOpacity
                                    key={order.id}
                                    activeOpacity={0.7}
                                    onPress={() => {
                                        navigation.navigate('Detail', { order: order })
                                    }}
                                >

                                    <View style={styles.eventRow}>
                                        <View style={styles.locationCol}>

                                            <View style={styles.locationRow}>
                                                <Text style={styles.direction}>由 </Text>
                                                <Text style={styles.locationName}>{order.origin_district}</Text>
                                            </View>
                                            <View
                                                style={styles.divider}
                                            />
                                            <View style={styles.locationRow}>
                                                <Text style={styles.direction}>到 </Text>
                                                <Text style={styles.locationName}>{order.destination_district}</Text>
                                            </View>
                                        </View>

                                        <View style={styles.arrowBox}><Text style={styles.arrow}>{'>'}</Text></View>
                                    </View>

                                </TouchableOpacity>
                            ))
                        }
                        {orderView[2] &&
                            searchingOrder.map((order) => (
                                !(order.pool) && <TouchableOpacity
                                    key={order.id}
                                    activeOpacity={0.7}
                                    onPress={() => {
                                        navigation.navigate('Detail', { order: order })
                                    }}
                                >

                                    <View style={styles.eventRow}>
                                        <View style={styles.locationCol}>

                                            <View style={styles.locationRow}>
                                                <Text style={styles.direction}>由 </Text>
                                                <Text style={styles.locationName}>{order.origin_district}</Text>
                                            </View>
                                            <View
                                                style={styles.divider}
                                            />
                                            <View style={styles.locationRow}>
                                                <Text style={styles.direction}>到 </Text>
                                                <Text style={styles.locationName}>{order.destination_district}</Text>
                                            </View>
                                        </View>

                                        <View style={styles.arrowBox}><Text style={styles.arrow}>{'>'}</Text></View>
                                    </View>

                                </TouchableOpacity>
                            ))
                        }
                    </View>
                </ScrollView >
            }
        </SafeAreaView >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    innerContainer: {
        margin: 20
    },
    eventRow: {
        backgroundColor: '#a4c2a5',
        borderRadius: 15,
        marginVertical: 5,
        padding: 10,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#566246'
    },
    direction: {
        fontSize: 20,
        padding: 6
    },
    locationCol: {
        flex: 10
    },
    locationRow: {
        flexDirection: 'row',
    },
    locationName: {
        fontSize: 30,
        color: 'black',
        flexWrap: 'wrap',
        paddingRight: 15,
    },
    divider: {
        borderBottomColor: '#566246',
        borderBottomWidth: 1,
        opacity: 0.3,
        margin: 5
    },
    arrowBox: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'

    },
    arrow: {
        fontSize: 30
    },
    buttonContainer: {
        // backgroundColor: 'blue',
        // paddingTop: 100,
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    iconSelected: {
        color: '#f1f2eb',
        paddingHorizontal: 20,
        paddingVertical: 10,

    },
    iconDeselected: {
        color: '#4a4a48',
        paddingHorizontal: 20,
        paddingVertical: 10,

    },
    selectedFilterBtn: {
        backgroundColor: '#566246',
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#a4c2a5'
    },
    deselectedFilterBtn: {
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#d8dad3',
        backgroundColor: '#d8dad3',
    }
});