import React, { useState, useEffect } from 'react';
import { Key } from 'react';
import { ScrollView, StyleSheet, TouchableOpacity, Pressable, View, Text, SafeAreaView } from 'react-native';
import { DataTable } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { fetchDriverHistoryOrder } from '../../../redux/driverOrder/thunk';
import { NavigationProp, useIsFocused, useNavigation } from '@react-navigation/native';
import { DriverHistoryStackScreenParamList } from './DriverHistoryStackScreen';
import { RootState } from '../../../store';
import { Order } from '../../../redux/driverOrder/reducer';
import NoResult from '../../noResult/noResult';
//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';

export default function DriverHistoryList() {
    const navigation: NavigationProp<DriverHistoryStackScreenParamList> = useNavigation();
    const [orderView, setorderView] = useState([true, false, false]);
    const driverId = useSelector((state: RootState) => state.auth.user?.id);
    const isFocused = useIsFocused()
    const dispatch = useDispatch();
    // const orders = useSelector((state: RootState) => state.viewOrder.history);

    const [orders, setOrders] = useState([])
    const userInfo = useSelector((state: RootState) => state.driver);

    // useEffect(() => {
    //     if (driverId != undefined) {
    //         dispatch(fetchDriverHistoryOrder(driverId));
    //         console.log(orders);
    //         return
    //     }
    // }, [isFocused])

    async function loadData(userId: number) {
        try {

            let res = await fetch(`${REACT_APP_BACKEND_URL}/order/completed/driver/${userId}`)

            const result = await res.json()
            // console.log(result);

            await setOrders(result)
            // setRendered(state => state + 1);

        } catch (error) {
            console.log(error);
        }

    }

    useEffect(() => {
        if (driverId) {
            loadData(driverId)
        }
        // if (userId) {
        //     dispatch(fetchSearchingOrder(userId));
        //     setRendered(state => state + 1);
        // }
    }, [isFocused])

    const EventRow = (order: Order) => {
        return (
            <TouchableOpacity
                // key={order.id}
                activeOpacity={0.7}
                onPress={() => {
                    navigation.navigate('HistoryDetail', { order: order })
                }}>

                <DataTable.Row

                    style={styles.eventRow}>
                    <DataTable.Cell style={styles.place}>
                        <Text style={styles.eventText}>{order.origin_district}</Text>
                    </DataTable.Cell>
                    <DataTable.Cell style={styles.toEventRow}>
                        <Text style={styles.toEventText}>到</Text>
                    </DataTable.Cell>
                    <DataTable.Cell style={styles.place}>
                        <Text style={styles.eventText}>{order.destination_district}</Text>
                    </DataTable.Cell>
                    <DataTable.Cell style={styles.buttonCell}>

                        <Text>{'>'}</Text>

                    </DataTable.Cell>
                </DataTable.Row>

            </TouchableOpacity>
        )
    }
    return (
        <SafeAreaView edges={['top', 'left', 'right']} style={styles.container}>
            {/* <ScrollView style={styles.container}> */}
            {orders.length == 0 ? <NoResult /> :
                <View>
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity
                            // underlayColor={''}
                            onPress={() => setorderView([true, false, false])}
                        >
                            <View style={orderView[0] ? styles.selectedFilterBtn : styles.deselectedFilterBtn}                           >
                                <Text style={orderView[0] ? styles.iconSelected : styles.iconDeselected}>全部</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            // underlayColor={''}
                            onPress={() => setorderView([false, true, false])}
                        >
                            <View style={orderView[1] ? styles.selectedFilterBtn : styles.deselectedFilterBtn}                           >
                                <Text style={orderView[1] ? styles.iconSelected : styles.iconDeselected}>完成</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            // underlayColor={''}
                            onPress={() => setorderView([false, false, true])}
                        >
                            <View style={orderView[2] ? styles.selectedFilterBtn : styles.deselectedFilterBtn}                           >
                                <Text style={orderView[2] ? styles.iconSelected : styles.iconDeselected}>過時</Text>
                            </View>
                        </TouchableOpacity>

                    </View>

                    <DataTable style={styles.DataTableContainer}>
                        <DataTable.Header style={styles.eventHeader}>
                            <DataTable.Title style={styles.place}>起點</DataTable.Title>
                            <DataTable.Title > </DataTable.Title>
                            <DataTable.Title style={styles.place}>目的地</DataTable.Title>
                            <DataTable.Title > </DataTable.Title>
                        </DataTable.Header>

                        {orderView[0] && orders.map((order: any) => (
                            <EventRow  {...order}
                                key={order.id} />))}

                        {orderView[1] && ((orders.filter(order => order.status == 'complete')).length ?
                            orders.map((order: any) => (
                                order.status == 'complete' && <EventRow  {...order}
                                    key={order.id} />)) : <NoResult />)}


                        {orderView[2] && ((orders.filter(order => order.status == 'expired')).length ?
                            orders.map((order: any) => (
                                order.status == 'expired' && <EventRow  {...order}
                                    key={order.id} />)) : <NoResult />)}


                        {/* {orderView[0] ?
                    orders.map((order: any) => (
                        <EventRow  {...order}
                            key={order.id} />)) :

                    orderView[1] ?
                        orders.map((order: any) => (
                            order.status == 'complete' ? <EventRow  {...order} key={order.id} /> : <Text style={{ display: 'none' }}> </Text>)) :

                        orders.map((order: any) => (
                            order.status == 'expired' ? <EventRow  {...order} key={order.id} /> : <Text style={{ display: 'none' }}> </Text>))} */}

                    </DataTable>
                </View>
            }
            {/* </ScrollView > */}
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {

        flex: 1,
        justifyContent: 'center',
        // paddingTop: 30,
        // paddingHorizontal: 30,
        // justifyContent: 'center'
        // backgroundColor: '#a4c2a5',
    },

    eventHeader: {
        borderColor: '#f1f2eb',
    },
    eventRow: {
        backgroundColor: '#d8dad3',
        borderRadius: 15,
        marginVertical: 5,
        padding: 10,
        flexDirection: 'row',
        // borderWidth: 1,
        // borderColor: '#566246'
        // height: '30%',
    },
    direction: {
        fontSize: 20,
        padding: 6
    },
    locationCol: {
        flex: 10
    },
    locationRow: {
        flexDirection: 'row',
    },
    locationName: {
        fontSize: 30,
        color: 'black',
        flexWrap: 'wrap',
        // backgroundColor: 'white',
        // paddingHorizontal: 15,
        paddingRight: 15,
    },
    divider: {
        borderBottomColor: '#566246',
        borderBottomWidth: 1,
        opacity: 0.3,
        margin: 5
    },
    arrowBox: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'

    },
    arrow: {
        fontSize: 30
    },
    eventText: {
        color: '#4a4a48',
        flexWrap: 'wrap',
        fontSize: 20,
    },
    buttonCell: {
        justifyContent: 'center',
    },
    buttonText: {
        color: 'white',
    },
    button: {
        backgroundColor: '#566246',
        borderRadius: 13,
        justifyContent: 'center',
        paddingHorizontal: 15,
        paddingVertical: 7,
    },
    buttonContainer: {
        // backgroundColor: 'blue',
        paddingTop: 100,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    selectbutton: {
        // backgroundColor: '#566246',
        borderRadius: 13,
        justifyContent: 'center',
        paddingHorizontal: 35,
        paddingVertical: 15,
        borderWidth: 2,
    },
    selectButtonText: {
        color: 'black',
        fontSize: 18,
    },
    place: {
        // backgroundColor: 'red',
        justifyContent: 'center',
        fontSize: 18,
    },
    toEventText: {
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
    },
    toEventRow: {
        justifyContent: 'center',
    },
    DataTableContainer: {
        // backgroundColor: 'orange',
        height: '100%',
        paddingHorizontal: 20,
    },
    iconSelected: {
        color: '#f1f2eb',
        paddingHorizontal: 20,
        paddingVertical: 10,

    },
    iconDeselected: {
        color: '#4a4a48',
        paddingHorizontal: 20,
        paddingVertical: 10,

    },
    selectedFilterBtn: {
        backgroundColor: '#566246',
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#a4c2a5'
    },
    deselectedFilterBtn: {
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#d8dad3',
        backgroundColor: '#d8dad3',
    }
});
