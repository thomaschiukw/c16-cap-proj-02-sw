import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { fetchDriverCompleteOrder, fetchDriverStartOrder } from "../../../redux/driverOrder/thunk";
import { RootState } from "../../../store";

export default function DriverMapInfo() {
    const dispatch = useDispatch();
    const driverId = useSelector((state: RootState) => state.auth.user?.id);
    const driverActiveOrder = useSelector((state: RootState) => state.driverOrder.activeOrder);

    return (
        <View style={styles.container}>
            {driverActiveOrder.length !== 0 && <View style={styles.innerContainer}>
                <View style={styles.infoContainer}>
                    <Text>{driverActiveOrder[0].origin_description}</Text>
                </View>
                <View style={styles.infoContainer}>
                    <Text>{driverActiveOrder[0].destination_description}</Text>
                </View>
                <View style={styles.btnContainer}>
                    {driverActiveOrder[0].status === "pending" && <TouchableOpacity
                        style={styles.btnElem}
                        activeOpacity={0.8}
                        onPress={() => dispatch(fetchDriverStartOrder())}
                    >
                        <Text style={styles.btnText}>開始</Text>
                    </TouchableOpacity>}
                    {driverActiveOrder[0].status === "travelling" && <TouchableOpacity
                        style={styles.btnElem}
                        activeOpacity={0.8}
                        onPress={() => dispatch(fetchDriverCompleteOrder())}
                    >
                        <Text style={styles.btnText}>完成</Text>
                    </TouchableOpacity>}
                </View>
            </View>}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    innerContainer: {
        marginVertical: 20,
        marginHorizontal: 15,
    },
    infoContainer: {
        // backgroundColor: 'white',
        backgroundColor: 'rgba(212,215,193,0.9)',
        borderWidth: 1,
        borderColor: '#566246',
        borderRadius: 5,
        paddingVertical: 5,
        paddingLeft: 10,
        paddingRight: 30,
        fontSize: 16,
        marginTop: 10,
    },
    btnContainer: {
        marginTop: 10,
        flexDirection: "row",
        justifyContent: 'space-around',
    },
    btnElem: {
        backgroundColor: '#566246',
        borderRadius: 200,
        paddingHorizontal: 50,
        paddingVertical: 8
    },
    btnText: {
        color: 'white',
        fontWeight: 'bold'
    }
})