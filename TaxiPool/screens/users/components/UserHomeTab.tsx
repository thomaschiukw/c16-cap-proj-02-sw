import React from "react";
import { NavigatorScreenParams } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { BookingStackScreen, BookingStackScreenParamList } from "../booking/BookingStackScreen";
import ActiveBooking from "../booking/ActiveBooking";
import { PoolStackScreen } from "../booking/PoolStackScreen";
import { UserHistoryStackScreen, UserHistoryStackScreenParamList } from "../history/UserHistoryStackScreen";
import { UserSettingStackScreen } from "../settings/UserSettingStackScreen";
import { Platform } from "react-native";

export type UserHomeTabParamList = {
    Call的: NavigatorScreenParams<BookingStackScreenParamList>;
    記錄: NavigatorScreenParams<UserHistoryStackScreenParamList>;
    評價: undefined;
    進行中: undefined;
    共乘: undefined;
    設定: undefined;
    測試: undefined;
}

const Tab = createBottomTabNavigator<UserHomeTabParamList>();

interface CustomTabBarLabelStyle {
    fontSize: number;
    fontWeight: "bold" | "normal" | "100" | "200" | "300" | "400" | "500" | "600" | "700" | "800" | "900" | undefined;
};

interface CustomTabBarIcon {
    size: number | undefined;
};

let customTabBarLabelStyle: CustomTabBarLabelStyle;
if (Platform.OS === 'ios') {
    customTabBarLabelStyle = {
        fontSize: 8,
        fontWeight: "500"
    };
} else if (Platform.OS === 'android') {
    customTabBarLabelStyle = {
        fontSize: 14,
        fontWeight: "500"
    };
}

const customTabBarIcon: CustomTabBarIcon = {
    size: 30
};

export default function UserHomeTab() {
    return (
        <Tab.Navigator
            initialRouteName={"Call的"}
        // backBehavior={"initialRoute"}
        >
            <Tab.Screen
                name="Call的"
                component={BookingStackScreen}
                options={{
                    headerShown: false,
                    tabBarLabelStyle: {
                        fontSize: customTabBarLabelStyle.fontSize,
                        fontWeight: customTabBarLabelStyle.fontWeight
                    },
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="taxi" color={color} size={customTabBarIcon.size} />
                    ),
                    unmountOnBlur: true,
                }}
            />
            <Tab.Screen
                name="進行中"
                component={ActiveBooking}
                options={{
                    headerShown: false,
                    tabBarLabelStyle: {
                        fontSize: customTabBarLabelStyle.fontSize,
                        fontWeight: customTabBarLabelStyle.fontWeight
                    },
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="forward" color={color} size={customTabBarIcon.size} />
                    ),
                    unmountOnBlur: true
                }}
            />
            <Tab.Screen
                name="記錄"
                component={UserHistoryStackScreen}
                options={{
                    headerShown: false,
                    tabBarLabelStyle: {
                        fontSize: customTabBarLabelStyle.fontSize,
                        fontWeight: customTabBarLabelStyle.fontWeight
                    },
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="history" color={color} size={customTabBarIcon.size} />
                    ),
                    unmountOnBlur: true
                }}
            />
            <Tab.Screen
                name="共乘"
                component={PoolStackScreen}
                options={{
                    headerShown: false,
                    tabBarLabelStyle: {
                        fontSize: customTabBarLabelStyle.fontSize,
                        fontWeight: customTabBarLabelStyle.fontWeight
                    },
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="account-group" color={color} size={customTabBarIcon.size} />
                    ),
                    unmountOnBlur: true
                }}
            />
            <Tab.Screen
                name="設定"
                component={UserSettingStackScreen}
                options={{
                    headerShown: false,
                    tabBarLabelStyle: {
                        fontSize: customTabBarLabelStyle.fontSize,
                        fontWeight: customTabBarLabelStyle.fontWeight
                    },
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name="account-settings" color={color} size={customTabBarIcon.size} />
                    ),
                    unmountOnBlur: true
                }}
            />
        </Tab.Navigator>
    );
};