import * as React from 'react';
import { createNativeStackNavigator, NativeStackScreenProps } from "@react-navigation/native-stack";
import { Button, Text, Pressable } from 'react-native';
import { StyleSheet } from 'react-native';
import UserHomeTab from '../../users/components/UserHomeTab';
// import PoolList from './PoolList';
// import PoolDetail from './PoolDetail';
import UserHistory from './UserHistory';
import UserHistoryDetails from './UserHistoryDetails';
import { Order } from '../../../redux/driverOrder/reducer';


export type UserHistoryStackScreenParamList = {
    UserHistory: undefined;
    UserHistoryDetails: undefined | { order: Order };
};

// export type poolListDetailProp = NativeStackScreenProps<PoolStackScreenParamList, 'Detail'>;


interface CustomTabBarLabelStyle {
    fontSize: number;
    fontWeight: "bold" | "normal" | "100" | "200" | "300" | "400" | "500" | "600" | "700" | "800" | "900" | undefined;
};


const customTabBarLabelStyle: CustomTabBarLabelStyle = {
    fontSize: 7.5,
    fontWeight: "500"
};

const Stack = createNativeStackNavigator<UserHistoryStackScreenParamList>();


export function UserHistoryStackScreen() {
    return (
        <Stack.Navigator
            initialRouteName="UserHistory"

        >
            <Stack.Screen
                name="UserHistory"
                component={UserHistory}
                options={{
                    headerShown: false,
                }}
            // options={{ animation: "fade" }}
            />
            <Stack.Screen
                name="UserHistoryDetails"
                component={UserHistoryDetails}
                options={{ title: "記錄",
                            headerShown: false,}}

            />

        </Stack.Navigator>
    );
};

