//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import React, { useState, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { DataTable } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { NavigationProp, useIsFocused, useNavigation } from '@react-navigation/native';
import { UserHistoryStackScreenParamList } from './UserHistoryStackScreen';
import { RootState } from '../../../store';
import { Order } from '../../../redux/driverOrder/reducer';
import NoResult from '../../noResult/noResult';
import Review from '../review/Review';
import { SafeAreaView } from 'react-native-safe-area-context';
import SplashScreen from '../../../components/SplashScreen';



export default function UserHistory() {
    const navigation: NavigationProp<UserHistoryStackScreenParamList> = useNavigation();
    const dispatch = useDispatch();
    const isFocused = useIsFocused()
    const userId = useSelector((state: RootState) => state.auth.user?.id);
    const [modalVisible, setModalVisible] = useState(false);
    const [orders, setOrders] = useState<Order[]>([])
    const [orderView, setorderView] = useState([true, false, false]);
    const [isLoading, setIsLoading] = useState(true);
    const [isRendered, setIsRendered] = useState(0);
    // const orders = useSelector((state: RootState) => state.viewOrder.history);

    // useEffect(() => {
    //     if (userId != undefined) {
    //         dispatch(fetchUserHistoryOrder(userId));

    //         return
    //     }
    // }, [isFocused])
    useEffect(() => {
        if (userId) {
            loadData(userId)
        }
    }, [isFocused])

    useEffect(() => {
        if (isRendered === 1) {
            setIsLoading(false);
        }
    }, [isRendered])

    async function loadData(userId: number) {
        try {
            let res = await fetch(`${REACT_APP_BACKEND_URL}/order/completed/user/${userId}`)

            const result = await res.json()
            // console.log(result);

            await setOrders(result)
            setIsRendered(state => state + 1);
        } catch (error) {
            console.log(error);
        }
    }

    const EventRow = (order: Order) => {
        return (
            <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => {
                    navigation.navigate('UserHistoryDetails', { order: order })
                }}>

                <DataTable.Row style={styles.eventRow}>
                    <DataTable.Cell style={styles.place}>
                        <Text style={styles.eventText}>{order.origin_district}</Text>
                    </DataTable.Cell>
                    <DataTable.Cell style={styles.toEventRow}>
                        <Text style={styles.toEventText}>到</Text>
                    </DataTable.Cell>
                    <DataTable.Cell style={styles.place}>
                        <Text style={styles.eventText}>{order.destination_district}</Text>
                    </DataTable.Cell>
                    {/* <DataTable.Cell children={Element} style ={justifyContent: "center" }> */}
                    <DataTable.Cell style={styles.buttonCell} >
                        {/* <Text>{'>'}</Text> */}
                        {/* <MaterialCommunityIcons name="star" style={styles.ratingStar} /> */}
                        <View style={styles.btnContainer}>
                            <TouchableOpacity
                                onPress={() => setModalVisible(true)}
                            >
                                <Review />
                            </TouchableOpacity>
                        </View>
                    </DataTable.Cell>
                </DataTable.Row>

            </TouchableOpacity>
        )
    }
    return (
        <SafeAreaView edges={['top', 'left', 'right']} style={styles.container}>
            {/* <ScrollView style={styles.container}> */}
            {isLoading && <SplashScreen />}
            {!(orders.length >= 1) && !isLoading && <NoResult />}

            {orders.length >= 1 && !isLoading &&
                <View>
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity
                            // underlayColor={''}
                            onPress={() => setorderView([true, false, false])}
                        >
                            <View>
                                <Text style={orderView[0] ? styles.iconSelected : styles.iconDeselected}>全部</Text>

                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            // underlayColor={''}
                            onPress={() => setorderView([false, true, false])}
                        >
                            <Text style={orderView[1] ? styles.iconSelected : styles.iconDeselected}>完成</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            // underlayColor={''}
                            onPress={() => setorderView([false, false, true])}
                        >
                            <Text style={orderView[2] ? styles.iconSelected : styles.iconDeselected}>過時</Text>
                        </TouchableOpacity>

                    </View>

                    <DataTable style={styles.DataTableContainer}>
                        <DataTable.Header style={styles.eventHeader}>
                            <DataTable.Title style={styles.place}>起點</DataTable.Title>
                            <DataTable.Title > </DataTable.Title>
                            <DataTable.Title style={styles.place}>目的地</DataTable.Title>
                            <DataTable.Title > </DataTable.Title>
                        </DataTable.Header>

                        {orderView[0] && orders.map((order: any) => (
                            <EventRow  {...order}
                                key={order.id} />))}

                        {/* {orderView[0] && (orders.length ? orders.map((order: any) => (
                            <EventRow  {...order}
                                key={order.id} />)) : <NoResult />)} */}

                        {orderView[1] && ((orders.filter(order => order.status == 'complete')).length ?
                            orders.map((order: any) => (
                                order.status == 'complete' && <EventRow  {...order}
                                    key={order.id} />)) : <NoResult />)}


                        {orderView[2] && ((orders.filter(order => order.status == 'expired')).length ?
                            orders.map((order: any) => (
                                order.status == 'expired' && <EventRow  {...order}
                                    key={order.id} />)) : <NoResult />)}


                    </DataTable>
                </View>
            }
            {/* </ScrollView > */}
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        flex: 1,
        // backgroundColor: '#a4c2a5',
    },

    eventHeader: {
        borderColor: '#f1f2eb',
    },
    eventRow: {
        backgroundColor: '#d8dad3',
        borderRadius: 15,
        marginVertical: 5,
        padding: 10,
        flexDirection: 'row',

    },
    direction: {
        fontSize: 20,
        padding: 6
    },
    locationCol: {
        flex: 10
    },
    locationRow: {
        flexDirection: 'row',
    },
    locationName: {
        fontSize: 30,
        color: 'black',
        flexWrap: 'wrap',

        paddingRight: 15,
    },
    divider: {
        borderBottomColor: '#566246',
        borderBottomWidth: 1,
        opacity: 0.3,
        margin: 5
    },
    arrowBox: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'

    },
    arrow: {
        fontSize: 30
    },
    eventText: {
        color: '#4a4a48',
        flexWrap: 'wrap',
        fontSize: 20,
    },
    buttonCell: {
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    buttonText: {
        color: 'white',
    },
    button: {
        backgroundColor: '#566246',
        borderRadius: 13,
        justifyContent: 'center',
        paddingHorizontal: 15,
        paddingVertical: 7,
    },
    buttonContainer: {
        // backgroundColor: 'blue',
        paddingTop: 100,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    selectbutton: {
        borderRadius: 13,
        justifyContent: 'center',
        paddingHorizontal: 35,
        paddingVertical: 15,
        borderWidth: 2,
    },
    selectButtonText: {
        color: 'black',
        fontSize: 18,
    },
    place: {
        // backgroundColor: 'red',
        justifyContent: 'center',
        fontSize: 18,
    },
    toEventText: {
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
    },
    toEventRow: {
        justifyContent: 'center',
    },
    DataTableContainer: {
        // backgroundColor: 'orange',
        height: '100%',
        paddingHorizontal: 20,
    },
    iconSelected: {
        backgroundColor: '#566246',
        color: '#f1f2eb',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#a4c2a5'
    },
    iconDeselected: {
        backgroundColor: '#d8dad3',
        color: '#4a4a48',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#f1f2eb'
    },
    btnContainer: {
        // backgroundColor: 'orange',
        // justifyContent: 'center',
        // alignItems: "center",
        // borderRadius: 100,
    },

});
