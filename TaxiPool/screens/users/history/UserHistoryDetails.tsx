import React, { useEffect, useMemo, useState } from "react";
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { SafeAreaView } from "react-native-safe-area-context";
import { useDispatch, useSelector } from "react-redux";
import SplashScreen from "../../../components/SplashScreen";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { DriverHistoryListDetailProp } from "../../drivers/orders/DriverHistoryStackScreen";

export default function UserHistoryDetails({ route }: DriverHistoryListDetailProp) {
    const order = route.params?.order;

    const dispatch = useDispatch();
    // const navigation: NavigationProp<DriverEventStackScreenParamList> = useNavigation();
    // const userId = useSelector((state: RootState) => state.auth.user?.id);
    // const order = useSelector((state: RootState) => state.order.order);
    // const isLoading = useSelector((state: RootState) => state.loading.isLoading);
    const [isLoading, setIsLoading] = useState(-1);


    return (
        <SafeAreaView edges={['top', 'left', 'right']} style={styles.container}>
            {isLoading >= 0 && <SplashScreen />}
            {/* {isLoading === 1 && <Text>Loading...</Text>} */}

            <ScrollView style={styles.container}>
                {isLoading <= -1 && order.id != undefined && <View style={styles.innerContainer}>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => { navigation.navigate("設定主頁") }}>
                        <Text style={styles.buttonText}>返回</Text>
                    </TouchableOpacity>
                    
                    <View style={styles.locationContainer}>
                        <View style={styles.locationLabelContainer}>
                            <Text style={styles.locationLabel}>起點</Text>
                        </View>
                        <Text style={styles.location}>{order.origin_description}</Text>
                    </View>
                    <View style={styles.locationContainer}>
                        <View style={styles.locationLabelContainer}>
                            <Text style={styles.locationLabel}>目的地</Text>
                        </View>
                        <Text style={styles.location}>{order.destination_description}</Text>
                    </View>

                    {/* <View style={styles.divider} /> */}

                    <View style={styles.section}>
                        <View><Text style={styles.sectionTitle}>訂單詳細</Text></View>
                        <View style={styles.categoryContainer}>
                            <Text style={styles.categoryLabel}>狀態</Text>
                            <View style={styles.categoryElem}>
                                <Text style={styles.text}>
                                    {order.status == 'searching' && <Text>待接單</Text>}
                                    {order.status == 'pending' && <Text>等待司機接客</Text>}
                                    {order.status == 'travelling' && <Text>駕駛中</Text>}
                                    {order.status == 'complete' && <Text>已完成</Text>}
                                    {order.status == 'expired' && <Text>過時</Text>}
                                </Text>
                            </View>
                        </View>

                        {order.name !== '' &&
                            <View style={styles.categoryContainer}>
                                <Text style={styles.categoryLabel}>司機資料</Text>
                                <View style={styles.categoryElem}>
                                    <View style={styles.categoryContainer}>
                                        <View style={styles.categoryElem}>
                                            <Text style={styles.text}>
                                                姓名: {order.name}
                                            </Text>
                                        </View>
                                        <View style={styles.categoryElem}>
                                            <Text style={styles.text}>
                                                車牌: {order.rego}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>}
                    </View>

                    <View style={styles.section}>
                        <View><Text style={styles.sectionTitle}>乘客要求</Text></View>
                        <View style={styles.categoryContainer}>
                            <Text style={styles.categoryLabel}>海底隧道</Text>
                            <View style={styles.categoryElem}>
                                {order.cross_harbour_tunnel || order.eastern_harbour_crossing || order.western_harbour_crossing ?
                                    <View></View> : <Text style={styles.text}>不適用</Text>}

                                {order.cross_harbour_tunnel && <View style={styles.iconSelected}>
                                    <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>紅隧</Text>
                                </View>}
                                {order.eastern_harbour_crossing && <View style={styles.iconSelected}>
                                    <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>東隧</Text>
                                </View>}
                                {order.western_harbour_crossing && <View style={styles.iconSelected}>
                                    <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>西隧</Text>
                                </View>}
                            </View>
                        </View>

                        <View style={styles.categoryContainer}>
                            <Text style={styles.categoryLabel}>其他要求</Text>
                            <View style={styles.categoryElem}>
                                {order.has_pet || order.smoke_free || order.has_luggage || order.is_disabled || order.urgent ?
                                    <View></View> : <Text style={styles.text}> 無特別要求</Text>}


                                {order.has_pet && <View style={styles.iconSelected}>
                                    <MaterialIcons name="pets" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>寵物</Text>
                                </View>}

                                {order.smoke_free && <View style={styles.iconSelected}>
                                    <MaterialIcons name="smoke-free" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>不吸煙</Text>
                                </View>}

                                {order.has_luggage && <View style={styles.iconSelected}>
                                    <MaterialIcons name="luggage" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>行李</Text>
                                </View>}

                                {order.is_disabled && <View style={styles.iconSelected}>
                                    <MaterialIcons name="wheelchair-pickup" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>傷殘人仕</Text>
                                </View>}

                                {order.urgent && <View style={styles.iconSelected}>
                                    <MaterialCommunityIcons name="run-fast" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>緊急</Text>
                                </View>}


                            </View>
                        </View>
                    </View>

                </View>}
            </ScrollView >
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    innerContainer: {
        margin: 15
    },
    locationLabelContainer: {
        backgroundColor: '#a4c2a5',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#566246',
        color: '#4a4a48',
        paddingVertical: 10,
        width: 80,
    },
    locationLabel: {
        fontWeight: 'bold',
        textAlign: 'center'
    },
    location: {
        paddingVertical: 15,
        color: '#4a4a48',
        fontSize: 28,
        flexWrap: 'wrap',
        fontWeight: 'bold',
    },
    locationContainer: {
        marginHorizontal: 10,
        // backgroundColor: '#a4c2a5',
        // flexDirection: 'column'
    },
    categoryContainer: {
        marginBottom: 10
    },
    categoryLabel: {
        fontWeight: "bold",
        marginHorizontal: 10,
        paddingVertical: 10,
        color: '#4a4a48',
        fontSize: 20
    },
    categoryElem: {
        flexDirection: "row",
        // marginVertical: 5,
        flexWrap: "wrap"
    },
    divider: {
        borderBottomColor: '#566246',
        borderBottomWidth: 1,
        opacity: 0.7,
        margin: 10
    },
    section: {
        backgroundColor: '#d8dad3',
        marginVertical: 10,
        borderRadius: 10,
        padding: 10,
    },
    sectionTitle: {
        backgroundColor: '#f1f2eb',
        borderRadius: 10,
        color: '#4a4a48',
        paddingVertical: 5,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 25,
    },
    iconSelected: {
        opacity: 1,
        marginBottom: 10,
        marginHorizontal: 20
    },
    iconDeselected: {
        opacity: 0.2,
        marginBottom: 15,
    },
    icon: {
        marginHorizontal: 10,
        fontSize: 50,
        color: '#4a4a48'
    },
    iconLabel: {
        textAlign: "center",
        fontWeight: "600",
        color: "black",
        fontSize: 17,
    },
    button: {
        alignItems: "center",
        backgroundColor: "#566246",
        padding: 10,
        marginVertical: 5,
        borderRadius: 10,
    },
    buttonText: {
        textAlign: 'center',
        color: '#f1f2eb',
        fontWeight: 'bold',
    },
    inputContainer: {
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        flexDirection: 'row',
        flexWrap: "wrap",
    },
    input: {
        backgroundColor: 'white',
        marginVertical: 5,
        height: 40,
        width: '75%',
        borderRadius: 5,
    },
    buttons: {
        backgroundColor: '#566246',
        margin: 5,
        padding: '3%',
        borderRadius: 10,
        width: 250,
    },
    text: {
        marginHorizontal: 20
    },
});