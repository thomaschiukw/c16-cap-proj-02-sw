import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { SafeAreaView } from "react-native-safe-area-context";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from '../../../store';
import { fetchActiveOrder } from "../../../redux/order/thunk";
import { socket } from "../../../App";
import SplashScreen from "../../../components/SplashScreen";
import NoResult from "../../noResult/noResult";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { useIsFocused } from "@react-navigation/native";

export default function ActiveBooking() {
    const dispatch = useDispatch();
    const userId = useSelector((state: RootState) => state.auth.user?.id);
    const activeOrder = useSelector((state: RootState) => state.order.activeOrder);
    const activePoolOrder = useSelector((state: RootState) => state.order.activePoolOrder);
    const firstActiveOrderPoolDeadline = useSelector((state: RootState) => state.order.activePoolOrder[0]?.pool_deadline);
    const [isLoading, setIsLoading] = useState(1);
    const [rendered, setRendered] = useState(1);
    const isFocused = useIsFocused();


    useEffect(() => {
        // dispatch(fetchDriverSession(driverId))
        if (userId) {
            dispatch(fetchActiveOrder(userId));
        }
    }, [isFocused])

    useEffect(() => {
        if (rendered < 2) {
            setRendered(state => state + 1);
        }
        if (rendered >= 2) {
            setIsLoading(-1);
        }
    }, [activeOrder, activePoolOrder])

    useEffect(() => {
        socket.on("order-accepted", () => {
            console.log("accepted");

            if (userId !== undefined) {
                dispatch(fetchActiveOrder(userId));
            }
        })

        socket.on("order-started", () => {
            if (userId !== undefined) {
                dispatch(fetchActiveOrder(userId));
            }
        })

        socket.on("order-completed", () => {
            if (userId !== undefined) {
                dispatch(fetchActiveOrder(userId));
            }
        })
        return () => { socket.removeAllListeners() };
    }, [])

    // console.log('rendered times =', rendered);
    const currentTime = new Date();
    const [seconds, setSeconds] = useState(0);
    const [timerMin, setTimerMin] = useState(0)
    const [timerSec, setTimerSec] = useState(0)

    useEffect(() => {
        if (firstActiveOrderPoolDeadline === undefined) {
            console.log("No pool order")
        } else {
            const poolDeadline = new Date(activePoolOrder[0].pool_deadline);

            let secondsDiff = ((poolDeadline.getTime() - currentTime.getTime()) / 1000);

            // console.log("currentTime", currentTime.getTime())
            // console.log("poolDeadline", poolDeadline.getTime())
            // console.log("secondsDiff", secondsDiff)

            setSeconds(Math.round(secondsDiff))
        }
    }, [firstActiveOrderPoolDeadline]);

    useEffect(() => {
        setTimerSec(seconds % 60)
        setTimerMin(Math.floor(seconds / 60))
    }, [seconds]);

    useEffect(() => {
        const interval = setInterval(() => {
            setSeconds(seconds => seconds - 1);
        }, 1000);

        // console.log("no dependance");
        return () => { clearInterval(interval) };
    }, [])

    return (
        <SafeAreaView edges={['top', 'left', 'right']} style={styles.pageContainer}>
            {/* Loading Page */}
            {isLoading == 1 && <SplashScreen />}

            {/* If there is no order (whether it is pool), show no Result page */}
            {isLoading === -1 && (!(activeOrder.length >= 1) && !(activePoolOrder.length >= 1)) && <NoResult />}

            {/* Show order of pool order*/}
            {isLoading === -1 && activePoolOrder.length >= 1 &&
                <ScrollView style={styles.container}>

                    <View style={styles.innerContainer}>
                        {firstActiveOrderPoolDeadline &&
                            <View style={styles.categoryContainer}>
                                <View style={styles.timer}>
                                    {seconds >= 0 ? <View><Text style={styles.sectionTitle}>剩餘時間 {timerMin < 10 && 0}{timerMin}:{timerSec < 10 && 0}{timerSec}</Text></View> :

                                        <View>
                                            <Text style={styles.sectionTitle}>已過共乘時限</Text>
                                        </View>

                                    }
                                </View>
                            </View>
                        }

                        <View style={styles.locationContainer}>
                            <View style={styles.locationLabelContainer}>
                                <Text style={styles.locationLabel}>起點</Text>
                            </View>
                            <Text style={styles.location}>{activePoolOrder[0].origin_description}</Text>
                        </View>
                        <View style={styles.locationContainer}>
                            <View style={styles.locationLabelContainer}>
                                <Text style={styles.locationLabel}>目的地</Text>
                            </View>
                            <Text style={styles.location}>{activePoolOrder[0].destination_description}</Text>
                        </View>

                        <View style={styles.section}>
                            <View><Text style={styles.sectionTitle}>訂單詳細</Text></View>
                            <View style={styles.categoryContainer}>
                                <Text style={styles.categoryLabel}>狀態</Text>
                                <View style={styles.categoryElem}>
                                    <Text style={styles.text}>
                                        {activePoolOrder[0].status == 'searching' && <Text>待接單</Text>}
                                        {activePoolOrder[0].status == 'pending' && <Text>等待司機接客</Text>}
                                        {activePoolOrder[0].status == 'travelling' && <Text>駕駛中</Text>}
                                        {activePoolOrder[0].status == 'complete' && <Text>已完成</Text>}
                                    </Text>
                                </View>
                            </View>

                            {activePoolOrder[0].status !== 'searching' &&
                                <View style={styles.categoryContainer}>
                                    <Text style={styles.categoryLabel}>司機資料</Text>
                                    <View style={styles.categoryElem}>
                                        <View style={styles.categoryContainer}>
                                            <View style={styles.categoryElem}>
                                                <Text style={styles.text}>
                                                    姓名: {activePoolOrder[0].name}
                                                </Text>
                                            </View>
                                            <View style={styles.categoryElem}>
                                                <Text style={styles.text}>
                                                    車牌: {activePoolOrder[0].rego}
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>}
                        </View>

                        <View style={styles.section}>
                            <View><Text style={styles.sectionTitle}>乘客要求</Text></View>
                            <View style={styles.categoryContainer}>
                                <Text style={styles.categoryLabel}>海底隧道</Text>
                                <View style={styles.categoryElem}>
                                    {activePoolOrder[0].cross_harbour_tunnel || activePoolOrder[0].eastern_harbour_crossing || activePoolOrder[0].western_harbour_crossing ?
                                        <View></View> : <Text style={styles.text}>不適用</Text>}

                                    {activePoolOrder[0].cross_harbour_tunnel && <View style={styles.iconSelected}>
                                        <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>紅隧</Text>
                                    </View>}
                                    {activePoolOrder[0].eastern_harbour_crossing && <View style={styles.iconSelected}>
                                        <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>東隧</Text>
                                    </View>}
                                    {activePoolOrder[0].western_harbour_crossing && <View style={styles.iconSelected}>
                                        <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>西隧</Text>
                                    </View>}
                                </View>
                            </View>

                            <View style={styles.categoryContainer}>
                                <Text style={styles.categoryLabel}>其他要求</Text>
                                <View style={styles.categoryElem}>
                                    {activePoolOrder[0].has_pet || activePoolOrder[0].smoke_free || activePoolOrder[0].has_luggage || activePoolOrder[0].is_disabled || activePoolOrder[0].urgent ?
                                        <View></View> : <Text style={styles.text}> 無特別要求</Text>}


                                    {activePoolOrder[0].has_pet && <View style={styles.iconSelected}>
                                        <MaterialIcons name="pets" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>寵物</Text>
                                    </View>}

                                    {activePoolOrder[0].smoke_free && <View style={styles.iconSelected}>
                                        <MaterialIcons name="smoke-free" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>不吸煙</Text>
                                    </View>}

                                    {activePoolOrder[0].has_luggage && <View style={styles.iconSelected}>
                                        <MaterialIcons name="luggage" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>行李</Text>
                                    </View>}

                                    {activePoolOrder[0].is_disabled && <View style={styles.iconSelected}>
                                        <MaterialIcons name="wheelchair-pickup" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>傷殘人仕</Text>
                                    </View>}

                                    {activePoolOrder[0].urgent && <View style={styles.iconSelected}>
                                        <MaterialCommunityIcons name="run-fast" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>緊急</Text>
                                    </View>}


                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView >
            }

            {/* Show order of not pool order*/}
            {isLoading === -1 && activeOrder.length >= 1 &&
                <ScrollView style={styles.container}>

                    <View style={styles.innerContainer}>
                        <View style={styles.locationContainer}>
                            <View style={styles.locationLabelContainer}>
                                <Text style={styles.locationLabel}>起點</Text>
                            </View>
                            <Text style={styles.location}>{activeOrder[0].origin_description}</Text>
                        </View>
                        <View style={styles.locationContainer}>
                            <View style={styles.locationLabelContainer}>
                                <Text style={styles.locationLabel}>目的地</Text>
                            </View>
                            <Text style={styles.location}>{activeOrder[0].destination_description}</Text>
                        </View>

                        <View style={styles.section}>
                            <View><Text style={styles.sectionTitle}>訂單詳細</Text></View>
                            <View style={styles.categoryContainer}>
                                <Text style={styles.categoryLabel}>狀態</Text>
                                <View style={styles.categoryElem}>
                                    <Text style={styles.text}>
                                        {activeOrder[0].status == 'searching' && <Text>待司機接單</Text>}
                                        {activeOrder[0].status == 'pending' && <Text>等待司機接客</Text>}
                                        {activeOrder[0].status == 'travelling' && <Text>駕駛中</Text>}
                                        {activeOrder[0].status == 'complete' && <Text>已完成</Text>}
                                    </Text>
                                </View>
                            </View>

                            {activeOrder[0].status !== 'searching' &&
                                <View style={styles.categoryContainer}>
                                    <Text style={styles.categoryLabel}>司機資料</Text>
                                    <View style={styles.categoryElem}>
                                        <View style={styles.categoryContainer}>
                                            <View style={styles.categoryElem}>
                                                <Text style={styles.text}>
                                                    姓名: {activeOrder[0].name}
                                                </Text>
                                            </View>
                                            <View style={styles.categoryElem}>
                                                <Text style={styles.text}>
                                                    車牌: {activeOrder[0].rego}
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>}
                        </View>

                        <View style={styles.section}>
                            <View><Text style={styles.sectionTitle}>乘客要求</Text></View>
                            <View style={styles.categoryContainer}>
                                <Text style={styles.categoryLabel}>海底隧道</Text>
                                <View style={styles.categoryElem}>
                                    {activeOrder[0].cross_harbour_tunnel || activeOrder[0].eastern_harbour_crossing || activeOrder[0].western_harbour_crossing ?
                                        <View></View> : <Text style={styles.text}>不適用</Text>}

                                    {activeOrder[0].cross_harbour_tunnel && <View style={styles.iconSelected}>
                                        <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>紅隧</Text>
                                    </View>}
                                    {activeOrder[0].eastern_harbour_crossing && <View style={styles.iconSelected}>
                                        <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>東隧</Text>
                                    </View>}
                                    {activeOrder[0].western_harbour_crossing && <View style={styles.iconSelected}>
                                        <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>西隧</Text>
                                    </View>}
                                </View>
                            </View>

                            <View style={styles.categoryContainer}>
                                <Text style={styles.categoryLabel}>其他要求</Text>
                                <View style={styles.categoryElem}>
                                    {activeOrder[0].has_pet || activeOrder[0].smoke_free || activeOrder[0].has_luggage || activeOrder[0].is_disabled || activeOrder[0].urgent ?
                                        <View></View> : <Text style={styles.text}> 無特別要求</Text>}


                                    {activeOrder[0].has_pet && <View style={styles.iconSelected}>
                                        <MaterialIcons name="pets" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>寵物</Text>
                                    </View>}

                                    {activeOrder[0].smoke_free && <View style={styles.iconSelected}>
                                        <MaterialIcons name="smoke-free" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>不吸煙</Text>
                                    </View>}

                                    {activeOrder[0].has_luggage && <View style={styles.iconSelected}>
                                        <MaterialIcons name="luggage" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>行李</Text>
                                    </View>}

                                    {activeOrder[0].is_disabled && <View style={styles.iconSelected}>
                                        <MaterialIcons name="wheelchair-pickup" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>傷殘人仕</Text>
                                    </View>}

                                    {activeOrder[0].urgent && <View style={styles.iconSelected}>
                                        <MaterialCommunityIcons name="run-fast" color="black" style={styles.icon} />
                                        <Text style={styles.iconLabel}>緊急</Text>
                                    </View>}


                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView >
            }
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    pageContainer: {
        justifyContent: 'center',
        flex: 1,

    },
    innerContainer: {
        margin: 15
    },
    locationLabelContainer: {
        backgroundColor: '#a4c2a5',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#566246',
        color: '#4a4a48',
        paddingVertical: 10,
        width: 80,
    },
    locationLabel: {
        fontWeight: 'bold',
        textAlign: 'center'
    },
    location: {
        paddingVertical: 15,
        color: '#4a4a48',
        fontSize: 28,
        flexWrap: 'wrap',
        fontWeight: 'bold',
    },
    locationContainer: {
        marginHorizontal: 10,
        // backgroundColor: '#a4c2a5',
        // flexDirection: 'column'
    },
    categoryContainer: {
        marginBottom: 10
    },
    categoryLabel: {
        fontWeight: "bold",
        marginHorizontal: 10,
        paddingVertical: 10,
        color: '#4a4a48',
        fontSize: 20
    },
    categoryElem: {
        flexDirection: "row",
        // marginVertical: 5,
        flexWrap: "wrap"
    },
    divider: {
        borderBottomColor: '#566246',
        borderBottomWidth: 1,
        opacity: 0.7,
        margin: 10
    },
    timer: {
        backgroundColor: '#d8dad3',
        marginVertical: 10,
        borderRadius: 10,
        padding: 10,
    },
    timerText: {
        backgroundColor: '#f1f2eb',
        color: '#4a4a48',
        paddingVertical: 5,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 25,
    },
    section: {
        backgroundColor: '#d8dad3',
        marginVertical: 10,
        borderRadius: 10,
        padding: 10,
    },
    sectionTitle: {
        backgroundColor: '#f1f2eb',
        borderRadius: 10,
        color: '#4a4a48',
        paddingVertical: 5,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 25,
    },
    iconSelected: {
        opacity: 1,
        marginBottom: 10,
        marginHorizontal: 20
    },
    iconDeselected: {
        opacity: 0.2,
        marginBottom: 15,
    },
    icon: {
        marginHorizontal: 10,
        fontSize: 50,
        color: '#4a4a48'
    },
    iconLabel: {
        textAlign: "center",
        fontWeight: "600",
        color: "black",
        fontSize: 17,
    },
    button: {
        alignItems: "center",
        backgroundColor: "#566246",
        padding: 10,
        marginVertical: 5,
        borderRadius: 10,
    },
    buttonText: {
        textAlign: 'center',
        color: '#f1f2eb',
        fontWeight: 'bold',
    },
    inputContainer: {
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        flexDirection: 'row',
        flexWrap: "wrap",
    },
    input: {
        backgroundColor: 'white',
        marginVertical: 5,
        height: 40,
        width: '75%',
        borderRadius: 5,
    },
    buttons: {
        backgroundColor: '#566246',
        margin: 5,
        padding: '3%',
        borderRadius: 10,
        width: 250,
    },
    text: {
        marginHorizontal: 20
    },
});