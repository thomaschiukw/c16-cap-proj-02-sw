//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import React, { useEffect, useState } from "react";
import { NavigationProp, useNavigation, useIsFocused } from '@react-navigation/native';
import { useSelector, useDispatch } from "react-redux";
import { RootState } from '../../../store';
import { PoolOrder, PoolStackScreenParamList } from './PoolStackScreen';
import { StackActions } from "@react-navigation/routers";
import SplashScreen from '../../../components/SplashScreen';
import { SafeAreaView } from 'react-native-safe-area-context';
import { fetchCurrentPool } from '../../../redux/order/thunk';

export default function PoolFetching() {
    const dispatch = useDispatch();
    const navigation: NavigationProp<PoolStackScreenParamList> = useNavigation();
    const userId = useSelector((state: RootState) => state.auth.user?.id);
    const orders = useSelector((state: RootState) => state.order.activePoolOrder);
    const [isDataLoaded, setIsDataLoaded] = useState(0);

    useEffect(() => {
        if (userId != undefined) {
            dispatch(fetchCurrentPool(userId));
            setIsDataLoaded(state => state + 1);
            // console.log('fetch: ', orders);
        }
    }, [])

    useEffect(() => {
        if (isDataLoaded === 1) {
            if (orders.length == 0) {
                // console.log('fetch:1');
                navigation.dispatch(StackActions.replace('Event'));
            } else {
                // console.log('fetch:2');
                // navigation.navigate('Joined', { orders: orders })
                navigation.dispatch(StackActions.replace('Joined', { orders: orders }));
            }
        }
    }, [orders])

    return (
        <SafeAreaView edges={['top', 'left', 'right']} >
            <SplashScreen />
        </SafeAreaView >
    )
}