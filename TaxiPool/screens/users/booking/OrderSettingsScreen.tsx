import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View, Alert, Modal, Pressable, Switch } from "react-native";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useForm, SubmitHandler } from 'react-hook-form';
import { useDispatch, useSelector } from "react-redux";
import { fetchCreateOrder } from "../../../redux/order/thunk";
import { NavigationProp, TabActions, useNavigation } from '@react-navigation/native'
import { UserHomeTabParamList } from "../components/UserHomeTab";
import { RootState } from "../../../store";
import SplashScreen from "../../../components/SplashScreen";
import { clearSearchInput } from "../../../redux/map/actions";

export interface OrderFormInput {
    minFare: number,
    headcount: number,
    isPool: boolean,
    isHybrid: boolean,
    isRedTaxi: boolean,
    isGreenTaxi: boolean,
    isBlueTaxi: boolean,
    isCentralTunnel: boolean,
    isEastTunnel: boolean,
    isWestTunnel: boolean,
    hasPet: boolean,
    isSmokeFree: boolean,
    hasLuggage: boolean,
    isDisabled: boolean,
    isUrgent: boolean,
}

export default function OrderSettingsScreen() {
    const originDescription = useSelector((state: RootState) => state.map.selectedOrigin?.details?.description);
    const originArea = useSelector((state: RootState) => state.map.selectedOrigin.administrative_area);
    // const orderOriginPlaceId = useSelector((state: RootState) => state.order.originDetails?.place_id);
    const destinationDescription = useSelector((state: RootState) => state.map.selectedDestination?.details?.description);
    const destArea = useSelector((state: RootState) => state.map.selectedDestination.administrative_area);
    // const orderDestinationPlaceId = useSelector((state: RootState) => state.order.destinationDetails?.place_id);
    const orderDistance = useSelector((state: RootState) => state.map.route.distance);
    const isOrderActive = useSelector((state: RootState) => state.order.isOrderActive);
    const [isCalculatingFare, setIsCalculatingFare] = useState(true);
    const isLoading = useSelector((state: RootState) => state.loading.isLoading);
    const [isOrderLoading, setIsOrderLoading] = useState(0);
    const [minFare, setMinFare] = useState<number>(0);
    const [maxFare, setMaxFare] = useState<number>(0);
    const [isPool, setIsPool] = useState<boolean>(false);
    const [isHybrid, setIsHybrid] = useState<boolean>(true);
    const [headcount, setHeadcount] = useState<number>(1);
    const [isRedTaxi, setIsRedTaxi] = useState<boolean>(true);
    const [isGreenTaxi, setIsGreenTaxi] = useState<boolean>(false);
    const [isBlueTaxi, setIsBlueTaxi] = useState<boolean>(false);
    const [isAllTunnel, setIsAllTunnel] = useState<boolean>(false);
    const [isCentralTunnel, setIsCentralTunnel] = useState<boolean>(true);
    const [isEastTunnel, setIsEastTunnel] = useState<boolean>(true);
    const [isWestTunnel, setIsWestTunnel] = useState<boolean>(false);
    const [hasPet, setHasPet] = useState<boolean>(false);
    const [isSmokeFree, setIsSmokeFree] = useState<boolean>(true);
    const [hasLuggage, setHasLuggage] = useState<boolean>(false);
    const [isDisabled, setIsDisabled] = useState<boolean>(false);
    const [isUrgent, setIsUrgent] = useState<boolean>(false);

    const [showTunnel, setShowTunnel] = useState(true);
    const [confirmModalVisible, setConfirmModalVisible] = useState(false);
    const [successModalVisible, setSuccessModalVisible] = useState(false);
    const [failModalVisible, setFailModalVisible] = useState(false);

    const [hasError, setHasError] = useState(false);

    const dispatch = useDispatch();
    const navigation: NavigationProp<UserHomeTabParamList> = useNavigation();

    useEffect(() => {
        if (orderDistance && orderDistance != NaN) {
            setIsCalculatingFare(true);
            setMinFare(0);
            setMaxFare(0);
            let urbanTaxiFare = 24 + ((orderDistance - 2) / 0.2) * 1.7;
            if (urbanTaxiFare < 24) {
                urbanTaxiFare = 24;
            }
            let ntTaxiFare = 20.5 + ((orderDistance - 2) / 0.2) * 1.5;
            if (ntTaxiFare < 20.5) {
                ntTaxiFare = 20.5;
            }
            let lantauTaxiFare = 19 + ((orderDistance - 2) / 0.2) * 1.5;
            if (lantauTaxiFare < 19) {
                lantauTaxiFare = 19;
            }

            if (isRedTaxi && !isGreenTaxi && !isBlueTaxi) {
                setMinFare(urbanTaxiFare);
                setValue("minFare", urbanTaxiFare);
                setIsCalculatingFare(false);
            } else if (!isRedTaxi && isGreenTaxi && !isBlueTaxi) {
                setMinFare(ntTaxiFare);
                setValue("minFare", ntTaxiFare);
                setIsCalculatingFare(false);
            } else if (!isRedTaxi && !isGreenTaxi && isBlueTaxi) {
                setMinFare(lantauTaxiFare);
                setValue("minFare", lantauTaxiFare);
                setIsCalculatingFare(false);
            } else if (isRedTaxi && isGreenTaxi && !isBlueTaxi) {
                setMinFare(ntTaxiFare);
                setValue("minFare", ntTaxiFare);
                setMaxFare(urbanTaxiFare);
                setIsCalculatingFare(false);
            } else if (isRedTaxi && !isGreenTaxi && isBlueTaxi) {
                setMinFare(lantauTaxiFare);
                setValue("minFare", lantauTaxiFare);
                setMaxFare(urbanTaxiFare);
                setIsCalculatingFare(false);
            } else if (!isRedTaxi && isGreenTaxi && isBlueTaxi) {
                setMinFare(lantauTaxiFare);
                setValue("minFare", lantauTaxiFare);
                setMaxFare(ntTaxiFare);
                setIsCalculatingFare(false);
            } else {
                setMinFare(lantauTaxiFare);
                setValue("minFare", lantauTaxiFare);
                setMaxFare(urbanTaxiFare);
                setIsCalculatingFare(false);
            }
        }
    }, [isRedTaxi, isGreenTaxi, isBlueTaxi, orderDistance])

    useEffect(() => {
        if (isOrderLoading == -1 && isOrderActive) {
            dispatch(clearSearchInput())
            setConfirmModalVisible(!confirmModalVisible);
            setSuccessModalVisible(!successModalVisible);
        }
        if (isOrderLoading == -1 && !isOrderActive) {
            setConfirmModalVisible(!confirmModalVisible);
            setFailModalVisible(!failModalVisible);
        }
    }, [isOrderLoading])

    useEffect(() => {
        if (originArea == destArea) {
            setShowTunnel(false);
            setValue("isCentralTunnel", false);
            setValue("isEastTunnel", false);
            setValue("isWestTunnel", false);
            setIsAllTunnel(false);
            setIsCentralTunnel(false);
            setIsEastTunnel(false);
            setIsWestTunnel(false);
        }

        if ((originArea == "Hong Kong Island" && destArea == "香港島") || (originArea == "香港島" && destArea == "Hong Kong Island")) {
            setShowTunnel(false);
            setValue("isCentralTunnel", false);
            setValue("isEastTunnel", false);
            setValue("isWestTunnel", false);
            setIsAllTunnel(false);
            setIsCentralTunnel(false);
            setIsEastTunnel(false);
            setIsWestTunnel(false);
        }

        if ((originArea == "新界" && destArea == "九龍") || (originArea == "九龍" && destArea == "新界")) {
            setShowTunnel(false);
            setValue("isCentralTunnel", false);
            setValue("isEastTunnel", false);
            setValue("isWestTunnel", false);
            setIsAllTunnel(false);
            setIsCentralTunnel(false);
            setIsEastTunnel(false);
            setIsWestTunnel(false);
        }
    }, [originArea, destArea])

    function selectAllTunnel() {
        if (isAllTunnel) {
            setIsAllTunnel(false);

            setValue("isCentralTunnel", false);
            setIsCentralTunnel(false);

            setValue("isEastTunnel", false);
            setIsEastTunnel(false);

            setValue("isWestTunnel", false);
            setIsWestTunnel(false);
        } else {
            setIsAllTunnel(true);

            setValue("isCentralTunnel", true);
            setIsCentralTunnel(true);

            setValue("isEastTunnel", true);
            setIsEastTunnel(true);

            setValue("isWestTunnel", true);
            setIsWestTunnel(true);
        }
    }

    function selectCentralTunnel() {
        let updatedState = !isCentralTunnel;
        setValue("isCentralTunnel", updatedState);
        setIsCentralTunnel(updatedState);

        if (isAllTunnel) {
            setIsAllTunnel(false);
        }
    }

    function selectEastTunnel() {
        let updatedState = !isEastTunnel;
        setValue("isEastTunnel", updatedState);
        setIsEastTunnel(updatedState);

        if (isAllTunnel) {
            setIsAllTunnel(false);
        }
    }

    function selectWestTunnel() {
        let updatedState = !isWestTunnel;
        setValue("isWestTunnel", updatedState);
        setIsWestTunnel(updatedState);

        if (isAllTunnel) {
            setIsAllTunnel(false);
        }
    }

    const { register, setValue, handleSubmit, control, reset, formState: { errors } } = useForm<OrderFormInput>({
        defaultValues: {
            headcount: headcount,
            isPool: isPool,
            isHybrid: isHybrid,
            isRedTaxi: isRedTaxi,
            isGreenTaxi: isGreenTaxi,
            isBlueTaxi: isBlueTaxi,
            isCentralTunnel: isCentralTunnel,
            isEastTunnel: isEastTunnel,
            isWestTunnel: isWestTunnel,
            hasPet: hasPet,
            isSmokeFree: isSmokeFree,
            hasLuggage: hasLuggage,
            isDisabled: isDisabled,
            isUrgent: isUrgent,
        }
    })

    const onSubmit: SubmitHandler<OrderFormInput> = data => dispatch(fetchCreateOrder(data));

    if (errors === true) {
        console.log('errors', errors);
    }

    return (
        <View style={styles.container}>
            <ScrollView >
                <View style={styles.innerContainer}>
                    <View style={styles.categoryElem}>
                        <Text style={styles.categoryLabel}>起點:</Text>
                        <Text>{originDescription}</Text>
                    </View>
                    <View style={styles.categoryElem}>
                        <Text style={styles.categoryLabel}>目的地:</Text>
                        <Text>{destinationDescription}</Text>
                    </View>
                    <View style={styles.categoryElem}>
                        <Text style={styles.categoryLabel}>估計車資:</Text>
                        {!isCalculatingFare && maxFare == 0 ? <Text>HKD ${minFare.toFixed(1)}</Text> : <Text>HKD ${minFare.toFixed(1)} ~ {maxFare.toFixed(1)}</Text>}
                    </View>
                    <View style={styles.categoryElem}>
                        <Text style={styles.categoryLabel}>距離:</Text>
                        <Text>{orderDistance?.toFixed(1)}km</Text>
                    </View>
                    <View style={styles.poolSwitch}>
                        <View style={styles.switchElem}>
                            <Text style={styles.categoryLabel}>共乘:</Text>
                        </View>
                        <View style={styles.SwitchBtn}>
                            <Switch
                                onValueChange={(bool) => {
                                    let updatedState = bool;
                                    setValue("isPool", updatedState)
                                    setIsPool(updatedState);
                                }}
                                value={isPool} />
                        </View>
                    </View>
                    <View style={styles.categoryElem}>
                        <Text style={styles.categoryLabel}>請選擇乘客數目</Text>
                        <View style={styles.categoryContainer}>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => {
                                    setValue("headcount", 1);
                                    setHeadcount(1);
                                }}
                            >
                                <View style={headcount === 1 ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialCommunityIcons name="numeric-1" style={styles.icon} />
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => {
                                    setValue("headcount", 2);
                                    setHeadcount(2);
                                }}
                            >
                                <View style={headcount === 2 ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialCommunityIcons name="numeric-2" style={styles.icon} />
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => {
                                    setValue("headcount", 3);
                                    setHeadcount(3);
                                }}
                            >
                                <View style={headcount === 3 ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialCommunityIcons name="numeric-3" style={styles.icon} />
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => {
                                    setValue("headcount", 4);
                                    setHeadcount(4);
                                }}
                            >
                                <View style={headcount === 4 ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialCommunityIcons name="numeric-4" style={styles.icon} />
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => {
                                    setValue("headcount", 5);
                                    setHeadcount(5);
                                }}
                            >
                                <View style={headcount === 5 ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialCommunityIcons name="numeric-5" style={styles.icon} />
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>

                    <View style={styles.categoryElem}>
                        <Text style={styles.categoryLabel}>請選擇車種</Text>
                        <View style={styles.categoryContainer}>
                            {<TouchableHighlight
                                underlayColor={''}
                                onPress={() => {
                                    let updatedState = !isRedTaxi;
                                    setValue("isRedTaxi", updatedState);
                                    setIsRedTaxi(updatedState);
                                }}
                            >
                                <View style={isRedTaxi ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialCommunityIcons name="taxi" color="red" style={styles.icon} />
                                    <Text style={styles.iconLabel}>紅的</Text>
                                </View>
                            </TouchableHighlight>}
                            {originArea !== "香港島" && originArea !== "Hong Kong Island" && originArea !== "九龍" && destArea !== "香港島" && destArea !== "Hong Kong Island" && destArea !== "九龍" &&
                                <TouchableHighlight
                                    underlayColor={''}
                                    onPress={() => {
                                        let updatedState = !isGreenTaxi;
                                        setValue("isGreenTaxi", updatedState);
                                        setIsGreenTaxi(updatedState);
                                    }}
                                >
                                    <View style={isGreenTaxi ? styles.iconSelected : styles.iconDeselected}>
                                        <MaterialCommunityIcons name="taxi" color="green" style={styles.icon} />
                                        <Text style={styles.iconLabel}>綠的</Text>
                                    </View>
                                </TouchableHighlight>}
                            {originArea !== "香港島" && originArea !== "Hong Kong Island" && originArea !== "九龍" && destArea !== "香港島" && destArea !== "Hong Kong Island" && destArea !== "九龍" &&
                                <TouchableHighlight
                                    underlayColor={''}
                                    onPress={() => {
                                        let updatedState = !isBlueTaxi;
                                        setValue("isBlueTaxi", updatedState);
                                        setIsBlueTaxi(updatedState);
                                    }}
                                >
                                    <View style={isBlueTaxi ? styles.iconSelected : styles.iconDeselected}>
                                        <MaterialCommunityIcons name="taxi" color="skyblue" style={styles.icon} />
                                        <Text style={styles.iconLabel}>藍的</Text>
                                    </View>
                                </TouchableHighlight>}

                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => {
                                    let updatedState = !isHybrid;
                                    setValue("isHybrid", updatedState);
                                    setIsHybrid(updatedState);
                                }}
                            >
                                <View style={isHybrid ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialCommunityIcons name="car-electric" style={styles.icon} />
                                    <Text style={styles.iconLabel}>混能車</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>

                    {showTunnel && <View style={styles.categoryElem}>
                        <Text style={styles.categoryLabel}>請選擇海底隧道</Text>
                        <View style={styles.categoryContainer}>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => selectAllTunnel()}
                            >
                                <View style={isAllTunnel ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="money" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>任何</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => selectCentralTunnel()}
                            >
                                <View style={isCentralTunnel ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="money" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>紅隧</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => selectEastTunnel()}
                            >
                                <View style={isEastTunnel ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="money" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>東隧</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => selectWestTunnel()}
                            >
                                <View style={isWestTunnel ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="money" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>西隧</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>}

                    <View style={styles.categoryElem}>
                        <Text style={styles.categoryLabel}>其他選擇</Text>
                        <View style={styles.categoryContainer}>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => {
                                    let updatedState = !hasPet;
                                    setValue("hasPet", updatedState);
                                    setHasPet(updatedState);
                                }}
                            >
                                <View style={hasPet ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="pets" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>寵物</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => {
                                    let updatedState = !isSmokeFree;
                                    setValue("isSmokeFree", updatedState);
                                    setIsSmokeFree(updatedState);
                                }}
                            >
                                <View style={isSmokeFree ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="smoke-free" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>不吸煙</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => {
                                    let updatedState = !hasLuggage;
                                    setValue("hasLuggage", updatedState);
                                    setHasLuggage(updatedState);
                                }}
                            >
                                <View style={hasLuggage ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="luggage" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>行李</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => {
                                    let updatedState = !isDisabled;
                                    setValue("isDisabled", updatedState);
                                    setIsDisabled(updatedState);
                                }}
                            >
                                <View style={isDisabled ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="wheelchair-pickup" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>傷殘人仕</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => {
                                    let updatedState = !isUrgent;
                                    setValue("isUrgent", updatedState);
                                    setIsUrgent(updatedState);
                                }}
                            >
                                <View style={isUrgent ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialCommunityIcons name="run-fast" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>緊急</Text>
                                </View>
                            </TouchableHighlight>

                        </View>
                    </View>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => {
                            setConfirmModalVisible(true)
                        }}>

                        <Text style={styles.buttonText}>傳送訂單</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>

            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={confirmModalVisible}
                    onRequestClose={() => {
                        setConfirmModalVisible(!confirmModalVisible);
                    }}
                >
                    <View style={styles.modalCenteredView}>
                        {/* Confirm order modal */}
                        <View style={styles.modalView}>
                            <Text style={styles.modalText}>提交您的訂單嗎?</Text>
                            <View style={styles.modalButtonRow}>
                                {isLoading == 1 && <SplashScreen />}
                                <TouchableOpacity
                                    style={[styles.modalButton, styles.modalButtonClose]}
                                    onPress={async () => {
                                        setIsOrderLoading(1);
                                        if (!isRedTaxi && !isGreenTaxi && !isBlueTaxi) {
                                            setHasError(true);
                                        } else {
                                            await handleSubmit(onSubmit)();
                                            setHasError(false);
                                        }
                                        setIsOrderLoading(-1);
                                    }}
                                >
                                    <Text style={styles.modalTextStyle}>確認訂單</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[styles.modalButton, styles.modalButtonClose]}
                                    onPress={() => setConfirmModalVisible(!confirmModalVisible)}
                                >
                                    <Text style={styles.modalTextStyle}>取消訂單</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={successModalVisible}
                    onRequestClose={() => {
                        setSuccessModalVisible(!successModalVisible);
                    }}
                >
                    <View style={styles.modalCenteredView}>
                        {/* Success sent order Modal */}
                        <View style={styles.modalView}>
                            <Text style={styles.modalText}>成功提交訂單!</Text>
                            <View style={styles.modalButtonRow}>
                                <Pressable
                                    style={[styles.modalButton, styles.modalButtonClose]}
                                    onPress={() => {
                                        setSuccessModalVisible(!successModalVisible);
                                        navigation.dispatch(TabActions.jumpTo('進行中'));
                                    }}>
                                    <Text style={styles.modalTextStyle}>監察最新狀態</Text>
                                </Pressable>

                            </View>
                        </View>
                    </View>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={failModalVisible}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        setFailModalVisible(!failModalVisible);
                    }}
                >
                    <View style={styles.modalCenteredView}>
                        {/* Fail sent order Modal */}
                        <View style={styles.modalView}>
                            <Text style={styles.modalText}>無法傳送訂單</Text>
                            {hasError && <Text style={styles.modalText}>請選擇的士種類</Text>}
                            <View style={styles.modalButtonRow}>
                                <Pressable
                                    style={[styles.modalButton, styles.modalButtonClose]}
                                    onPress={() => {
                                        setFailModalVisible(!failModalVisible)
                                        setHasError(false);
                                    }}>
                                    <Text style={styles.modalTextStyle}>返回訂單</Text>
                                </Pressable>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View >
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    innerContainer: {
        margin: 20
    },
    categoryElem: {
        marginBottom: 8
    },
    categoryLabel: {
        fontWeight: "bold",
        fontSize: 16,
        color: "rgba(90,90,90,1)",
    },
    categoryContainer: {
        flexDirection: "row",
        marginVertical: 10,
        flexWrap: "wrap",
    },
    iconSelected: {
        opacity: 1,
        marginBottom: 10,
    },
    iconDeselected: {
        opacity: 0.2,
        marginBottom: 10,
    },
    icon: {
        marginHorizontal: 5,
        fontSize: 50
    },
    iconLabel: {
        textAlign: "center",
        fontWeight: "600",
        color: "black"
    },
    button: {
        alignItems: "center",
        backgroundColor: "#566246",
        padding: 10,
        marginVertical: 5,
        borderRadius: 10,
    },
    buttonText: {
        textAlign: 'center',
        color: '#f1f2eb',
        fontWeight: 'bold',
    },
    modalCenteredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    modalButtonRow: {
        flexDirection: "row",
    },
    modalButton: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        margin: 5,
    },
    modalButtonOpen: {
        backgroundColor: "#F194FF",
    },
    modalButtonClose: {
        backgroundColor: "#566246",
    },
    modalTextStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    poolSwitch: {
        // backgroundColor: "grey",
        flexDirection: "row",
        justifyContent: "flex-start",
        marginVertical: 15,
    },
    switchElem: {
        // backgroundColor: "yellow",
        justifyContent: "center",
        alignItems: "center",
    },
    SwitchBtn: {
        // backgroundColor: "orange",
        marginLeft: "60%",
        marginVertical: 5,
    }
});