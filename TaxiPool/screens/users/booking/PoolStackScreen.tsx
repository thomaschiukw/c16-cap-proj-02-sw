import * as React from 'react';
import { createNativeStackNavigator, NativeStackScreenProps } from "@react-navigation/native-stack";
import PoolList from './PoolList';
import PoolDetail from './PoolDetail';
import PoolJoined from './PoolJoined';
import PoolFetching from './PoolFetching';

export interface PoolOrder {
    id: number;
    origin_place_id: string;
    origin_lat: number;
    origin_lng: number;
    origin_district: string;
    origin_description: string;
    destination_place_id: string;
    destination_lat: number;
    destination_lng: number;
    destination_district: string;
    destination_description: string;
    driver_session_id?: any;
    fare: string;
    distance: string;
    pool: boolean;
    pool_deadline: Date;
    hybrid: boolean;
    red_taxi: boolean;
    green_taxi: boolean;
    blue_taxi: boolean;
    cross_harbour_tunnel: boolean;
    eastern_harbour_crossing: boolean;
    western_harbour_crossing: boolean;
    has_pet: boolean;
    smoke_free: boolean;
    has_luggage: boolean;
    is_disabled: boolean;
    urgent: boolean;
    status: string;
    remarks?: any;
    created_at: Date;
    updated_at: Date;
    headcount_sum?: string;
    name?: string;
    rego?: string;
    headcount?: number;
    username?: string;
}

export type PoolStackScreenParamList = {
    Fetching: undefined;
    Event: undefined;
    Detail: { order: PoolOrder };
    Joined: { orders: PoolOrder[] };
};

export type PoolListDetailProp = NativeStackScreenProps<PoolStackScreenParamList, 'Detail'>;
export type PoolJoinedProp = NativeStackScreenProps<PoolStackScreenParamList, 'Joined'>;

const Stack = createNativeStackNavigator<PoolStackScreenParamList>();

export function PoolStackScreen() {
    return (
        <Stack.Navigator
            initialRouteName="Fetching"

        >
            <Stack.Screen
                name="Fetching"
                component={PoolFetching}
                options={{
                    headerShown: false,
                    gestureEnabled: false,
                    headerBackVisible: false,
                    animation: "fade"
                }}
            />
            <Stack.Screen
                name="Event"
                component={PoolList}
                options={{
                    headerShown: false,
                    animation: "fade"
                }}
            />
            <Stack.Screen
                name="Detail"
                component={PoolDetail}
                options={{ title: "返回" }}
            />
            <Stack.Screen
                name="Joined"
                component={PoolJoined}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
};