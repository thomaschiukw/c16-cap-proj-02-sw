import * as React from 'react';
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import MapScreen from './MapScreen';
import OrderSettingsScreen from './OrderSettingsScreen';

export type BookingStackScreenParamList = {
    Map: undefined;
    OrderConfig: undefined;
};

const Stack = createNativeStackNavigator<BookingStackScreenParamList>();

export function BookingStackScreen() {
    return (
        <Stack.Navigator
            initialRouteName="Map"
        >
            <Stack.Screen
                name="Map"
                component={MapScreen}
                options={{
                    headerBackVisible: false,
                    gestureEnabled: false,
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="OrderConfig"
                component={OrderSettingsScreen}
                options={{
                    title: "下單設定",
                    headerBackVisible: true,
                }}
            />
        </Stack.Navigator>
    );
}