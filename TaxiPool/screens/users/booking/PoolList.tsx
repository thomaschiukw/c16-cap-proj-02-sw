//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import React, { useState, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, Text, SafeAreaView } from 'react-native';
import { DataTable } from 'react-native-paper';
import { useSelector, useDispatch } from 'react-redux';
import { NavigationProp, useNavigation, useIsFocused, StackActions } from '@react-navigation/native';
import { PoolStackScreenParamList } from './PoolStackScreen';
import { RootState } from '../../../store';
import NoResult from '../../noResult/noResult';
import { fetchPoolList } from "../../../redux/order/thunk";
import SplashScreen from '../../../components/SplashScreen';
import { socket } from '../../../App';

export default function PoolList() {
    const navigation: NavigationProp<PoolStackScreenParamList> = useNavigation();
    const dispatch = useDispatch();
    const orders = useSelector((state: RootState) => state.order.activePoolOrder);
    const userId = useSelector((state: RootState) => state.auth.user?.id);
    const [rendered, setRendered] = useState(0);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        if (userId) {
            dispatch(fetchPoolList(userId));
            setRendered(state => state + 1);
        }
    }, [])

    useEffect(() => {
        if (rendered === 1) {
            setIsLoading(false)
        }
    }, [orders])

    useEffect(() => {
        socket.on("order-accepted", () => {
            console.log("accepted");
            if (userId !== undefined) {
                dispatch(fetchPoolList(userId));
            }
        })

        socket.on("order-started", () => {
            navigation.dispatch(StackActions.replace('Fetching'));

        })

        socket.on("order-completed", () => {
            navigation.dispatch(StackActions.replace('Fetching'));

        })
        return () => { socket.removeAllListeners() };
    }, [])

    return (

        <SafeAreaView style={styles.container}>
            {isLoading && <SplashScreen />}
            <Text style={styles.pool}>共乘<Text style={styles.list}>列表</Text></Text>
            <DataTable style={styles.DataTableContainer}>
                <DataTable.Header style={styles.eventHeader}>
                    <DataTable.Title>起點</DataTable.Title>
                    <DataTable.Title> </DataTable.Title>
                    <DataTable.Title>目的地</DataTable.Title>
                    <DataTable.Title> </DataTable.Title>
                </DataTable.Header>

                <SafeAreaView style={styles.container}>
                    {orders.length == 0 && !isLoading && <NoResult />}
                </SafeAreaView>
                {orders.length > 0 && !isLoading &&
                    orders.map((order: any) => (
                        <TouchableOpacity
                            key={order.id}
                            activeOpacity={0.7}
                            onPress={() => {
                                navigation.navigate('Detail', { order: order })
                            }}>

                            <DataTable.Row style={styles.eventRow}>
                                <DataTable.Cell>
                                    <Text style={styles.eventText}>{order.origin_district}</Text>
                                </DataTable.Cell>
                                <DataTable.Cell style={styles.toEventRow}>
                                    <Text style={styles.toEventText}>到</Text>
                                </DataTable.Cell>
                                <DataTable.Cell>
                                    <Text style={styles.eventText}>{order.destination_district}</Text>
                                </DataTable.Cell>
                                <DataTable.Cell style={styles.buttonCell}>

                                    <Text>{'>'}</Text>

                                </DataTable.Cell>
                            </DataTable.Row>

                        </TouchableOpacity>
                    ))}

            </DataTable>

        </SafeAreaView >

    );
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 30,
        paddingHorizontal: 30,
        // backgroundColor: '#a4c2a5',
    },
    eventHeader: {
        borderColor: '#f1f2eb',
    },
    eventRow: {
        backgroundColor: '#d8dad3',
        borderRadius: 15,
        marginVertical: 5,
        // height: '30%',
    },
    eventText: {
        color: '#4a4a48',
        flexWrap: 'wrap',
        // backgroundColor: 'yellow',
    },
    buttonCell: {
        justifyContent: 'center',
    },
    buttonText: {
        color: 'white',
    },
    button: {
        backgroundColor: '#566246',
        borderRadius: 13,
        justifyContent: 'center',
        paddingHorizontal: 15,
        paddingVertical: 7,
    },
    buttonContainer: {
        // backgroundColor: 'blue',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    selectbutton: {
        // backgroundColor: '#566246',
        borderRadius: 13,
        justifyContent: 'center',
        paddingHorizontal: 35,
        paddingVertical: 15,
        borderWidth: 2,
    },
    selectButtonText: {
        color: 'black',
        fontSize: 18,
    },
    toEventText: {
        // backgroundColor: 'red',
    },
    toEventRow: {
        justifyContent: 'center',
    },
    DataTableContainer: {
        // flex: 1,
        height: '100%',
    },
    iconSelected: {
        backgroundColor: '#566246',
        color: '#f1f2eb',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#a4c2a5'
    },
    iconDeselected: {
        backgroundColor: '#d8dad3',
        color: '#4a4a48',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#f1f2eb'
    },
    pool: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#a4c2a5',
        textAlign: 'center',
    },
    list: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#4a4a48'
    },
});
