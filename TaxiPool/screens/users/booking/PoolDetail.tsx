//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, Text, View, Modal, Pressable, Alert, TextInput, TouchableOpacity } from "react-native";
import { NavigationProp, useNavigation } from '@react-navigation/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useSelector } from "react-redux";
import { RootState } from '../../../store';
import { PoolListDetailProp, PoolStackScreenParamList } from './PoolStackScreen';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { StackActions } from "@react-navigation/routers";
import { SafeAreaView } from "react-native-safe-area-context";

export default function PoolDetail({ route }: PoolListDetailProp) {
    const navigation: NavigationProp<PoolStackScreenParamList> = useNavigation();
    // const [orderView, setorderView] = useState([true, false, false]);
    const [submitModalVisible, setSubmitModalVisible] = useState(false);
    const [alertModalVisible, setAlertModalVisible] = useState(false);
    const userId = useSelector((state: RootState) => state.auth.user?.id);

    const [headcount, setHeadcount] = useState('1')
    // const order=route.params!.order
    const orders = useSelector((state: RootState) => state.order.activePoolOrder);
    const orderId = route.params!.order.id
    // console.log(orderId);

    const order = (orders.filter(order => order.id == orderId))[0];
    // console.log('detail: ', order);

    // console.log("orders", order);

    //timer
    const currentTime = new Date();
    const poolDeadline = new Date(order.pool_deadline);
    let secondsDiff = ((poolDeadline.getTime() - currentTime.getTime()) / 1000);
    const [seconds, setSeconds] = useState(0);
    const [timerMin, setTimerMin] = useState(0)
    const [timerSec, setTimerSec] = useState(0)
    useEffect(() => {
        setSeconds(Math.round(secondsDiff))
        const interval = setInterval(() => {
            setSeconds(seconds => seconds - 1);
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    useEffect(() => {
        setTimerSec(seconds % 60)
        setTimerMin(Math.floor(seconds / 60))
    }, [seconds]);

    
    const activePoolOrder = useSelector((state: RootState) => state.order.activePoolOrder);

    async function submitJoin(userId: number, orderId: number, headcount: string) {
        try {
            const joinForm = {
                userId: userId,
                orderId: orderId,
                headcount: parseInt(headcount)
            }
            const res = await fetch(`${REACT_APP_BACKEND_URL}/join-pool/`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(joinForm),
            });
            const result = await res.json()

        } catch (error) {
            console.log(error);
        }

    }
    return (
        <SafeAreaView edges={['top', 'left', 'right']} style={styles.pageContainer}>
            {/* {isLoading == 1 && <SplashScreen />}
    {isLoading === -1 && !(activeOrder.length >= 1) && <NoResult />}
    {isLoading === -1 && activeOrder.length >= 1 &&  */}
            <ScrollView style={styles.container}>



                <View style={styles.innerContainer}>
                    <View style={styles.categoryContainer}>
                        <View style={styles.section}>
                            {seconds >= 0 ? <View><Text style={styles.sectionTitle}>剩餘時間 {timerMin < 10 && 0}{timerMin}:{timerSec < 10 && 0}{timerSec}</Text></View> :

                                <View>
                                    <Text style={styles.sectionTitle}>已過共乘時限</Text>
                                </View>

                            }
                            {/* {timerSec > 0 ? <View><Text style={styles.sectionTitle}>剩餘時間 {timerMin}:{timerSec}</Text></View> :
                                timerMin > 0 ? <View><Text style={styles.sectionTitle}>剩餘時間 {timerMin}:{timerSec}</Text></View> :

                                    <View>
                                        <Text style={styles.sectionTitle}>已失效</Text>
                                    </View>

                            } */}
                        </View>
                    </View>
                    <View style={styles.locationContainer}>
                        <View style={styles.locationLabelContainer}>
                            <Text style={styles.locationLabel}>起點</Text>
                        </View>
                        <Text style={styles.location}>{order.origin_description}</Text>
                    </View>
                    <View style={styles.locationContainer}>
                        <View style={styles.locationLabelContainer}>
                            <Text style={styles.locationLabel}>目的地</Text>
                        </View>
                        <Text style={styles.location}>{order.destination_description}</Text>
                    </View>

                    <View style={styles.section}>
                        <View><Text style={styles.sectionTitle}>訂單詳細</Text></View>
                        <View style={styles.categoryContainer}>
                            <Text style={styles.categoryLabel}>狀態</Text>
                            <View style={styles.categoryElem}>
                                <Text style={styles.text}>
                                    {order.status == 'searching' && <Text>待司機接單</Text>}
                                    {order.status == 'pending' && <Text>等待司機接客</Text>}
                                    {order.status == 'travelling' && <Text>駕駛中</Text>}
                                    {order.status == 'complete' && <Text>已完成</Text>}
                                </Text>
                            </View>
                        </View>

                        <View style={styles.categoryContainer}>
                            <Text style={styles.categoryLabel}>共乘人數</Text>
                            <View style={styles.categoryElem}>
                                <Text style={styles.text}>
                                    {order.headcount_sum}
                                </Text>
                            </View>
                        </View>


                        {order.status !== 'searching' &&
                            <View style={styles.categoryContainer}>
                                <Text style={styles.categoryLabel}>司機資料</Text>
                                <View style={styles.categoryElem}>
                                    <View style={styles.categoryContainer}>
                                        <View style={styles.categoryElem}>
                                            <Text style={styles.text}>
                                                姓名: {activePoolOrder[0].name}
                                            </Text>
                                        </View>
                                        <View style={styles.categoryElem}>
                                            <Text style={styles.text}>
                                                車牌: {activePoolOrder[0].rego}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>}
                    </View>

                    <View style={styles.section}>
                        <View><Text style={styles.sectionTitle}>乘客要求</Text></View>
                        <View style={styles.categoryContainer}>
                            <Text style={styles.categoryLabel}>海底隧道</Text>
                            <View style={styles.categoryElem}>
                                {order.cross_harbour_tunnel || order.eastern_harbour_crossing || order.western_harbour_crossing ?
                                    <View></View> : <Text style={styles.text}>不適用</Text>}

                                {order.cross_harbour_tunnel && <View style={styles.iconSelected}>
                                    <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>紅隧</Text>
                                </View>}
                                {order.eastern_harbour_crossing && <View style={styles.iconSelected}>
                                    <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>東隧</Text>
                                </View>}
                                {order.western_harbour_crossing && <View style={styles.iconSelected}>
                                    <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>西隧</Text>
                                </View>}
                            </View>
                        </View>

                        <View style={styles.categoryContainer}>
                            <Text style={styles.categoryLabel}>其他要求</Text>
                            <View style={styles.categoryElem}>
                                {order.has_pet || order.smoke_free || order.has_luggage || order.is_disabled || order.urgent ?
                                    <View></View> : <Text style={styles.text}> 無特別要求</Text>}


                                {order.has_pet && <View style={styles.iconSelected}>
                                    <MaterialIcons name="pets" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>寵物</Text>
                                </View>}

                                {order.smoke_free && <View style={styles.iconSelected}>
                                    <MaterialIcons name="smoke-free" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>不吸煙</Text>
                                </View>}

                                {order.has_luggage && <View style={styles.iconSelected}>
                                    <MaterialIcons name="luggage" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>行李</Text>
                                </View>}

                                {order.is_disabled && <View style={styles.iconSelected}>
                                    <MaterialIcons name="wheelchair-pickup" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>傷殘人仕</Text>
                                </View>}

                                {order.urgent && <View style={styles.iconSelected}>
                                    <MaterialCommunityIcons name="run-fast" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>緊急</Text>
                                </View>}


                            </View>
                        </View>


                    </View>


                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={submitModalVisible}
                        onRequestClose={() => {
                            setSubmitModalVisible(!submitModalVisible);
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.modalText}>請輸入共乘人數: </Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <TextInput
                                        style={styles.input}
                                        onChangeText={value => setHeadcount(value)}
                                        keyboardType='numeric'
                                        value={headcount}
                                    />
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Pressable
                                        style={styles.button}
                                        onPress={() => {

                                            if (userId !== undefined) {
                                                if ((parseInt(headcount) + parseInt(order.headcount_sum)) < 5) {
                                                    // console.log(headcount);

                                                    submitJoin(userId, order.id, headcount)
                                                    setSubmitModalVisible(!submitModalVisible)
                                                    navigation.dispatch(StackActions.replace('Fetching'));

                                                } else {
                                                    // console.log((parseInt(headcount) + parseInt(order.headcount_sum)));

                                                    setAlertModalVisible(!alertModalVisible)
                                                    //confirm max no of passengers
                                                }
                                            }
                                        }}
                                    >
                                        <Text style={styles.textStyle}>確認</Text>
                                    </Pressable>
                                    <Pressable
                                        style={styles.button}
                                        onPress={() => {
                                            setSubmitModalVisible(!submitModalVisible)
                                        }}
                                    >
                                        <Text style={styles.textStyle}>取消</Text>
                                    </Pressable>
                                </View>
                            </View>
                        </View>
                    </Modal>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={alertModalVisible}
                        onRequestClose={() => {
                            Alert.alert("Modal has been closed.");
                            setAlertModalVisible(!alertModalVisible);
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={[styles.alertModalView, styles.modalView]}>
                                <Text style={styles.modalText}>共乘人數上限: 4</Text>
                                <Text style={styles.modalText}>最多可共乘多{4 - parseInt(order.headcount_sum)}名乘客!</Text>
                                <Pressable
                                    style={styles.button}
                                    onPress={() => setAlertModalVisible(!alertModalVisible)}
                                >
                                    <Text style={styles.textStyle}>確定</Text>
                                </Pressable>
                            </View>
                        </View>
                    </Modal>


                    {seconds >= 0 && <TouchableOpacity
                        style={styles.button}
                        onPress={() => {
                            setSubmitModalVisible(!submitModalVisible)
                            // if (driverId) {
                            //     dispatch(fetchDriverPendingOrder(driverId));
                            // }
                        }}
                        activeOpacity={0.6}
                    >
                        <Text style={styles.buttonText}>共乘</Text>
                    </TouchableOpacity>

                    }
                </View>
            </ScrollView >
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    pageContainer: {
        justifyContent: 'center',
        flex: 1,

    },
    innerContainer: {
        margin: 15
    },
    locationLabelContainer: {
        backgroundColor: '#a4c2a5',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#566246',
        color: '#4a4a48',
        paddingVertical: 10,
        width: 80,
    },
    locationLabel: {
        fontWeight: 'bold',
        textAlign: 'center'
    },
    location: {
        paddingVertical: 15,
        color: '#4a4a48',
        fontSize: 28,
        flexWrap: 'wrap',
        fontWeight: 'bold',
    },
    locationContainer: {
        marginHorizontal: 10,
        // backgroundColor: '#a4c2a5',
        // flexDirection: 'column'
    },
    categoryContainer: {
        marginBottom: 10
    },
    categoryLabel: {
        fontWeight: "bold",
        marginHorizontal: 10,
        paddingVertical: 10,
        color: '#4a4a48',
        fontSize: 20
    },
    categoryElem: {
        flexDirection: "row",
        // marginVertical: 5,
        flexWrap: "wrap"
    },
    timer: {
        backgroundColor: '#566246',
        borderRadius: 15,
        padding: 10,
        // justifyContent: 'center',

    },
    timerText: {
        fontWeight: 'bold',
        fontSize: 30,
        color: '#f1f2eb',
        textAlign: 'center',
    },
    divider: {
        borderBottomColor: '#566246',
        borderBottomWidth: 1,
        opacity: 0.7,
        margin: 10
    },
    section: {
        backgroundColor: '#d8dad3',
        marginVertical: 10,
        borderRadius: 10,
        padding: 10,
    },
    sectionTitle: {
        backgroundColor: '#f1f2eb',
        borderRadius: 10,
        color: '#4a4a48',
        paddingVertical: 5,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 25,
    },
    iconSelected: {
        opacity: 1,
        marginBottom: 10,
        marginHorizontal: 20
    },
    iconDeselected: {
        opacity: 0.2,
        marginBottom: 15,
    },
    icon: {
        marginHorizontal: 10,
        fontSize: 50,
        color: '#4a4a48'
    },
    iconLabel: {
        textAlign: "center",
        fontWeight: "600",
        color: "black",
        fontSize: 17,
    },
    button: {
        alignItems: "center",
        backgroundColor: "#566246",
        padding: 10,
        margin: 5,
        borderRadius: 10,
    },
    buttonText: {
        textAlign: 'center',
        color: '#f1f2eb',
        fontWeight: 'bold',
    },
    inputContainer: {
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        flexDirection: 'row',
        flexWrap: "wrap",
    },
    input: {
        backgroundColor: 'white',
        marginVertical: 5,
        height: 40,
        width: '75%',
        borderRadius: 5,
        borderColor: '#4a4a48',
        borderWidth: 1,
        flex: 0.4,
        textAlign: 'center',

    },
    // buttons: {
    //     backgroundColor: '#566246',
    //     margin: 5,
    //     padding: '3%',
    //     borderRadius: 10,
    //     width: 250,
    // },
    text: {
        marginHorizontal: 20
    },
    //modal
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    alertModalView: {
        // paddingVertical: 100,
        height: 200,
        justifyContent: 'center',
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        paddingHorizontal: 35,
        paddingVertical: 15,

        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    // modalButton: {
    //     backgroundColor: '#a4c2a5'
    // },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});