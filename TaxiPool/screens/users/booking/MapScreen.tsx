//@ts-expect-error
import { GOOGLE_MAPS_API_KEY } from '@env';
//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import React, { useEffect, useRef, useState } from 'react';
import { Dimensions, Keyboard, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
// import Geolocation from 'react-native-geolocation-service';
import Geolocation from '@react-native-community/geolocation';
import MapViewDirections from 'react-native-maps-directions';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../store';
import MapPlacesInput from './MapPlacesInput';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { destinationCleared, originCleared, routeDetailsCalculated } from '../../../redux/map/actions';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { socket } from '../../../App';
import { fetchActiveOrder } from '../../../redux/order/thunk';
import { useIsFocused } from '@react-navigation/native';

interface DriverMarkers {
    userid: number;
    lat: number;
    lng: number;
    type: string;
    rego: string;
}

export default function MapScreen() {
    const mapView = useRef<MapView | null>(null) as React.MutableRefObject<MapView | null>;;
    const dispatch = useDispatch();
    const { width, height } = Dimensions.get('window');
    const aspectRatio = width / height;
    const userId = useSelector((state: RootState) => state.auth.user?.id);
    const origin = useSelector((state: RootState) => state.map.selectedOrigin.details?.place_id);
    const destination = useSelector((state: RootState) => state.map.selectedDestination.details?.place_id);
    const originCoord = useSelector((state: RootState) => state.map.selectedOrigin.coordinates);
    const destinationCoord = useSelector((state: RootState) => state.map.selectedDestination.coordinates);
    const activeOrder = useSelector((state: RootState) => state.order.activeOrder);
    const isOrderActive = useSelector((state: RootState) => state.order.isOrderActive);
    const [isLoading, setIsLoading] = useState(true);
    const [lat, setLat] = useState(22.25);
    const [long, setLong] = useState(114.1524);
    const [latitudeDelta, setLatitudeDelta] = useState(0.005);
    const [longitudeDelta, setLongitudeDelta] = useState(latitudeDelta * aspectRatio);
    const [driversMarker, setDriversMarker] = useState<DriverMarkers[]>([]);
    const [activeOrderFetched, setActiveOrderFetched] = useState(0);

    useEffect(() => {
        let mounted = true;
        Geolocation.getCurrentPosition(info => {
            // if (activeOrder && mounted) {
            //     setLat(info.coords.longitude);
            //     setLong(info.coords.longitude);
            // }
            // else if (!originCoord && !destinationCoord && mounted) {
            //     setLat(info.coords.latitude);
            //     setLong(info.coords.longitude);
            // } else {
            //     setLat(info.coords.latitude);
            //     setLong(info.coords.longitude);
            // }

            if (mounted) {
                setLat(info.coords.latitude);
                setLong(info.coords.longitude);
            }
        })
        return function cleanup() {
            mounted = false;
        }
    }, [])

    useEffect(() => {
        setActiveOrderFetched(state => state + 1);
        let mounted = false;
        
        // if (activeOrderFetched === 1) {
            if (activeOrder.length !== 0 && activeOrder[0].driver_session_id) {
                getOrderDriverLocation().then(data => {
                    if (!mounted && data) {
                        setDriversMarker(data);
                    }
                });
            } else {
                getDriverLocation().then(data => {
                    if (!mounted && data) {
                        setDriversMarker(data);
                    }
                });
            }

            if (activeOrder.length === 0) {
                dispatch(originCleared("CLEARED"));
                dispatch(destinationCleared("CLEARED"));
            }
        // }
        setIsLoading(false);

        return () => {
            mounted = true;
        }
    }, [activeOrder])

    useEffect(() => {
        let mounted = false;
        socket.on("order-completed", () => {
            console.log("order-completed");
            dispatch(originCleared("CLEARED"));
            dispatch(destinationCleared("CLEARED"));
            getDriverLocation().then(data => {
                if (!mounted && data) {
                    setDriversMarker(data);
                }
            });
            if (userId) {
                dispatch(fetchActiveOrder(userId));
            }
            socket.removeAllListeners("driver-location-changed")
        })

        socket.on("driver-location-changed", () => {
            // console.log("driver-location-changed");

            if (activeOrder.length !== 0) {
                console.log("driver-location-changed!!");

                getOrderDriverLocation().then(data => {
                    if (!mounted && data) {
                        setDriversMarker(data);
                    }
                });
            }
        })

        return () => {
            socket.removeAllListeners()
            mounted = true;
        };
    }, [])

    async function getOrderDriverLocation() {
        try {
            const res = await fetch(`${REACT_APP_BACKEND_URL}/user/onduty-driver/${activeOrder[0].driver_session_id}/location`);
            const result = await res.json();
            return result
            // setDriversMarker(result);
        } catch (error) {
            console.error(error);
        }
    }

    async function getDriverLocation() {
        try {
            const res = await fetch(`${REACT_APP_BACKEND_URL}/user/oncall-drivers/location`);
            const result: DriverMarkers[] = await res.json();
            return result
        } catch (error) {
            console.error(error);
        }
    }

    function updateCurrentLocation() {
        Geolocation.getCurrentPosition(info => {
            mapView.current?.animateToRegion({ // Takes a region object as parameter
                longitude: info.coords.longitude,
                latitude: info.coords.latitude,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005 * aspectRatio
            }, 1000);
        });
    }

    function animateToBearing() {
        Geolocation.getCurrentPosition(info => {
            // console.log(info)
            if (info.coords.heading && info.coords.altitude) {
                mapView.current?.animateCamera({
                    center: {
                        latitude: info.coords.latitude,
                        longitude: info.coords.longitude,
                    },
                    heading: info.coords.heading,
                    altitude: info.coords.altitude,
                    zoom: 18
                }, { duration: 1000 })
            } else {
                mapView.current?.animateCamera({
                    center: {
                        latitude: info.coords.latitude,
                        longitude: info.coords.longitude,
                    },
                    zoom: 18
                }, { duration: 1000 })
            }
        });
    }

    function animateToCurrentRoute() {
        if (activeOrder.length !== 0) {
            const x = (activeOrder[0].origin_lng + activeOrder[0].destination_lng) / 2;
            const y = ((activeOrder[0].origin_lat + activeOrder[0].destination_lat) / 2);
            const xDelta = (Math.sqrt(Math.pow((activeOrder[0].origin_lng - activeOrder[0].destination_lng), 2))) + 0.03;
            const yDelta = (Math.sqrt(Math.pow((activeOrder[0].origin_lat - activeOrder[0].destination_lat), 2))) + 0.05;

            mapView.current?.animateToRegion({ // Takes a region object as parameter
                longitude: x,
                latitude: y,
                latitudeDelta: xDelta,
                longitudeDelta: yDelta
            }, 1000);
        }

        if (originCoord && destinationCoord && activeOrder.length === 0) {
            const x = (originCoord.lng + destinationCoord.lng) / 2;
            const y = ((originCoord.lat + destinationCoord.lat) / 2) + 0.025;
            const xDelta = (Math.sqrt(Math.pow((originCoord.lng - destinationCoord.lng), 2))) + 0.03;
            const yDelta = (Math.sqrt(Math.pow((originCoord.lat - destinationCoord.lat), 2))) + 0.1;

            mapView.current?.animateToRegion({ // Takes a region object as parameter
                longitude: x,
                latitude: y,
                latitudeDelta: xDelta,
                longitudeDelta: yDelta
            }, 1000);
        }
    }

    return (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <View style={styles.container}>
                {!isLoading && <MapView
                    ref={mapView}
                    provider={PROVIDER_GOOGLE}
                    region={{
                        latitude: lat,
                        longitude: long,
                        latitudeDelta: latitudeDelta,
                        longitudeDelta: longitudeDelta,
                    }}
                    showsUserLocation={true}
                    followsUserLocation={true}
                    showsMyLocationButton={false}
                    showsCompass={true}
                    showsBuildings={false}
                    loadingEnabled={true}
                    mapPadding={{
                        top: 170,
                        left: 0,
                        bottom: 0,
                        right: 0
                    }}
                    style={styles.map}
                >
                    {activeOrder.length === 1 && <MapViewDirections
                        origin={`place_id:${activeOrder[0].origin_place_id}`}
                        destination={`place_id:${activeOrder[0].destination_place_id}`}
                        apikey={GOOGLE_MAPS_API_KEY}
                        strokeWidth={5}
                        strokeColor="purple"
                        // onStart={(params) => {
                        //     console.log(`Started routing between "${params.origin}" and "${params.destination}"`);
                        // }}
                        onReady={result => {
                            // console.log(`Distance: ${result.distance} km`);
                            // console.log(`Duration: ${result.duration} min.`);
                            // console.log(result.coordinates);
                            dispatch(routeDetailsCalculated(result.distance, result.duration, result.coordinates));
                        }}
                    />}
                    {(activeOrder.length === 0 && origin !== undefined && destination !== undefined) && <MapViewDirections
                        origin={`place_id:${origin}`}
                        destination={`place_id:${destination}`}
                        apikey={GOOGLE_MAPS_API_KEY}
                        strokeWidth={5}
                        strokeColor="purple"
                        onReady={result => {
                            dispatch(routeDetailsCalculated(result.distance, result.duration, result.coordinates));
                            animateToCurrentRoute();
                        }}
                    />}

                    {activeOrder.length === 0 && originCoord !== undefined && <Marker
                        coordinate={{ latitude: originCoord.lat, longitude: originCoord.lng }}
                        title={'出發地'}
                        pinColor={'red'}
                    />}
                    {destinationCoord !== undefined && <Marker
                        coordinate={{ latitude: destinationCoord.lat, longitude: destinationCoord.lng }}
                        title={'目的地'}
                        pinColor={'green'}
                    />}
                    {activeOrder.length !== 0 && <Marker
                        coordinate={{ latitude: activeOrder[0].origin_lat, longitude: activeOrder[0].origin_lng }}
                        title={'出發地'}
                        pinColor={'red'}
                    />}
                    {activeOrder.length !== 0 && <Marker
                        coordinate={{ latitude: activeOrder[0].destination_lat, longitude: activeOrder[0].destination_lng }}
                        title={'目的地'}
                        pinColor={'green'}
                    />}
                    {driversMarker.length !== 0 && driversMarker !== undefined &&
                        driversMarker.map((driverMarker, index) => (<Marker
                            key={index}
                            coordinate={{ latitude: driverMarker.lat, longitude: driverMarker.lng }}
                            title={`${driverMarker.rego}`}
                        >
                            <MaterialCommunityIcons name="taxi" color={driverMarker.type} size={20} />

                        </Marker>))}
                </MapView>}

                <View style={{
                    position: "absolute",
                    top: 10,
                    width: "100%",
                }}>
                    <MapPlacesInput />
                </View>

                <View style={{
                    position: "absolute",
                    bottom: 0,
                    width: "100%",
                }}>
                    <View style={styles.btnContainer}>
                        <TouchableOpacity
                            onPress={animateToCurrentRoute}
                            activeOpacity={0.8}
                            style={styles.btnElem}>
                            <MaterialIcons name="directions" color="black"
                                style={styles.btnIcon}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={updateCurrentLocation}
                            onLongPress={animateToBearing}
                            activeOpacity={0.8}
                            style={styles.btnElem}>
                            <MaterialIcons name="my-location" color="black"
                                style={styles.btnIcon}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback >
    )
};

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    btnContainer: {
        alignItems: "flex-end",
        borderRadius: 100
    },
    btnElem: {
        marginHorizontal: 10,
        marginBottom: 10,
        backgroundColor: 'rgba(25,114,232,1)',
        borderRadius: 200,
    },
    btnIcon: {
        bottom: 0,
        right: 0,
        fontSize: 25,
        padding: 15,
        color: 'white'
    }
});