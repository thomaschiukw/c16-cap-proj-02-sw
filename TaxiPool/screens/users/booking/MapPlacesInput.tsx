import React, { useEffect, useState } from 'react';
import { Keyboard, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { useDebouncedCallback } from 'use-debounce';
import { RootState } from '../../../store';
import { fetchDestination, fetchOrigin, setDestination, setOrigin } from '../../../redux/map/thunks';
import { BookingStackScreenParamList } from './BookingStackScreen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { clearSearchInput, destinationCleared, originCleared } from '../../../redux/map/actions';
import { socket } from '../../../App';
import { fetchActiveOrder } from '../../../redux/order/thunk';

export default function MapPlacesInput() {
    const dispatch = useDispatch();
    const navigation: NavigationProp<BookingStackScreenParamList> = useNavigation();
    const isLoading = useSelector((state: RootState) => state.loading.isLoading);
    const originPredictionStatus = useSelector((state: RootState) => state.map.originPredictions.status);
    const destinationPredictionStatus = useSelector((state: RootState) => state.map.destinationPredictions.status);
    const originPredictions = useSelector((state: RootState) => state.map.originPredictions.predictions);
    const destPredictions = useSelector((state: RootState) => state.map.destinationPredictions.predictions);
    const selectedOrigin = useSelector((state: RootState) => state.map.selectedOrigin?.details?.description);
    const selectedDestination = useSelector((state: RootState) => state.map.selectedDestination?.details?.description);
    const isOrderActive = useSelector((state: RootState) => state.order.isOrderActive);
    const userId = useSelector((state: RootState) => state.auth.user?.id);
    const activeOrder = useSelector((state: RootState) => state.order.activeOrder);
    const [isOriginHide, setOriginHide] = useState(true);
    const [isDestHide, setDestHide] = useState(true);
    const [originValue, setOriginValue] = useState(selectedOrigin || '');
    const [destValue, setDestValue] = useState(selectedDestination || '');
    const [hasOriginResult, setHasOriginResult] = useState(false);

    // useEffect(() => {
    //     if (userId && activeOrder.length == 0) {
    //         dispatch(fetchActiveOrder(userId));
    //     }
    // }, [])

    useEffect(() => {
        if (activeOrder.length !== 0) {
            setOriginValue(activeOrder[0].origin_description);
            setDestValue(activeOrder[0].destination_description);

        } else if (activeOrder.length === 0) {
            setOriginValue("");
            setDestValue("");
        }
    }, [activeOrder])

    useEffect(() => {
        socket.on("order-completed", () => {
            dispatch(originCleared("CLEARED"));
            dispatch(destinationCleared("CLEARED"));
            if (userId) {
                dispatch(fetchActiveOrder(userId));
            }
        })
        return () => { socket.removeAllListeners() };
    }, [])

    const originInputDebounced = useDebouncedCallback(
        (value) => {
            dispatch(fetchOrigin(value));
        }, 200
    );

    const destinationInputDebounced = useDebouncedCallback(
        (value) => {
            dispatch(fetchDestination(value));
        }, 200
    );

    useEffect(() => {
        if (originPredictionStatus !== "" && originPredictionStatus !== "OK") {
            setHasOriginResult(false);
        } else {
            setHasOriginResult(true);
        }

        if (originValue.length === 0) {
            setHasOriginResult(true);
        }

        if (isLoading === 1) {
            setHasOriginResult(true);
        }
    }, [originPredictionStatus, originValue, isLoading]);

    return (
        <View style={styles.container}>
            <View>
                <TextInput
                    style={styles.input}
                    placeholder="出發地"
                    onFocus={() => {
                        setOriginHide(false);
                        setDestHide(true);
                    }}
                    onChangeText={value => {
                        setOriginValue(value);
                        originInputDebounced(value);
                    }}
                    onBlur={() => setOriginHide(true)}
                    value={originValue}
                    editable={!isOrderActive}
                />
                {!isOrderActive && <View style={{ position: 'absolute', right: 10, top: 20 }}>
                    <TouchableOpacity
                        onPress={() => {
                            setOriginValue('');
                            dispatch(clearSearchInput());
                            dispatch(originCleared("CLEARED"));
                        }}
                    >
                        <MaterialCommunityIcons name="close" color="#566246" size={25} />
                    </TouchableOpacity>
                </View>}
            </View>
            {(originPredictions && !isOriginHide) && <View>
                <TouchableOpacity
                    style={styles.predictionsElem}
                >
                    <Text style={styles.predictionLabel}>使用我當前的位置</Text>
                </TouchableOpacity>
            </View>}
            {(originPredictions && !isOriginHide) && originPredictions.map((originPrediction) => (
                <View key={originPrediction.place_id}>
                    <TouchableOpacity
                        style={styles.predictionsElem}
                        onPress={() => {
                            setOriginValue(originPrediction.description!);
                            setOriginHide(true);
                            dispatch(setOrigin(originPrediction));
                            Keyboard.dismiss();
                        }}
                    >
                        <Text style={styles.predictionLabel}>{originPrediction.description}</Text>
                    </TouchableOpacity>
                </View>
            ))}
            {!hasOriginResult &&
                <View>
                    <TouchableOpacity
                        style={styles.predictionsElem}
                    >
                        <Text style={styles.predictionLabel}>未找到結果</Text>
                    </TouchableOpacity>
                </View>
            }

            <View>
                <TextInput
                    style={styles.input}
                    placeholder="目的地"
                    onFocus={() => {
                        setDestHide(false);
                        setOriginHide(true);
                    }}
                    onChangeText={value => {
                        setDestValue(value);
                        destinationInputDebounced(value);
                    }}
                    onBlur={() => setDestHide(true)}
                    value={destValue}
                    editable={!isOrderActive}
                />
                {!isOrderActive && <View style={{ position: 'absolute', right: 10, top: 20 }}>
                    <TouchableOpacity
                        onPress={() => {
                            setDestValue('');
                            dispatch(clearSearchInput());
                            dispatch(destinationCleared("CLEARED"));
                        }}
                    >
                        <MaterialCommunityIcons name="close" color="#566246" size={25} />
                    </TouchableOpacity>
                </View>}
            </View>

            {(destPredictions && !isDestHide) && destPredictions.map((destPrediction) => (
                <View key={destPrediction.place_id}>
                    <TouchableOpacity
                        style={styles.predictionsElem}
                        onPress={() => {
                            setDestValue(destPrediction.description!);
                            setDestHide(true);
                            dispatch(setDestination(destPrediction));
                            Keyboard.dismiss();
                        }}
                    >
                        <Text style={styles.predictionLabel}>{destPrediction.description}</Text>
                    </TouchableOpacity>
                </View>
            ))}

            {destinationPredictionStatus !== "OK" && destinationPredictionStatus !== "" &&
                <View>
                    <TouchableOpacity
                        style={styles.predictionsElem}
                    >
                        <Text style={styles.predictionLabel}>未找到結果</Text>
                    </TouchableOpacity>
                </View>
            }

            {!isOrderActive && <View style={styles.buttonContainer}>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => {
                        setOriginValue('');
                        setDestValue('');
                        dispatch(clearSearchInput());
                        dispatch(originCleared("CLEARED"));
                        dispatch(destinationCleared("CLEARED"));
                    }}
                >
                    <MaterialCommunityIcons name="close" color="#FFFFFF" size={20} />
                    <Text style={styles.buttonText}>取消</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.button}
                    disabled={selectedOrigin == undefined || selectedDestination == undefined || isOrderActive}
                    onPress={
                        () => {
                            navigation.navigate('OrderConfig');
                        }
                    }
                >
                    <MaterialCommunityIcons name="arrow-right" color="#FFFFFF" size={20} />
                    <Text style={styles.buttonText}>下一步</Text>
                </TouchableOpacity>
            </View>}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        margin: 10
    },
    label: {
        color: 'black',
    },
    input: {
        flex: 1,
        backgroundColor: 'rgba(240,240,240,0.7)',
        height: 44,
        fontSize: 16,
        marginTop: 10,
        paddingVertical: 5,
        paddingLeft: 10,
        paddingRight: 30,
        borderWidth: 1,
        borderColor: '#566246',
        borderRadius: 5,
    },
    button: {
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: "#566246",
        paddingHorizontal: 10,
        paddingVertical: 6,
        marginTop: 5,
        marginHorizontal: 5,
        borderRadius: 100,
    },
    buttonText: {
        textAlign: 'center',
        color: '#f1f2eb',
        fontSize: 14
    },
    predictionsElem: {
        backgroundColor: "rgba(241,242,235, 0.7)",
        padding: 10,
        fontSize: 16,
    },
    predictionLabel: {
        fontSize: 16
    },
    buttonContainer: {
        flexDirection: "row",
        justifyContent: 'flex-end'
    }
});