import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import { NavigationProp, useNavigation, useIsFocused } from '@react-navigation/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useSelector, useDispatch } from "react-redux";
import { RootState } from '../../../store';
import { PoolJoinedProp, PoolStackScreenParamList } from './PoolStackScreen';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { SafeAreaView } from "react-native-safe-area-context";
import { fetchPoolList } from "../../../redux/order/thunk";

export default function PoolJoined({ route }: PoolJoinedProp) {
    const navigation: NavigationProp<PoolStackScreenParamList> = useNavigation();
    const userId = useSelector((state: RootState) => state.auth.user?.id);
    // const [orders, setOrders] = useState<PoolOrder[]>([])
    const isFocused = useIsFocused()
    const [isDataLoaded, setIsDataLoaded] = useState(0)
    const dispatch = useDispatch();

    const orders = route.params!.orders;
    const activePoolOrder = useSelector((state: RootState) => state.order.activePoolOrder);
    const firstActiveOrderPoolDeadline = useSelector((state: RootState) => state.order.activePoolOrder[0]?.pool_deadline);

    const currentTime = new Date();
    const [seconds, setSeconds] = useState(0);
    const [timerMin, setTimerMin] = useState(0)
    const [timerSec, setTimerSec] = useState(0)

    useEffect(() => {
        if (firstActiveOrderPoolDeadline === undefined) {
            // console.log("No pool order")
        } else {
            const poolDeadline = new Date(activePoolOrder[0].pool_deadline);

            let secondsDiff = ((poolDeadline.getTime() - currentTime.getTime()) / 1000);

            // console.log("currentTime", currentTime.getTime())
            // console.log("poolDeadline", poolDeadline.getTime())
            // console.log("secondsDiff", secondsDiff)

            setSeconds(Math.round(secondsDiff))
        }
    }, [firstActiveOrderPoolDeadline]);

    useEffect(() => {
        setTimerSec(seconds % 60)
        setTimerMin(Math.floor(seconds / 60))
    }, [seconds]);

    useEffect(() => {
        const interval = setInterval(() => {
            setSeconds(seconds => seconds - 1);
        }, 1000);
        () => clearInterval(interval);
        console.log("no dependance");

        return clearInterval(interval);
    }, [])
    // console.log('joined: ', orders);
    useEffect(() => {
        if (userId != undefined) {
            dispatch(fetchPoolList);
        }
    }, [isFocused])
    // useEffect(() => {
    //     if (userId != undefined) {
    //         loadData(userId);
    //     }
    // }, [isFocused])
    // useEffect(() => {
    //     if (orders.length == 0 && isDataLoaded === 1) {
    //         navigation.dispatch(StackActions.replace('Event'));

    //     }

    // }, [orders, isDataLoaded])
    // async function loadData(userId: number) {
    //     try {

    //         let res = await fetch(`${REACT_APP_BACKEND_URL}/current-pool/${userId}`)

    //         const result = await res.json()
    //         // console.log(result);

    //         await setOrders(result)
    //         console.log(result);

    //         // order = orders[0]
    //         setIsDataLoaded(state => state + 1);

    //     } catch (error) {
    //         console.log(error);
    //     }

    // }

    return (
        <SafeAreaView edges={['top', 'left', 'right']} style={styles.pageContainer}>
            {/* {isLoading == 1 && <SplashScreen />}
    {isLoading === -1 && !(activeOrder.length >= 1) && <NoResult />}
    {isLoading === -1 && activeOrder.length >= 1 &&  */}

            {orders.length > 0 &&
                <ScrollView style={styles.container}>

                    {activePoolOrder.map((order: any, index: any) => (
                        <View style={styles.innerContainer} key={index}>
                            <View style={styles.section}>
                                <View style={styles.sectionTitle}>
                                    <Text style={styles.sectionText}>乘客{index + 1}</Text>
                                    <Text style={styles.sectionText}>{order.username}</Text>
                                </View>
                                <View>
                                    <Text style={styles.headcountText}>人數: {order.headcount}</Text>
                                </View>
                            </View>
                        </View>
                    ))}


                </ScrollView >
            }


        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    pageContainer: {
        justifyContent: 'center',
        flex: 1,

    },
    timer: {
        backgroundColor: '#d8dad3',
        marginVertical: 10,
        borderRadius: 10,
        padding: 10,
    },
    timerText: {
        backgroundColor: '#f1f2eb',
        color: '#4a4a48',
        paddingVertical: 5,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 25,
    },
    innerContainer: {
        margin: 15
    },
    locationLabelContainer: {
        backgroundColor: '#a4c2a5',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#566246',
        color: '#4a4a48',
        paddingVertical: 10,
        width: 80,
    },
    locationLabel: {
        fontWeight: 'bold',
        textAlign: 'center'
    },
    location: {
        paddingVertical: 15,
        color: '#4a4a48',
        fontSize: 28,
        flexWrap: 'wrap',
        fontWeight: 'bold',
    },
    locationContainer: {
        marginHorizontal: 10,
        // backgroundColor: '#a4c2a5',
        // flexDirection: 'column'
    },
    categoryContainer: {
        marginBottom: 10
    },
    categoryLabel: {
        fontWeight: "bold",
        marginHorizontal: 10,
        paddingVertical: 10,
        color: '#4a4a48',
        fontSize: 20
    },
    categoryElem: {
        flexDirection: "row",
        // marginVertical: 5,
        flexWrap: "wrap"
    },
    divider: {
        borderBottomColor: '#566246',
        borderBottomWidth: 1,
        opacity: 0.7,
        margin: 10
    },
    section: {
        backgroundColor: '#a4c2a5',
        marginVertical: 10,
        borderRadius: 10,
        padding: 10,
    },
    sectionTitle: {
        backgroundColor: '#f1f2eb',
        borderRadius: 10,
        color: '#4a4a48',
        paddingVertical: 5,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 25,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    sectionText: {
        fontWeight: 'bold',
        fontSize: 20,
    },
    headcountText: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
        margin: 5

    },
    iconSelected: {
        opacity: 1,
        marginBottom: 10,
        marginHorizontal: 20
    },
    iconDeselected: {
        opacity: 0.2,
        marginBottom: 15,
    },
    icon: {
        marginHorizontal: 10,
        fontSize: 50,
        color: '#4a4a48'
    },
    iconLabel: {
        textAlign: "center",
        fontWeight: "600",
        color: "black",
        fontSize: 17,
    },
    button: {
        alignItems: "center",
        backgroundColor: "#566246",
        padding: 10,
        margin: 5,
        borderRadius: 10,
    },
    buttonText: {
        textAlign: 'center',
        color: '#f1f2eb',
        fontWeight: 'bold',
    },
    inputContainer: {
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        flexDirection: 'row',
        flexWrap: "wrap",
    },
    input: {
        backgroundColor: 'white',
        marginVertical: 5,
        height: 40,
        width: '75%',
        borderRadius: 5,
        borderColor: '#4a4a48',
        borderWidth: 1,
        flex: 0.4,
        textAlign: 'center',

    },
    // buttons: {
    //     backgroundColor: '#566246',
    //     margin: 5,
    //     padding: '3%',
    //     borderRadius: 10,
    //     width: 250,
    // },
    text: {
        marginHorizontal: 20
    },
    //modal
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    alertModalView: {
        // paddingVertical: 100,
        height: 200,
        justifyContent: 'center',
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        paddingHorizontal: 35,
        paddingVertical: 15,

        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    // modalButton: {
    //     backgroundColor: '#a4c2a5'
    // },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});