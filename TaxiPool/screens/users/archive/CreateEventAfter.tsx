import * as React from 'react';
import { useState } from 'react';
import { Text, View, StyleSheet, ScrollView, Switch, TextInput } from 'react-native';


export default function CreateEventAfter() {
    const [text, setText] = React.useState('');
    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);
    return (
        <ScrollView style={styles.container}>
            <View style={styles.inputContainer}>
                <TextInput
                    style={styles.input}
                    onChangeText={(text) => setText(text)}
                    placeholder="起點"
                />
                <View style={styles.buttons0}>
                    <Text style={styles.buttonText}>Detail</Text>
                </View>
                <TextInput
                    style={styles.input}
                    onChangeText={(text) => setText(text)}
                    placeholder="目的地"
                />
                <View style={styles.buttons0}>
                    <Text style={styles.buttonText}>Cancal</Text>
                </View>
            </View>

            <View style={styles.mapContainer}>
                <View style={styles.buttons0}>
                    <Text style={styles.buttonText}>Map</Text>
                </View>
            </View>


        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignContent: 'flex-start',
        backgroundColor: '#a4c2a5',
        padding: 10,
    },

    inputContainer: {
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        flexDirection: 'row',
        flexWrap: "wrap",
    },
    mapContainer: {
        height: 500,
        marginTop: 5,
        marginBottom: 5,
        // flexDirection: 'row',
        // flexWrap: "wrap",
        justifyContent: 'center',
        alignItems: 'center',
        // alignContent: 'space-between',
        borderRadius: 20,
        // borderWidth: 2,
        // backgroundColor: 'red',
    },

    input: {
        backgroundColor: 'white',
        marginVertical: 5,
        height: 40,
        width: '75%',
        borderRadius: 5,
    },

    buttons: {
        backgroundColor: '#566246',
        margin: 5,
        padding: '3%',
        borderRadius: 10,
        width: 250,
    },
    buttons0: {
        backgroundColor: 'green',
        margin: 5,
        padding: '3%',
        borderRadius: 10,
        width: 80,
    },
    buttons1: {
        backgroundColor: 'purple',
        margin: 5,
        padding: '3%',
        borderRadius: 10,
        width: 250,
    },

    text: {
        textAlign: 'center',
    },

    baseText: {
        flexDirection: 'column',
        textAlign: 'center',
        color: 'black',
        fontWeight: 'bold',
    },

    buttonText: {
        textAlign: 'center',
        color: '#f1f2eb',
        fontWeight: 'bold',
    },
    scrollView: {
        backgroundColor: 'yellow',
        borderRadius: 20,
    },
});
