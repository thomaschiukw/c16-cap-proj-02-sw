import React, { useState, useEffect } from 'react';

import { SafeAreaView, View, Text, StyleSheet, Image, PermissionsAndroid, Platform, Button, } from 'react-native';

import Geolocation from '@react-native-community/geolocation';

import { MapPath } from './MapPath'

const watchID:(number | null) = 0;

const GetCurrentGPS = () => {
    const [myLongitude, setCurrentLongitude] = useState("...");
    const [myLatitude, setCurrentLatitude] = useState("...");
    const [locationStatus, setLocationStatus] = useState('');

    
    const getOneTimeLocation = () => {
        setLocationStatus('Getting Location ...');
        Geolocation.getCurrentPosition(
            //Will give you the current location
            (position) => {
                setLocationStatus('You are Here');

                //getting the Longitude from the location json
                const myLongitude = JSON.stringify(position.coords.longitude);

                //getting the Latitude from the location json
                const myLatitude = JSON.stringify(position.coords.latitude);

                //Setting Longitude state
                setCurrentLongitude(myLongitude);

                //Setting Latitude state
                setCurrentLatitude(myLatitude);
            },
            (error) => {
                setLocationStatus(error.message);
            },
            {
                enableHighAccuracy: false,
                timeout: 100000,
                maximumAge: 1000
            },
        );
    };

    const subscribeLocationLocation = () => {
        Geolocation.watchPosition(
            (position) => {
                //Will give you the location on location change
                setLocationStatus('You are Here');

                //getting the Longitude from the location json        
                const myLongitude = JSON.stringify(position.coords.longitude);
                //getting the Latitude from the location json
                const myLatitude = JSON.stringify(position.coords.latitude);

                //Setting Longitude state
                setCurrentLongitude(myLongitude);

                //Setting Latitude state
                setCurrentLatitude(myLatitude);
            },
            (error) => {
                setLocationStatus(error.message);
            },
            {
                enableHighAccuracy: false,
                maximumAge: 1000
            },
        );
    };


    useEffect(() => {

        const requestLocationPermission = async () => {
            if (Platform.OS === 'ios') {
                getOneTimeLocation();
                subscribeLocationLocation();
            } else {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                        {
                            title: 'Location Access Required',
                            message: 'This App needs to Access your location',
                            buttonPositive: "OK",
                        },
                    );
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        //To Check, If Permission is granted
                        getOneTimeLocation();
                        subscribeLocationLocation();
                    } else {
                        setLocationStatus('Permission Denied');
                    }
                } catch (err) {
                    console.warn(err);
                }
            }
        };
        requestLocationPermission();
        return () => {
            // Geolocation.clearWatch(watchID);
        };
    }, [myLongitude,myLatitude]);

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <Text>Longitude: {myLongitude}</Text>
            <Text>Latitude: {myLatitude}</Text>
            <View style={styles.container}>
                <View style={styles.container}>
                    <MapPath myLongitude={myLongitude} myLatitude={myLatitude} />

                    {/* <Image
                        source={{
                            uri:
                                'https://raw.githubusercontent.com/AboutReact/sampleresource/master/location.png',
                        }}
                        style={{ width: 100, height: 100 }}
                    />
                    <Text style={styles.boldText}>
                        {locationStatus}
                    </Text>
                    <Text
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 16,
                        }}>
                        Longitude: {myLongitude}
                    </Text>
                    <Text
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 16,
                        }}>
                        Latitude: {myLatitude}
                    </Text> */}
                    {/* <View style={{ marginTop: 20 }}>
                        <Button
                            title="Button"
                            onPress={getOneTimeLocation}
                        />
                    </View> */}
                </View>
                {/* <Text
                    style={{
                        fontSize: 18,
                        textAlign: 'center',
                        color: 'grey'
                    }}>
                    React Native Geolocation
                </Text> */}
            </View>
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    boldText: {
        fontSize: 25,
        color: 'red',
        marginVertical: 16,
    },
});

export default GetCurrentGPS;