import { StyleSheet } from 'react-native';

const styles = StyleSheet.create(
    {
        container: {
            marginTop: 50,
            height: 300,
        },
        mapView: {
            // position: "absolute",
            width: 400,
            height: 300,
            // top:0,
            // left:0,
        },
    });
export default styles;