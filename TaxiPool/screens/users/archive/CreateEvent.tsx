import React, { useState } from "react";
import { ScrollView, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View , TextInput} from "react-native";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { SafeAreaView } from "react-native-safe-area-context";

export default function CreateEvent() {
    const [isRedTaxi, setIsRedTaxi] = useState(true);
    const [isGreenTaxi, setIsGreenTaxi] = useState(false);
    const [isBlueTaxi, setIsBlueTaxi] = useState(false);
    const [isAllTunnel, setIsAllTunnel] = useState(false);
    const [isCentralTunnel, setIsCentralTunnel] = useState(true);
    const [isEastTunnel, setIsEastTunnel] = useState(true);
    const [isWestTunnel, setIsWestTunnel] = useState(false);
    const [isAllPrice, setIsAllPrice] = useState(false);
    const [isNinePrice, setIsNinePrice] = useState(true);
    const [isEightFivePrice, setIsEightFivePrice] = useState(false);
    const [isEightPrice, setIsEightPrice] = useState(false);
    const [hasPet, setHasPet] = useState(false);
    const [isSmokeFree, setIsSmokeFree] = useState(true);
    const [hasLuggage, setHasLuggage] = useState(false);
    const [isDisabled, setIsDisabled] = useState(false);
    const [isUrgent, setIsUrgent] = useState(false);
    const [text, setText] = React.useState('');

    function selectAllTunnel() {
        if (isAllTunnel) {
            setIsAllTunnel(false);
            setIsCentralTunnel(false);
            setIsEastTunnel(false);
            setIsWestTunnel(false);
        } else {
            setIsAllTunnel(true);
            setIsCentralTunnel(true);
            setIsEastTunnel(true);
            setIsWestTunnel(true);
        }
    }

    function selectCentralTunnel() {
        setIsCentralTunnel(!isCentralTunnel)
        if (isAllTunnel) {
            setIsAllTunnel(false);
        }
    }

    function selectEastTunnel() {
        setIsEastTunnel(!isEastTunnel)
        if (isAllTunnel) {
            setIsAllTunnel(false);
        }
    }

    function selectWestTunnel() {
        setIsWestTunnel(!isWestTunnel)
        if (isAllTunnel) {
            setIsAllTunnel(false);
        }
    }

    const eventSettingsFormState = [
        { isRedTaxi },
        { isGreenTaxi },
        { isBlueTaxi },
        { isAllTunnel },
        { isCentralTunnel },
        { isEastTunnel },
        { isWestTunnel },
        { isAllPrice },
        { isNinePrice },
        { isEightFivePrice },
        { isEightPrice },
        { hasPet },
        { isSmokeFree },
        { hasLuggage },
        { isDisabled },
        { isUrgent }
    ]

    return (
        <SafeAreaView edges={['top', 'left', 'right']}  style={styles.container}>
            <View style={styles.container}>
               

                <View style={styles.inputContainer}>
                <TextInput
                    style={styles.input}
                    onChangeText={(text) => setText(text)}
                    placeholder="起點"
                />
                <View style={styles.buttons0}>
                    <Text style={styles.buttonText}>共乘</Text>
                </View>
                <TextInput
                    style={styles.input}
                    onChangeText={(text) => setText(text)}
                    placeholder="目的地"
                />
                <View style={styles.buttons0}>
                    <Text style={styles.buttonText}>建立</Text>
                </View>
                   </View>

                   <ScrollView style={styles.innerContainer}>
                   <View style={styles.categoryElem}>
                        <Text style={styles.categoryLabel}>種類</Text>
                        <View style={styles.categoryContainer}>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => setIsRedTaxi(!isRedTaxi)}
                            >
                                <View style={isRedTaxi ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialCommunityIcons name="taxi" color="red" style={styles.icon} />
                                    <Text style={styles.iconLabel}>紅的</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => setIsGreenTaxi(!isGreenTaxi)}
                            >
                                <View style={isGreenTaxi ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialCommunityIcons name="taxi" color="green" style={styles.icon} />
                                    <Text style={styles.iconLabel}>綠的</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => setIsBlueTaxi(!isBlueTaxi)}
                            >
                                <View style={isBlueTaxi ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialCommunityIcons name="taxi" color="skyblue" style={styles.icon} />
                                    <Text style={styles.iconLabel}>藍的</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>

                    <View style={styles.categoryElem}>
                        <Text style={styles.categoryLabel}>海底隧道</Text>
                        <View style={styles.categoryContainer}>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => selectAllTunnel()}
                            >
                                <View style={isAllTunnel ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>任何</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => selectCentralTunnel()}
                            >
                                <View style={isCentralTunnel ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>紅隧</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => selectEastTunnel()}
                            >
                                <View style={isEastTunnel ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>東隧</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => selectWestTunnel()}
                            >
                                <View style={isWestTunnel ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="airport-shuttle" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>西隧</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>

                    <View style={styles.categoryElem}>
                        <Text style={styles.categoryLabel}>價錢</Text>
                        <View style={styles.categoryContainer}>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => setIsAllPrice(!isAllPrice)}
                            >
                                <View style={isAllPrice ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="local-atm" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>正價</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => setIsNinePrice(!isNinePrice)}
                            >
                                <View style={isNinePrice ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="local-atm" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>九折</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => setIsEightFivePrice(!isEightFivePrice)}
                            >
                                <View style={isEightFivePrice ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="local-atm" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>八五折</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => setIsEightPrice(!isEightPrice)}
                            >
                                <View style={isEightPrice ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="local-atm" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>八折</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>

                    <View style={styles.categoryElem}>
                        <Text style={styles.categoryLabel}>其他選擇</Text>
                        <View style={styles.categoryContainer}>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => setHasPet(!hasPet)}
                            >
                                <View style={hasPet ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="pets" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>寵物</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => setIsSmokeFree(!isSmokeFree)}
                            >
                                <View style={isSmokeFree ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="smoke-free" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>不吸煙</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => setHasLuggage(!hasLuggage)}
                            >
                                <View style={hasLuggage ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="luggage" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>行李</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => setIsDisabled(!isDisabled)}
                            >
                                <View style={isDisabled ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialIcons name="wheelchair-pickup" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>傷殘人仕</Text>
                                </View>
                            </TouchableHighlight>
                            <TouchableHighlight
                                underlayColor={''}
                                onPress={() => setIsUrgent(!isUrgent)}
                            >
                                <View style={isUrgent ? styles.iconSelected : styles.iconDeselected}>
                                    <MaterialCommunityIcons name="run-fast" color="black" style={styles.icon} />
                                    <Text style={styles.iconLabel}>緊急</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>
                   
                </ScrollView>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    innerContainer: {
        margin: 20
    },
    categoryElem: {
        marginBottom: 5
    },
    categoryLabel: {
        fontWeight: "bold",
        marginHorizontal: 10
    },
    categoryContainer: {
        flexDirection: "row",
        marginVertical: 10,
        flexWrap: "wrap"
    },
    iconSelected: {
        opacity: 1,
        marginBottom: 10,
    },
    iconDeselected: {
        opacity: 0.2,
        marginBottom: 15,
    },
    icon: {
        marginHorizontal: 10,
        fontSize: 50
    },
    iconLabel: {
        textAlign: "center",
        fontWeight: "600",
        color: "black"
    },
    button: {
        alignItems: "center",
        backgroundColor: "#566246",
        padding: 10,
        marginVertical: 5,
        borderRadius: 10,
    },
    buttonText: {
        textAlign: 'center',
        color: '#f1f2eb',
        fontWeight: 'bold',
    },
    inputContainer: {
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        flexDirection: 'row',
        flexWrap: "wrap",
    },
    input: {
        backgroundColor: 'white',
        marginVertical: 5,
        height: 40,
        width: '70%',
        borderRadius: 5,
    },
    buttons: {
        backgroundColor: '#566246',
        margin: 5,
        padding: '3%',
        borderRadius: 10,
        width: 250,
    },
    buttons0: {
        backgroundColor: 'green',
        margin: 5,
        padding: '3%',
        borderRadius: 10,
        width: 100,
    },
});
