import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { useDispatch, useSelector } from "react-redux";

import { RootState } from "../../../store";
import styles from './MapPathStyle';

// import distance from 'google-distance-matrix';
// var distance = require('google-distance-matrix');

// const pointForCoordinate() => {}

export const MapPath: object = (myLongitude: string) => {
    const origin = { longitude: parseFloat(Object.values(myLongitude)[0]) ,latitude: parseFloat(Object.values(myLongitude)[1])};

    const dispatch = useDispatch();
    const isLoading = useSelector((state: RootState) => state.loading.isLoading);

    // const destination = 'Hong Ning Rd, Kwun Tong';
    const destination = 'Google Hong Kong';
    // const originLoc = {
    //     latitude: origin.details.geometry.location.lat,
    //     longitude: origin.details.geometry.location.lng,
    // };

    // const destinationLoc = {
    //     latitude: destination.details.geometry.location.lat,
    //     longitude: destination.details.geometry.location.lng,
    // };

    const GOOGLE_MAPS_APIKEY = 'AIzaSyBBNfnxDZjnwMrB2FwbQDMc-xgIRrXsRkk';

    React.useEffect(() => {
    }, [myLongitude])

    return (
        <View style={styles.container}>
            <MapView
                style={styles.mapView}
                showsUserLocation={true}
                provider={PROVIDER_GOOGLE}
                followsUserLocation={true}
                userLocationUpdateInterval={5000}
                showsMyLocationButton={true}
                region={{
                    longitude: (Object.values(myLongitude)[0]) == "..." ? -122.4324 : parseFloat((Object.values(myLongitude)[0])),
                    latitude: (Object.values(myLongitude)[1]) == "..." ? 37.78825 : parseFloat((Object.values(myLongitude)[1])),
                    latitudeDelta: 0.003,
                    longitudeDelta: 0.001,
                }}>
                <MapViewDirections
                    origin={origin}
                    destination={destination}
                    apikey={GOOGLE_MAPS_APIKEY}
                    mode={'DRIVING'}
                    strokeWidth={4}
                    strokeColor="#BBBDBF"
                />
            </MapView>
        </View>
    )
}

export default MapPath;