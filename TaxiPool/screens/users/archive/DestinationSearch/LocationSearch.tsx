import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, SafeAreaView } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { useNavigation } from '@react-navigation/native';

import { useSelector } from "react-redux";
import { RootState } from "../../../../store";

import styles from './LocationSearchStyle';
import PlaceRow from "./PlaceRow";

const GOOGLE_PLACES_API_KEY = 'AIzaSyBBNfnxDZjnwMrB2FwbQDMc-xgIRrXsRkk';

const currentPlace = {
    description: 'Current Location',
    geometry: { location: { lat: 114.8496818, lng: 22.2940881 } },
};
const homePlace = {
    description: 'Home',
    geometry: { location: { lat: 122.8152937, lng: 24.4597668 } },
};
const workPlace = {
    description: 'Work',
    geometry: { location: { lat: 114.8496818, lng: 22.2940881 } },
};

const GooglePlacesInput = (props) => {
    const [originPlace, setOriginPlace] = useState(null);
    const [destinationPlace, setDestinationPlace] = useState(null);

    // const  originPlace = useSelector((state:RootState)=>state.originPlace);
    // const  destinationPlace = useSelector((state:RootState)=>state.destinationPlace);

    const navigation = useNavigation();

    const checkNavigation = () => {
        if (originPlace && destinationPlace) {
            // navigation.navigate('SearchResults', {
            //   originPlace,
            //   destinationPlace,
            // })
            console.log("hihihihi")
        }
    }

    useEffect(() => {
        checkNavigation();
    }, [originPlace, destinationPlace]);

    return (
        <SafeAreaView>
            {/* <Text>"hihi"</Text> */}
            <View style={styles.container}>
                <GooglePlacesAutocomplete
                    placeholder="Where from?"
                    onPress={(data, details = null) => {
                        setOriginPlace({ data, details });
                        // console.log("data",data)
                        // console.log("details location",details?.geometry.location)
                        // console.log("details location.lat",details?.geometry.location.lat)
                        // console.log("originPlace",originPlace)
                        // if (originPlace){
                        //   console.log("originPlace",originPlace.data)
                        // } else {
                        //   console.log("no originPlace.data");

                        // }

                    }}
                    enablePoweredByContainer={false}
                    suppressDefaultStyles
                    currentLocation={true}
                    currentLocationLabel='Current location'
                    styles={{
                        textInput: styles.textInput,
                        container: styles.autocompleteContainer,
                        listView: styles.listView,
                        separator: styles.separator,
                    }}
                    fetchDetails
                    query={{
                        key: 'AIzaSyBBNfnxDZjnwMrB2FwbQDMc-xgIRrXsRkk',
                        language: 'en',
                    }}
                    renderRow={(data) => <PlaceRow data={data} />}
                    // renderDescription={(data) => data.description || data.vicinity}
                    predefinedPlaces={[homePlace, workPlace]}
                />

                <GooglePlacesAutocomplete
                    placeholder="Where to?"
                    onPress={(data, details = null) => {
                        setDestinationPlace({ data, details });
                    }}
                    enablePoweredByContainer={false}
                    suppressDefaultStyles
                    styles={{
                        textInput: styles.textInput,
                        container: {
                            ...styles.autocompleteContainer,
                            top: 55,
                        },
                        separator: styles.separator,
                    }}
                    fetchDetails
                    query={{
                        key: 'AIzaSyBBNfnxDZjnwMrB2FwbQDMc-xgIRrXsRkk',
                        language: 'en',
                    }}
                    renderRow={(data) => <PlaceRow data={data} />}
                />

                {/* Circle near Origin input */}
                <View style={styles.circle} />

                {/* Line between dots */}
                <View style={styles.line} />

                {/* Square near Destination input */}
                <View style={styles.square} />
            </View>
        </SafeAreaView>
    )
}

export default GooglePlacesInput;