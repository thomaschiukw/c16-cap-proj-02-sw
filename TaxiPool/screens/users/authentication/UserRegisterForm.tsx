import React from "react";
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { useDispatch } from "react-redux";
import { useForm, Controller, SubmitHandler } from 'react-hook-form';
import { fetchRegister } from "../../../redux/register/thunk";

export type UserRegisterFormInput = {
    username: string;
    email: string;
    phone: string;
    password: string;
}

export default function RegisterForm() {
    const { register, setValue, handleSubmit, control, reset, formState: { errors } } = useForm<UserRegisterFormInput>({
        defaultValues: {
            username: '',
            email: '',
            phone: '',
            password: ''
        }
    });

    const dispatch = useDispatch();

    const onSubmit: SubmitHandler<UserRegisterFormInput> = data => {
        dispatch(fetchRegister(data));
    };

    if (errors === true) {
        console.log('errors', errors);
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>乘客登記</Text>
            {/* <Text style={styles.label}>Username:</Text> */}
            <View style={styles.input_row}>
                <View style={styles.infoContent}>
                    <Text style={styles.info}>用戶名稱</Text>
                </View>

                <View style={styles.input_errMSG_column}><Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.inputTextContent}
                            // placeholder="用戶名稱"
                            onBlur={onBlur}
                            onChangeText={value => onChange(value)}
                            value={value}
                        />
                    )}
                    name="username"
                    rules={{ required: true }}
                /><View style={{ flexDirection: 'row' }}>
                    {errors.username && <Text>"請輸入您的用戶名稱"</Text>}
                    <Text> </Text></View></View></View>



            <View style={styles.input_row}>
                <View style={styles.infoContent}>
                    <Text style={styles.info}>電郵地址</Text>
                </View>
                <View style={styles.input_errMSG_column}><Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.inputTextContent}
                            // placeholder="電郵地址"
                            textContentType='emailAddress'
                            keyboardType='email-address'
                            onBlur={onBlur}
                            onChangeText={value => onChange(value)}
                            value={value}
                        />
                    )}
                    name="email"
                    rules={{
                        required: true, pattern: {
                            value: /\S+@\S+\.\S+/,
                            message: "請輸入有效電郵地址"
                        }
                    }}
                /><View style={{ flexDirection: 'row' }}>
                    {errors.email && errors.email.type === "required" && <Text>"請輸入您的電郵地址"</Text>}
                    {errors.email && <Text>{errors.email.message}</Text>}
                    <Text> </Text></View></View></View>



            <View style={styles.input_row}>
                <View style={styles.infoContent}>
                    <Text style={styles.info}>電話號碼</Text>
                </View>

                <View style={styles.input_errMSG_column}>
                    <Controller
                        control={control}
                        render={({ field: { onChange, onBlur, value } }) => (
                            <TextInput
                                style={styles.inputTextContent}
                                // placeholder="電話號碼"
                                onBlur={onBlur}
                                keyboardType='numeric'
                                maxLength={8}
                                onChangeText={value => onChange(value.replace(/[^0-9]/g, ''))}
                                value={value}
                            />
                        )}
                        name="phone"
                        rules={{ required: true, minLength: 8 }}
                    />
                    <View style={{ flexDirection: 'row' }}>
                        {errors.phone && errors.phone.type === "required" && <Text>"請輸入您的電話號碼"</Text>}
                        {errors.phone && errors.phone.type === "minLength" && <Text>"請輸入有效電話號碼"</Text>}
                        <Text> </Text></View>
                </View>
            </View>


            <View style={styles.input_row}>
                <View style={styles.infoContent}>
                    <Text style={styles.info}>密碼</Text>
                </View>
                <View style={styles.input_errMSG_column}><Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.inputTextContent}
                            // placeholder="密碼"
                            onBlur={onBlur}
                            onChangeText={value => onChange(value)}
                            value={value}
                            secureTextEntry={true}
                        />
                    )}
                    name="password"
                    rules={{ required: true }}
                /><View style={{ flexDirection: 'row' }}>{errors.password && <Text>"請輸入您的密碼"</Text>}<Text> </Text></View>
            </View></View>

            <View>
                <TouchableOpacity
                    style={styles.button}
                    onPress={handleSubmit(onSubmit)}
                >
                    <Text style={styles.buttonText}>登記</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => {
                        reset({
                            username: 'test',
                            email: 'test@gmail.com',
                            phone: '87654321',
                            password: 'test'
                        })
                    }}
                >
                    <Text style={styles.buttonText}>預設登記</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => {
                        reset({
                            username: '',
                            email: '',
                            phone: '',
                            password: ''
                        })
                    }}
                >
                    <Text style={styles.buttonText}>重置</Text>
                </TouchableOpacity>
            </View>
        </View >
    )
}

const styles = StyleSheet.create({
    container:{
        width: "90%",
        alignSelf: "center",
    },
    title: {
        fontSize: 30,
        color: '#4a4a48',
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 20
    },
    label: {
        color: 'black',
    },
    input_errMSG_column: {
        flexDirection: 'column'
    },
    input_row: {
        flexDirection: 'row'
    },
    input: {
        height: 40,
        // marginVertical: 10,
        borderWidth: 2,
        borderColor: '#566246',
        paddingHorizontal: 10,
        borderRadius: 15,
    },
    info: {
        color: "#4a4a48",
    },
    infoContent: {
        flex: 2,
        justifyContent: 'center',
    },
    inputTextContent: {
        backgroundColor: '#f1f2eb',
        borderRadius: 5,
        borderColor: "#566246",
        borderWidth: 1,
        justifyContent: 'center',
        textAlign: 'center',
        height: 40,
        width: 200,
    },
    button: {
        alignItems: "center",
        backgroundColor: "#566246",
        padding: 10,
        marginVertical: 5,
        borderRadius: 10,
    },
    buttonText: {
        textAlign: 'center',
        color: '#f1f2eb',
        fontWeight: 'bold',
    }
});