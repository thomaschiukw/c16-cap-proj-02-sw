import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavigationProp, StackActions, useNavigation } from "@react-navigation/native";
import { RESULTS } from "react-native-permissions";
import { RootState } from "../../../store";
import { autoLogin } from "../../../redux/authentication/thunk";
import { LoginStackScreenParamList } from "../../../components/LoginStackScreen";
import { permissionStatusCheck } from "../../../redux/permission/thunk";
import SplashScreen from "../../../components/SplashScreen";

export default function LoginLoadingScreen() {
    const dispatch = useDispatch();
    const navigation: NavigationProp<LoginStackScreenParamList> = useNavigation();
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
    const isDriver = useSelector((state: RootState) => state.auth.user?.isDriver);
    const isLoading = useSelector((state: RootState) => state.loading.isLoading);
    const isLocationGranted = useSelector((state: RootState) => state.permission.permissionStatus);

    useEffect(() => {
        dispatch(autoLogin());
        dispatch(permissionStatusCheck());
    }, []);

    useEffect(() => {
        if (isLoading === -1) {
            if (isLocationGranted === RESULTS.GRANTED) {
                if (isAuthenticated && !isDriver) {
                    navigation.dispatch(StackActions.replace('UserHome'));
                } else if (isAuthenticated && isDriver) {
                    navigation.dispatch(StackActions.replace('DriverHome'));
                } else {
                    navigation.dispatch(StackActions.replace('Login'));
                }
            } else {
                navigation.dispatch(StackActions.replace('Permission'));
            }
        }
    }, [isLoading]);

    return (
        <SplashScreen />
    );
}