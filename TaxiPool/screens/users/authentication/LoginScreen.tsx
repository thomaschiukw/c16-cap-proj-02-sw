import * as React from 'react';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image } from 'react-native';
import { NavigationProp, StackActions, useNavigation } from '@react-navigation/native';
import { LoginStackScreenParamList } from '../../../components/LoginStackScreen';
import { RootState } from '../../../store';
import SplashScreen from '../../../components/SplashScreen';
import LoginForm from './LoginForm';

export default function LoginScreen() {
    const navigation: NavigationProp<LoginStackScreenParamList> = useNavigation();
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
    const isDriver = useSelector((state: RootState) => state.auth.user?.isDriver);
    const isLoading = useSelector((state: RootState) => state.loading.isLoading);

    useEffect(() => {
        if (isLoading === -1) {
            if (isAuthenticated && !isDriver) {
                navigation.dispatch(StackActions.replace('UserHome'));
            }
            if (isAuthenticated && isDriver) {
                navigation.dispatch(StackActions.replace('DriverHome'));
            }
        }
    }, [isLoading]);

    return (
        <ScrollView
            style={styles.container}
            keyboardDismissMode='interactive'
        >
            {isLoading === 1 && <SplashScreen />}
            <View style={styles.logoContainer}>
                <Text style={styles.logo}>Taxi<Text style={styles.logoPool}>Pool</Text></Text>
            </View>
            <View style={styles.signinContainer}>

                <LoginForm />
                <Text style={styles.text}>- 或 -</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    <TouchableOpacity
                        style={[styles.button, styles.registerType]}
                        onPress={() => {
                            navigation.navigate('Register', { isDriver: false });
                        }}
                    >
                        <Text style={styles.buttonText}>乘客登記</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.button, styles.registerType]}
                        onPress={() => {
                            navigation.navigate('Register', { isDriver: true });
                        }}
                    >
                        <Text style={styles.buttonText}>司機登記</Text>
                    </TouchableOpacity>
                </View>

                <Image style={styles.loginPageLogo} source={require('../../src/taxi_loginPage.png')} />
            </View>
        </ScrollView>
    );

};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1f2eb',
        padding: 10,
    },
    logoContainer: {
        flex: 1,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#f1f2eb',
        paddingVertical: 20,
        borderRadius: 20,
        marginHorizontal: 10,
        justifyContent: 'center'
    },
    logo: {
        color: '#a4c2a5',
        fontSize: 60,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    logoPool: {
        color: '#4a4a48'
    },
    signinContainer: {
        marginHorizontal: 15,
        marginVertical: 10,
        justifyContent: 'space-evenly',
    },
    // buttons: {
    //     alignItems: "center",
    //     backgroundColor: "#566246",
    //     padding: 10,
    //     marginVertical: 5,
    //     borderRadius: 10,
    // },
    buttonText: {
        textAlign: 'center',
        color: '#f1f2eb',
        fontWeight: 'bold',
    },
    text: {
        textAlign: 'center',
        marginVertical: 5
    },
    iconContainer: {
        justifyContent: "space-around",
        flexDirection: "row",
        flexWrap: "wrap",
        marginVertical: 20
    },
    signinIcon: {
        backgroundColor: '#4a4a48',
        height: 55,
        width: 55
    },
    spinnerTextStyle: {
        color: 'white',
        fontSize: 20,
    },
    button: {
        alignItems: "center",
        backgroundColor: "#566246",
        padding: 15,
        marginVertical: 5,
        borderRadius: 10,
        width: '40%',
    },
    registerType: {
        paddingHorizontal: 25,
        marginBottom: 15
    },
    loginPageLogo: {
        width: 200,
        height: 200,
        resizeMode: 'contain',
        alignSelf: "center",
    }
});