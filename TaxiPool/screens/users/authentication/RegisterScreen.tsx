import * as React from 'react';
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Text, View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { LoginStackScreenParamList, RegisterScreenProp } from '../../../components/LoginStackScreen';
import { RootState } from '../../../store';
import UserRegisterForm from './UserRegisterForm';
import DriverRegisterForm from './DriverRegisterForm';
import SplashScreen from '../../../components/SplashScreen';

export default function LoginScreen({ route }: RegisterScreenProp) {

    const isDriver = route.params.isDriver
    const dispatch = useDispatch();
    const navigation: NavigationProp<LoginStackScreenParamList> = useNavigation();
    const isLoading = useSelector((state: RootState) => state.loading.isLoading);
    const isRegistered = useSelector((state: RootState) => state.register.isRegistered);
    const isRegistrationError = useSelector((state: RootState) => state.register.error);
    // console.log('Register status:', isRegistrationError);

    useEffect(() => {
        if (isRegistered) {
            navigation.goBack();
        }
    }, [isRegistered]);

    return (
        <ScrollView
            style={styles.container}
        >
            {isLoading === 1 && <SplashScreen />}
            <View style={styles.logoContainer}>
                <Text style={styles.logo}>Taxi<Text style={styles.logoPool}>Pool</Text></Text>
            </View>

            {/* <View>{isRegistrationError && <Text>{isRegistrationError}</Text>}</View> */}

            <View style={styles.signinContainer}>
                {isDriver ? <DriverRegisterForm /> : <UserRegisterForm />}
            </View>

            <TouchableOpacity
                style={styles.button}
                onPress={() => { navigation.navigate("Login") }}>
                <Text style={styles.buttonText}>返回</Text>
            </TouchableOpacity>


        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1f2eb',
        width: "100%",
        alignSelf: "center",
    },
    logoContainer: {
        flex: 1,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#f1f2eb',
        paddingVertical: 20,
        borderRadius: 20,
        marginHorizontal: 10,
        justifyContent: 'center'
    },
    logo: {
        color: '#a4c2a5',
        fontSize: 60,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    logoPool: {
        color: '#4a4a48'
    },
    signinContainer: {
        marginHorizontal: 15,
        marginVertical: 10,
        justifyContent: 'space-evenly',
    },
    buttons: {
        alignItems: "center",
        backgroundColor: "#566246",
        padding: 10,
        marginVertical: 5,
        borderRadius: 10,
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        // color: '#f1f2eb',
        // fontWeight: 'bold',
    },
    text: {
        textAlign: 'center',
        marginVertical: 5
    },
    iconContainer: {
        justifyContent: "space-around",
        flexDirection: "row",
        flexWrap: "wrap",
        marginVertical: 10
    },
    signinIcon: {
        backgroundColor: '#4a4a48',
        height: 55,
        width: 55
    },
    spinnerTextStyle: {
        color: 'white',
        fontSize: 20,
    },
    button: {
        backgroundColor: '#566246',
        borderRadius: 10,
        marginTop: 20,
        paddingVertical: 10,
        width: "85%",
        alignSelf: "center"
    },
});