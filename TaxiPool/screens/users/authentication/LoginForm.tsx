import React, { useState } from "react";
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { useForm, Controller, SubmitHandler } from 'react-hook-form';
import { useDispatch } from "react-redux";
import { login } from "../../../redux/authentication/thunk";

export type LoginFormInput = {
    username: string;
    password: string;
}

export default function LoginForm() {
    const [username, setUsername] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    
    const { register, setValue, handleSubmit, control, reset, formState: { errors } } = useForm<LoginFormInput>({
        defaultValues: {
            username: '',
            password: ''
        }
    })

    const dispatch = useDispatch();

    const onSubmit: SubmitHandler<LoginFormInput> = data => {
        dispatch(login(data));
    }

    if (errors === true) {
        console.log('errors', errors);
    }

    return (
        <View style={styles.container}>
        {/* <Text style={styles.label}>Username:</Text> */}

        <View style={styles.input_row}>
            <View style={styles.infoContent}>
                <Text style={styles.info}>用戶名稱</Text>
            </View>

            <View>
                <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.inputTextContent}
                            // placeholder="請輸入您的用戶名稱"
                            onBlur={onBlur}
                            onChangeText={(value) => {
                                onChange(value)
                                setUsername(value)
                            }}
                            value={value}
                        />
                    )}
                    name="username"
                    rules={{ required: true }}
                />
                <View style={styles.input_row}>{errors.username && <Text style={styles.warning}>請輸入您的用戶名稱</Text>}<Text></Text></View>
            </View>
        </View>


        {/* <Text style={styles.label}>Password:</Text> */}

        <View style={styles.input_row}>
            <View style={styles.infoContent}>
                <Text style={styles.info}>密碼</Text>
            </View>

            {/* <View style={styles.inputTextBox}> */}

            <View>
                <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value } }) => (
                        <TextInput
                            style={styles.inputTextContent}
                            // placeholder="請輸入您的密碼"
                            onBlur={onBlur}
                            onChangeText={value => {
                                onChange(value)
                                setPassword(value)
                            }}
                            value={value}
                            secureTextEntry={true}
                        />
                    )}
                    name="password"
                    rules={{ required: true }}
                />
                <View style={styles.input_row}>{errors.password && <Text style={styles.warning}>請輸入您的密碼</Text>}<Text> </Text></View>
            </View>
            {/* </View> */}
        </View>

            {/* <View style={styles.input_row}>{(username.length !== 0 && password.length !== 0)&& <Text style={styles.warning}>用戶名稱或密碼錯誤</Text>}<Text> </Text></View> */}

            <View style={styles.buttonList}>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => {
                            handleSubmit(onSubmit)();
                        }
                    }>

                    <Text style={styles.buttonText}>登入</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => {
                        reset({
                            username: 'admin',
                            password: 'admin'
                        })
                    }}
                >
                    <Text style={styles.buttonText}>用戶登入</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => {
                        reset({
                            username: 'realdriver',
                            password: 'driver'
                        })
                    }}
                >
                    <Text style={styles.buttonText}>司機登入</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => {
                        reset({
                            username: '',
                            password: ''
                        })
                    }}
                >
                    <Text style={styles.buttonText}>重設</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        width: "90%",
        alignSelf: "center",
    },
    label: {
        color: 'black',
    },
    input_row: {
        flexDirection: 'row',
    },
    input: {
        height: 0,
        borderWidth: 2,
        borderColor: '#566246',
        borderRadius: 15,
    },
    warning:{
        color: "red",
    },
    info: {
        color: "#4a4a48",
    },
    infoContent: {
        flex: 2,
        justifyContent: 'center',
    },
    inputTextBox: {
        flex: 5,
        alignItems: 'center',
    },
    inputTextContent: {
        backgroundColor: '#f1f2eb',
        borderRadius: 5,
        borderColor: "#566246",
        borderWidth: 1,
        justifyContent: 'center',
        textAlign: 'center',
        height: 40,
        width: 200,
    },
    buttonList: {
        alignItems: "center",
        marginTop: 10,
    },
    button: {
        backgroundColor: "#566246",
        paddingVertical: 10,
        width: "100%",
        marginVertical: 5,
        borderRadius: 10,
    },
    buttonText: {
        textAlign: 'center',
        color: '#f1f2eb',
        fontWeight: 'bold',
    }
});
