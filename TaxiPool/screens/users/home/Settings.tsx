// import React, { Component, useEffect, useState } from 'react';
// import { StyleSheet, Text, View, TouchableHighlight, Switch, TextInput, TouchableOpacity, Modal, Pressable, Alert } from 'react-native';
// import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// import { useDispatch, useSelector } from "react-redux";
// import { useForm, SubmitHandler } from 'react-hook-form';
// import { ChangeDriverInfo, fetchDriverInfo } from "../../../redux/driver/thunk";
// import { RootState } from '../../../store';
// import SplashScreen from '../../../components/SplashScreen';
// import { NavigationProp, TabActions, useNavigation } from '@react-navigation/native';
// import { UserHomeTabParamList } from '../../users/components/UserHomeTab';

// export type DriverFormInput = {
//     name: string,
//     email: string,
//     phone: string,
// }

// export default function DriverSettingChange() {
//     const dispatch = useDispatch();
//     const isLoading = useSelector((state: RootState) => state.loading.isLoading);
//     const userID = useSelector((state: RootState) => state.auth.user?.id);
//     const userInfo = useSelector((state: RootState) => state.driver);

//     useEffect(() => {
//         if (userID) {
//             dispatch(fetchDriverInfo(userID));
//         }
//     }, [dispatch, userInfo])

//     const [name, setName] = useState<string>(userInfo.name);
//     const [email, setEmail] = useState<string>(userInfo.email);
//     const [phone, setPhone] = useState<string>(userInfo.phone);
//     const [isOrderLoading, setIsOrderLoading] = useState(0);
//     const [confirmModalVisible, setConfirmModalVisible] = useState(false);
//     const [successModalVisible, setSuccessModalVisible] = useState(false);
//     const [failModalVisible, setFailModalVisible] = useState(false);


//     const { register, setValue, handleSubmit, control, reset, formState: { errors } } = useForm<DriverFormInput>({
//         defaultValues: {
//             name: name,
//             email: email,
//             phone: phone,
//         }
//     })

//     const onSubmit: SubmitHandler<DriverFormInput> = data => dispatch(ChangeDriverInfo(data));


//     const navigation: NavigationProp<UserHomeTabParamList> = useNavigation();
//     const goToSetting = () => {
//         navigation.goBack()
//     }

//     return (

//         <View>
//             {/* <Text>{userID}</Text> */}
//             <View style={styles.container}>
//                 <View style={styles.body}>
//                     <View style={styles.item}>
//                         <View style={styles.iconContent}>
//                             <MaterialCommunityIcons name="account" style={styles.icon} />
//                         </View>

//                         <View style={styles.infoContent}>
//                             <Text style={styles.info}>姓名</Text>
//                         </View>

//                         <View style={styles.inputTextBox}>
//                             <TextInput style={styles.inputTextContent}
//                                 onChangeText={(name) => {
//                                     setValue("name", name)
//                                     setName(name)
//                                 }}
//                                 value={name} />
//                         </View>
//                     </View>

//                     <View style={styles.item}>
//                         <View style={styles.iconContent}>
//                             <MaterialCommunityIcons name="email" style={styles.icon} />
//                         </View>

//                         <View style={styles.infoContent}>
//                             <Text style={styles.info}>電郵</Text>
//                         </View>

//                         <View style={styles.inputTextBox}>
//                             <TextInput style={styles.inputTextContent}
//                                 onChangeText={(email) => {
//                                     setValue("email", email)
//                                     setEmail(email)
//                                 }}
//                                 value={email} />
//                         </View>
//                     </View>

//                     <View style={styles.item}>
//                         <View style={styles.iconContent}>
//                             <MaterialCommunityIcons name="cellphone" style={styles.icon} />
//                         </View>

//                         <View style={styles.infoContent}>
//                             <Text style={styles.info}>電話</Text>
//                         </View>

//                         <View style={styles.inputTextBox}>
//                             <TextInput style={styles.inputTextContent}
//                                 keyboardType={'numeric'}
//                                 onChangeText={(phone) => {
//                                     setValue("phone", phone)
//                                     setPhone(phone)
//                                 }}
//                                 value={phone.toString()}
//                             />
//                         </View>
//                     </View>
//                 </View>

//                 <View>
//                     <Modal
//                         animationType="slide"
//                         transparent={true}
//                         visible={confirmModalVisible}
//                         onRequestClose={() => {
//                             Alert.alert("Modal has been closed.");
//                             setConfirmModalVisible(!confirmModalVisible);
//                         }}
//                     >
//                         <View style={styles.modalCenteredView}>
//                             {/* Confirm order modal */}
//                             <View style={styles.modalView}>
//                                 <Text style={styles.modalText}>修改您的個人資料嗎?</Text>
//                                 <View style={styles.modalButtonRow}>
//                                     {isLoading == 1 && <SplashScreen />}
//                                     <TouchableOpacity
//                                         style={[styles.modalButton, styles.modalButtonClose]}
//                                         onPress={async () => {
//                                             setIsOrderLoading(1);
//                                             await handleSubmit(onSubmit)();
//                                             setIsOrderLoading(-1);
//                                             setSuccessModalVisible(!successModalVisible)
//                                         }}
//                                     >
//                                         <Text style={styles.modalTextStyle}>確認修改</Text>
//                                     </TouchableOpacity>
//                                     <TouchableOpacity
//                                         style={[styles.modalButton, styles.modalButtonClose]}
//                                         onPress={() => setConfirmModalVisible(!confirmModalVisible)}
//                                     >
//                                         <Text style={styles.modalTextStyle}>取消修改</Text>
//                                     </TouchableOpacity>
//                                 </View>
//                             </View>
//                         </View>
//                     </Modal>

//                     <Modal
//                         animationType="slide"
//                         transparent={true}
//                         visible={successModalVisible}
//                         onRequestClose={() => {
//                             Alert.alert("Modal has been closed.");
//                             setSuccessModalVisible(!successModalVisible);
//                         }}
//                     >
//                         <View style={styles.modalCenteredView}>
//                             {/* Success sent order Modal */}
//                             <View style={styles.modalView}>
//                                 <Text style={styles.modalText}>成功修改個人資料!</Text>
//                                 <View style={styles.modalButtonRow}>
//                                     <Pressable
//                                         style={[styles.modalButton, styles.modalButtonClose]}
//                                         onPress={() => {
//                                             setSuccessModalVisible(!successModalVisible)
//                                             goToSetting()
//                                         }}>
//                                         <Text style={styles.modalTextStyle}>跳轉至個人資料</Text>
//                                     </Pressable>

//                                 </View>
//                             </View>
//                         </View>
//                     </Modal>

//                     <Modal
//                         animationType="slide"
//                         transparent={true}
//                         visible={failModalVisible}
//                         onRequestClose={() => {
//                             Alert.alert("Modal has been closed.");
//                             setFailModalVisible(!failModalVisible);
//                         }}
//                     >
//                         <View style={styles.modalCenteredView}>
//                             {/* Fail sent order Modal */}
//                             <View style={styles.modalView}>
//                                 <Text style={styles.modalText}>無法修改個人資料</Text>
//                                 <View style={styles.modalButtonRow}>
//                                     <Pressable
//                                         style={[styles.modalButton, styles.modalButtonClose]}
//                                         onPress={() => {
//                                             setFailModalVisible(!failModalVisible)
//                                         }}>
//                                         <Text style={styles.modalTextStyle}>返回個人資料</Text>
//                                     </Pressable>
//                                 </View>
//                             </View>
//                         </View>
//                     </Modal>
//                 </View >
//             </View>
//         </View>
//     );
// }


// const styles = StyleSheet.create({
//     container: {
//         paddingHorizontal: 10,
//     },
//     header: {
//         backgroundColor: "#f1f2eb",
//     },
//     headerContent: {
//         padding: 30,
//         alignItems: 'center',
//     },
//     name: {
//         fontSize: 30,
//         color: "#566246",
//         fontWeight: 'bold',
//         paddingVertical: 20,
//     },
//     userInfo: {
//         fontSize: 16,
//         color: "#566246",
//         fontWeight: '600',
//     },
//     body: {
//         backgroundColor: "#f1f2eb",
//         height: 620,
//         alignItems: 'center',
//         paddingTop: 50,
//     },
//     item: {
//         flexDirection: 'row',
//         alignItems: 'center',
//         marginBottom: 10,
//     },
//     iconContent: {
//         flex: 1,
//         alignItems: 'center',
//         paddingRight: 5,
//     },
//     infoContent: {
//         flex: 2,
//     },
//     taxiTypeContent: {
//         flex: 5,
//         flexDirection: 'row',
//         justifyContent: 'space-evenly',
//     },
//     inputTextBox: {
//         flex: 5,
//         alignItems: 'center',
//     },
//     inputTextContent: {
//         backgroundColor: '#f1f2eb',
//         borderRadius: 5,
//         borderColor: "#566246",
//         borderWidth: 1,
//         justifyContent: 'center',
//         textAlign: 'center',
//         height: 40,
//         width: 200,
//     },
//     booleanOption: {
//         flex: 2,
//     },
//     inputBooleanContent: {
//         flex: 1,
//         // marginRight: 10,
//     },
//     info: {
//         // fontSize: 18,
//         // marginTop: 20,
//         color: "#4a4a48",
//         // width:20,
//         justifyContent: 'center',
//     },
//     buttonText: {
//         color: 'white',
//         textAlign: 'center'
//     },
//     button: {
//         backgroundColor: '#566246',
//         borderRadius: 13,
//         marginHorizontal: 50,
//         // marginTop: 100,
//         paddingVertical: 10
//     },
//     categoryLabel: {
//         fontWeight: "bold",
//         marginHorizontal: 10
//     },
//     categoryContainer: {
//         flexDirection: "row",
//         marginVertical: 10,
//         flexWrap: "wrap"
//     },
//     iconSelected: {
//         opacity: 1,
//         alignItems: 'flex-end',
//     },
//     iconDeselected: {
//         opacity: 0.2,

//         alignItems: 'flex-end',
//     },
//     icon: {
//         width: 30,
//         height: 30,
//         marginTop: 15,
//         // marginHorizontal: 10,
//         fontSize: 20,
//         color: "#4a4a48",
//     },
//     // icon: {
//     //     marginHorizontal: 10,
//     //     fontSize: 70
//     // },
//     iconLabel: {
//         // textAlign: "center",
//         fontWeight: "600",
//         color: "black"
//     },
//     taxiType: {
//         alignItems: 'center',
//     },
//     modalCenteredView: {
//         flex: 1,
//         justifyContent: "center",
//         alignItems: "center",
//         marginTop: 0
//     },
//     modalView: {
//         margin: 20,
//         backgroundColor: "white",
//         borderRadius: 20,
//         padding: 35,
//         alignItems: "center",
//         shadowColor: "#000",
//         shadowOffset: {
//             width: 0,
//             height: 2
//         },
//         shadowOpacity: 0.25,
//         shadowRadius: 4,
//         elevation: 5,
//     },
//     modalButtonRow: {
//         flexDirection: "row",
//     },
//     modalButton: {
//         borderRadius: 20,
//         padding: 10,
//         elevation: 2,
//         margin: 5,
//     },
//     modalButtonOpen: {
//         backgroundColor: "#F194FF",
//     },
//     modalButtonClose: {
//         backgroundColor: "#2196F3",
//     },
//     modalTextStyle: {
//         color: "white",
//         fontWeight: "bold",
//         textAlign: "center"
//     },
//     modalText: {
//         marginBottom: 15,
//         textAlign: "center"
//     },
//     spinnerTextStyle: {
//         color: 'white',
//         fontSize: 20,
//     },
// });
