import { useFocusEffect } from "@react-navigation/native";
import React, { useCallback, useState } from "react";
import { Alert, Modal, StyleSheet, Text, Pressable, View, TouchableOpacity, ScrollView, } from "react-native";
import { TextInput } from "react-native-paper";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { useSelector } from "react-redux";
import { RootState } from "../../../store";




export default function Review() {
    const [modalVisible, setModalVisible] = useState(false);
    const [isOrderLoading, setIsOrderLoading] = useState(0);
    const [confirmModalVisible, setConfirmModalVisible] = useState(false);
    const [successModalVisible, setSuccessModalVisible] = useState(false);
    const [failModalVisible, setFailModalVisible] = useState(false);
    const [workBtnModalVisible, setWorkBtnModalVisible] = useState(false);
    // const { onPress, title = 'Save' } = props;
    // let initArr: number[] = [];
    const [arr, setArr] = useState([]);
    const [defaultRating, setDefaultRating] = useState(0);
    // To set the max number of Stars
    const [maxRating, setMaxRating] = useState([1, 2, 3, 4, 5]);
    //remarks box TODO
    const [text, setText] = React.useState('');
    const origin = useSelector((state: RootState) => state.order.originDetails?.place_id);

    useFocusEffect(
        useCallback(() => {
            // Do something when the screen is focused
            console.log(origin)
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
            };
        }, [origin])
    );


    const CustomRatingBar = () => {

        return (
            <View style={styles.customRatingBarStyle}>

                {maxRating.map((item, key) => {
                    return (
                        <TouchableOpacity
                            activeOpacity={0.7}
                            key={item}
                            onPress={() => setDefaultRating(item)}>
                            <MaterialCommunityIcons name={item <= defaultRating ? 'star' : 'star-outline'} color='#a4c2a5' size={40} />
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };



    return (
        <View style={styles.centeredView}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>


                        <ScrollView style={styles.container}>
                            <View style={styles.ratingContainer}>
                                <View style={styles.serviceRating}>
                                    <Text style={styles.service}>Service <Text style={styles.rating}>Rating</Text></Text>
                                </View>

                                <View>
                                    <View style={styles.ratingCol}>
                                        <View style={styles.ratingRow}>
                                            <Text style={styles.ratingItem}>How was your experience ?</Text>
                                        </View>
                                        <View style={styles.ratingRow}>
                                            <Text style={styles.ratingBar}><CustomRatingBar /></Text>
                                        </View>
                                    </View>
                                </View>

                                <TextInput
                                    style={styles.remarks}
                                    mode="outlined"
                                    label="Remarks"
                                    placeholder="Type something"
                                    right={<TextInput.Affix text="/100" />}
                                />
                                <TouchableOpacity
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={async () => {
                                        setIsOrderLoading(1);
                                        setIsOrderLoading(-1);
                                        setModalVisible(!modalVisible)
                                    }}
                                >
                                    <Text style={styles.textStyle}>Submit</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>


                    </View>
                </View>
            </Modal>
            <Pressable
                style={[styles.btnElem, styles.buttonOpen]}
                onPress={() => setModalVisible(true)}
            >
                <MaterialIcons name="star" color="black"
                    style={styles.btnIcon} />
            </Pressable>
        </View>
    );
};

const styles = StyleSheet.create({
    centeredView: {
        // flex: 1,
        // justifyContent: "center",
        // alignItems: "center",
        // marginTop: 22
    },
    modalView: {
        height: '90%',
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 10,
        justifyContent: 'center',
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    buttonOpen: {
        backgroundColor: "blue",
        justifyContent: 'center',
        alignItems: "center",
    },
    buttonClose: {
        backgroundColor: "#566246",
        justifyContent: 'center',
        alignItems: "center",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    container: {
        padding: 15,
        backgroundColor: 'white',
    },
    ratingContainer: {
        // backgroundColor: 'yellow',
        borderColor: '#4a4a48',
        borderRadius: 20,
    },
    ratingRow: {
        flexDirection: 'row',
        marginVertical: 5,
        padding: 10,
        borderRadius: 15,
        justifyContent: 'center',
    },
    ratingItem: {
        // backgroundColor:'green'
        color: '#4a4a48',
        fontSize: 20,
        marginTop: 80,
    },
    customRatingBarStyle: {
        // justifyContent: 'center',
        flexDirection: 'row',
        // backgroundColor: 'purple'
        // marginTop: 30,
    },
    remarks: {
        marginHorizontal: 20,
        // backgroundColor: 'red',
    },
    buttonText: {
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        margin: 20,
    },
    button: {
        backgroundColor: '#566246',
        borderRadius: 10,
        margin: 20,
        padding: 20,
    },
    service: {
        fontSize: 35,
        fontWeight: 'bold',
        color: '#a4c2a5',
        textAlign: "center",
    },
    rating: {
        fontSize: 35,
        fontWeight: 'bold',
        color: '#4a4a48'
    },
    serviceRating: {

    },
    ratingCol: {
        flexDirection: 'column',
    },
    ratingBar: {
        // backgroundColor: 'orange',
    },
    btnElem: {
        justifyContent: 'center',
        alignItems: "center",
        backgroundColor: 'rgba(25,114,232,1)',
        borderRadius: 200,
    },
    btnIcon: {
        fontSize: 50,
        // padding: 15,
        color: 'yellow'
    },
});















import { useFocusEffect } from '@react-navigation/native';
import React, { useCallback, useEffect, useState } from 'react';
import { TouchableOpacity, View, StyleSheet, Pressable, Text, ScrollView } from 'react-native';
import { DataTable, TextInput } from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useSelector } from 'react-redux';
import { RootState } from '../../../store';

export default function Review() {
    // const { onPress, title = 'Save' } = props;
    // let initArr: number[] = [];
    const [arr, setArr] = useState([]);
    const [defaultRating, setDefaultRating] = useState(0);
    // To set the max number of Stars
    const [maxRating, setMaxRating] = useState([1, 2, 3, 4, 5]);
    //remarks box TODO
    const [text, setText] = React.useState('');
    const origin = useSelector((state: RootState) => state.order.originDetails?.place_id);

    useFocusEffect(
        useCallback(() => {
            // Do something when the screen is focused
            console.log(origin)
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
            };
        }, [origin])
    );

    const CustomRatingBar = () => {

        return (
            <View style={styles.customRatingBarStyle}>

                {maxRating.map((item, key) => {
                    return (
                        <TouchableOpacity
                            activeOpacity={0.7}
                            key={item}
                            onPress={() => setDefaultRating(item)}>
                            <MaterialCommunityIcons name={item <= defaultRating ? 'star' : 'star-outline'} color='#a4c2a5' size={50} />
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };
    return (
        <ScrollView style={styles.container}>
            <View style={styles.ratingContainer}>
                <View style={styles.serviceRating}>
                    <Text style={styles.service}>Service <Text style={styles.rating}>Rating</Text></Text>
                </View>

                <View>
                    <View style={styles.ratingCol}>
                        <View style={styles.ratingRow}>
                            <Text style={styles.ratingItem}>How was your experience ?</Text>
                        </View>
                        <View style={styles.ratingRow}>
                            <Text style={styles.ratingBar}><CustomRatingBar /></Text>
                        </View>
                    </View>
                </View>

                <TextInput
                    style={styles.remarks}
                    mode="outlined"
                    label="Remarks"
                    placeholder="Type something"
                    right={<TextInput.Affix text="/100" />}
                />
                <TouchableOpacity style={styles.button} onPress={() => setArr([])}>
                    <Text style={styles.buttonText}>Submit</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 30,
        paddingHorizontal: 30,
        backgroundColor: '#f1f2eb',
    },
    ratingContainer: {
        // backgroundColor: 'yellow',
        borderColor: '#4a4a48',
        borderRadius: 20,
    },
    ratingRow: {
        flexDirection: 'row',
        marginVertical: 5,
        padding: 10,
        borderRadius: 15,
        justifyContent: 'center',
    },
    ratingItem: {
        // backgroundColor:'green'
        color: '#4a4a48',
        fontSize: 20,
        marginTop: 80,
    },
    customRatingBarStyle: {
        // justifyContent: 'center',
        flexDirection: 'row',
        // backgroundColor: 'purple'
        // marginTop: 30,
    },
    remarks: {
        marginHorizontal: 20,
        // backgroundColor: 'red',
    },
    buttonText: {
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        margin: 20,
    },
    button: {
        backgroundColor: '#566246',
        borderRadius: 13,
        margin: 20,
        paddingVertical: 10,
    },
    service: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#a4c2a5',
        textAlign: "center",
    },
    rating: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#4a4a48'
    },
    serviceRating: {

    },
    ratingCol: {
        flexDirection: 'column',
    },
    ratingBar: {
        // backgroundColor: 'orange',
    },
});
