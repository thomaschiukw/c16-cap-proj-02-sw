import { useFocusEffect } from "@react-navigation/native";
import React, { useCallback, useState } from "react";
import { Alert, Modal, StyleSheet, Text, Pressable, View, TouchableOpacity, ScrollView, } from "react-native";
import { TextInput } from "react-native-paper";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { useSelector } from "react-redux";
import { RootState } from "../../../store";




export default function Review() {
    const [modalVisible, setModalVisible] = useState(false);
    const [isOrderLoading, setIsOrderLoading] = useState(0);
    const [confirmModalVisible, setConfirmModalVisible] = useState(false);
    const [successModalVisible, setSuccessModalVisible] = useState(false);
    const [failModalVisible, setFailModalVisible] = useState(false);
    const [workBtnModalVisible, setWorkBtnModalVisible] = useState(false);
    // const { onPress, title = 'Save' } = props;
    // let initArr: number[] = [];
    const [arr, setArr] = useState([]);
    const [defaultRating, setDefaultRating] = useState(0);
    // To set the max number of Stars
    const [maxRating, setMaxRating] = useState([1, 2, 3, 4, 5]);
    //remarks box TODO
    const [text, setText] = React.useState('');


    // const origin = useSelector((state: RootState) => state.order.originDetails?.place_id);

    // useFocusEffect(
    //     useCallback(() => {
    //         // Do something when the screen is focused
    //         console.log(origin)
    //         return () => {
    //             // Do something when the screen is unfocused
    //             // Useful for cleanup functions
    //         };
    //     }, [origin])
    // );


    const CustomRatingBar = () => {

        return (
            <View style={styles.customRatingBarStyle}>

                {maxRating.map((item, key) => {
                    return (
                        <TouchableOpacity
                            activeOpacity={0.7}
                            key={item}
                            onPress={() => setDefaultRating(item)}>
                            <MaterialCommunityIcons name={item <= defaultRating ? 'star' : 'star-outline'} color='#a4c2a5' size={45} />
                        </TouchableOpacity>
                    );
                })}
            </View>
        );
    };



    return (
        <View style={styles.centeredView}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>


                        <ScrollView style={styles.container}>
                            <View style={styles.ratingContainer}>
                                <View style={styles.serviceRating}>
                                    <Text style={styles.service}>服務<Text style={styles.rating}>評價</Text></Text>
                                </View>

                                <View>
                                    <View style={styles.ratingCol}>
                                        <View style={styles.ratingRow}>
                                            <Text style={styles.ratingItem}>覺得這次行程如何 ?</Text>
                                        </View>
                                        <View style={styles.ratingRow}>
                                            <Text style={styles.ratingBar}><CustomRatingBar /></Text>
                                        </View>
                                    </View>
                                </View>

                                <TextInput
                                    style={styles.remarks}
                                    mode="outlined"
                                    label="評語"
                                    placeholder="Type something"
                                    right={<TextInput.Affix text="/100" />}
                                />

                                <View style={styles.endButton}>
                                    <TouchableOpacity
                                        style={[styles.button, styles.buttonClose]}
                                        onPress={async () => {
                                            setIsOrderLoading(1);
                                            setIsOrderLoading(-1);
                                            setModalVisible(!modalVisible)
                                        }}
                                    >
                                        <Text style={styles.textStyle}>確定</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        style={[styles.button, styles.buttonClose]}
                                        onPress={async () => {
                                            setModalVisible(!modalVisible)
                                        }}
                                    >
                                        <Text style={styles.textStyle}>返回</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>


                    </View>
                </View>
            </Modal>
            <TouchableOpacity
                style={[styles.btnElem, styles.buttonOpen]}
                onPress={() => setModalVisible(true)}
            >
                <MaterialIcons name="star-border" color="black"
                    style={styles.btnIcon} />
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    centeredView: {
        // flex: 1,
        // justifyContent: "center",
        // alignItems: "center",
        // marginTop: 22
    },
    modalView: {
        height: '90%',
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        // padding: 5,
        justifyContent: 'center',
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    buttonOpen: {
        backgroundColor: "#d8dad3",
        justifyContent: 'center',
        alignItems: "center",
    },
    buttonClose: {
        backgroundColor: "#566246",
        justifyContent: 'center',
        alignItems: "center",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 20,
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    container: {
        paddingVertical: 10,
        backgroundColor: 'white',
        width: '90%',
    },
    ratingContainer: {
        // backgroundColor: 'yellow',
        borderColor: '#4a4a48',
        borderRadius: 20,
    },
    ratingRow: {
        marginVertical: 5,
        padding: 10,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: "center",
    },
    ratingItem: {
        // backgroundColor: 'green',
        color: '#4a4a48',
        fontSize: 20,
        marginTop: 100,
    },
    customRatingBarStyle: {
        // justifyContent: 'center',
        flexDirection: 'row',
        // backgroundColor: 'purple'
    },
    remarks: {
        marginHorizontal: 20,
        // backgroundColor: 'red',
    },
    buttonText: {
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        margin: 20,
    },
    button: {
        backgroundColor: '#566246',
        borderRadius: 10,
        marginTop: 30,
        marginHorizontal: "3%",
        padding: 15,
        width: '40%',
    },
    service: {
        fontSize: 35,
        fontWeight: 'bold',
        color: '#a4c2a5',
        textAlign: "center",
    },
    rating: {
        fontSize: 35,
        fontWeight: 'bold',
        color: '#4a4a48',
    },
    serviceRating: {
        paddingTop: 50,
    },
    ratingCol: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: "center",
    },
    ratingBar: {
        // backgroundColor: 'orange',
        marginVertical: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: "center",
    },
    btnElem: {
        justifyContent: 'center',
        alignItems: "center",
        backgroundColor: 'rgba(25,114,232,1)',
        borderRadius: 200,
    },
    btnIcon: {
        fontSize: 40,
        // padding: 15,
        color: '#566246'
    },
    endButton: {
        flexDirection: "row",
        justifyContent: 'center',
    }
});