import React, { Component, useEffect } from 'react';
import { StyleSheet, Text, View, Pressable, ScrollView, Image } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { NavigationProp, useNavigation, TabActions } from '@react-navigation/native'
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../store';
import { fetchUserInfo } from '../../../redux/user/thunk';
import SplashScreen from '../../../components/SplashScreen';
import { UserLogout } from '../home/UserLogout';
import { UserSettingStackScreenParamList } from './UserSettingStackScreen';

export default function UserSetting() {
    const dispatch = useDispatch();
    const isLoading = useSelector((state: RootState) => state.loading.isLoading);
    const userID = useSelector((state: RootState) => state.auth.user?.id);
    const userInfo = useSelector((state: RootState) => state.user);

    function updateUserInfo() {
        if (userID) {
            dispatch(fetchUserInfo(userID));
        }
    }

    useEffect(() => {
        updateUserInfo()
    }, [dispatch, userInfo])

    const navigation: NavigationProp<UserSettingStackScreenParamList> = useNavigation();
    const goToChangeSetting = () => {
        navigation.navigate('更改設定');
    }

    return (
        <ScrollView keyboardDismissMode='interactive'>
            {isLoading === 1 && <SplashScreen />}
            <View>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.nameBox}>
                            <View style={styles.rego}><Text style={styles.name}>{userInfo.username}</Text></View>
                            <Image style={styles.settingLogo} source={require('../../src/setting2.png')} />
                        </View>
                    </View>



                    <View style={styles.headerContent}>
                        <Text style={styles.userInfo}>{userInfo.email}</Text>
                        <Text style={styles.userInfo}>{userInfo.phone}</Text>
                    </View>
                </View>
                <Pressable style={styles.button}
                    onPress={() => goToChangeSetting()}
                >
                    <Text style={styles.buttonText}>修改設定</Text>
                </Pressable>
                <UserLogout />
            </View>
        </ScrollView >
    );
}


const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 10,
    },
    header: {
        backgroundColor: "#f1f2eb",
    },
    nameBox: {
        paddingTop: 30,
        alignItems: 'center',
    },
    headerContent: {
        paddingVertical: 30,
        alignItems: 'center',
    },
    rego:{
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#566246',
        backgroundColor: "white",
        position: "absolute",
        top: 190,
        zIndex: 2,
        width: "40%",
    },
    name: {
        fontSize: 25,
        color: "#566246",
        fontWeight: 'bold',
        textAlign: "center"
    },
    settingLogo: {
        height: 240,
        width: 360,
        resizeMode: 'contain',
        alignSelf: "center",
    },
    userInfo: {
        fontSize: 16,
        color: "#566246",
        fontWeight: '600',
    },
    item: {
        flexDirection: 'row',
    },
    infoContent: {
        flex: 1,
        alignItems: 'flex-start',
        paddingLeft: 5
    },
    iconContent: {
        flex: 1,
        alignItems: 'center',
        paddingRight: 5,
    },
    icon: {
        width: 30,
        height: 30,
        marginVertical: 20,
        marginHorizontal: 10,
        fontSize: 20,
        color: "#4a4a48",

    },
    info: {
        // fontSize: 18,
        marginTop: 20,
        color: "#4a4a48",
    },
    buttonText: {
        color: 'white',
        textAlign: 'center'
    },
    button: {
        backgroundColor: '#566246',
        borderRadius: 10,
        marginHorizontal: '10%',
        paddingVertical: 10
    }
});
