import React, { Component, useEffect, useState } from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Switch, TextInput, TouchableOpacity, TouchableWithoutFeedback, Modal, Pressable, Keyboard, Alert, Image } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { useDispatch, useSelector } from "react-redux";
import { useForm, SubmitHandler, Controller } from 'react-hook-form';
import { changeUserInfo, fetchUserInfo } from "../../../redux/user/thunk";
import { RootState } from '../../../store';
import SplashScreen from '../../../components/SplashScreen';
import { NavigationProp, useNavigation } from '@react-navigation/native';
import { UserHomeTabParamList } from '../components/UserHomeTab';

export type UserFormInput = {
    name: string,
    email: string,
    phone: string
}

export default function UserSettingChange() {
    const dispatch = useDispatch();
    const isLoading = useSelector((state: RootState) => state.loading.isLoading);
    const userID = useSelector((state: RootState) => state.auth.user?.id);
    const userInfo = useSelector((state: RootState) => state.user);

    function updateUserInfo() {
        if (userID) {
            dispatch(fetchUserInfo(userID));
        }
    }

    useEffect(() => {
        updateUserInfo()
    }, [dispatch, userInfo])

    const [isOrderLoading, setIsOrderLoading] = useState(0);
    const [confirmModalVisible, setConfirmModalVisible] = useState(false);
    const [successModalVisible, setSuccessModalVisible] = useState(false);
    const [failModalVisible, setFailModalVisible] = useState(false);

    useEffect(() => {
        if (isOrderLoading == 1) {
            setConfirmModalVisible(!confirmModalVisible);
        }
        if (isOrderLoading == -1) {
            setSuccessModalVisible(!successModalVisible);
        }
    }, [isOrderLoading])

    const { register, setValue, handleSubmit, control, reset, formState: { errors } } = useForm<UserFormInput>({
        defaultValues: {
            name: userInfo.username,
            email: userInfo.email,
            phone: userInfo.phone,
        }
    })

    const onSubmit: SubmitHandler<UserFormInput> = data => dispatch(changeUserInfo(data));
    if (errors === true) {
        console.log('errors', errors);
    }

    const navigation: NavigationProp<UserHomeTabParamList> = useNavigation();
    const goToSetting = () => {
        userID && dispatch(fetchUserInfo(userID))
        navigation.goBack()
    }

    return (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <View>
                <View style={styles.container}>
                    <View style={styles.body}>
                        <Image style={styles.changingSettingLogo} source={require('../../src/changingSetting.png')} />

                        <View style={styles.item}>
                            <View style={styles.iconContent}>
                                <MaterialCommunityIcons name="email" style={styles.icon} />
                            </View>


                            <View style={styles.infoContent}>
                                <Text style={styles.info}>電郵地址</Text>
                            </View>
                            <View style={styles.inputTextBox}><Controller
                                control={control}
                                render={({ field: { onChange, onBlur, value } }) => (
                                    <TextInput
                                        style={styles.inputTextContent}
                                        textContentType='emailAddress'
                                        keyboardType='email-address'
                                        onBlur={onBlur}
                                        onChangeText={value => onChange(value)}
                                        value={value}
                                    />
                                )}
                                name="email"
                                rules={{
                                    required: true, pattern: {
                                        value: /\S+@\S+\.\S+/,
                                        message: "請輸入有效電郵地址",
                                    }
                                }}
                            /><View style={{ flexDirection: 'row' }}>
                                    {errors.email && errors.email.type === "required" && <Text style={styles.errText}>"請輸入您的電郵地址"</Text>}
                                    {errors.email && <Text style={styles.errText}>{errors.email.message}</Text>}
                                    <Text> </Text></View></View>

                        </View>

                        <View style={styles.item}>

                            <View style={styles.iconContent}>
                                <MaterialCommunityIcons name="cellphone" style={styles.icon} />
                            </View>

                            <View style={styles.infoContent}>
                                <Text style={styles.info}>電話號碼</Text>
                            </View>

                            <View style={styles.inputTextBox}>
                                <Controller
                                    control={control}
                                    render={({ field: { onChange, onBlur, value } }) => (
                                        <TextInput style={styles.inputTextContent}
                                            maxLength={8}
                                            keyboardType={'numeric'}
                                            onChangeText={value => onChange(value.replace(/[^0-9]/g, ''))}
                                            value={value}
                                        />
                                    )}
                                    name="phone"
                                    rules={{ required: true, minLength: 8 }}
                                />
                                <View style={{ flexDirection: 'row' }}>
                                    {errors.phone && errors.phone.type === "required" && <Text style={styles.errText}>"請輸入您的電話號碼"</Text>}
                                    {errors.phone && errors.phone.type === "minLength" && <Text style={styles.errText}>"請輸入有效電話號碼"</Text>}
                                    <Text> </Text></View>
                            </View>

                        </View>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => {
                                if (errors.name?.message == undefined &&
                                    errors.email?.message == undefined &&
                                    errors.phone?.message == undefined) {
                                    setConfirmModalVisible(!confirmModalVisible)
                                }
                            }}>
                            <Text style={styles.buttonText}>確認修改</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => { navigation.navigate("設定主頁") }}>
                            <Text style={styles.buttonText}>返回</Text>
                        </TouchableOpacity>

                    </View>


                    <View>
                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={confirmModalVisible}
                            onRequestClose={() => {
                                Alert.alert("Modal has been closed.");
                                setConfirmModalVisible(!confirmModalVisible);
                            }}
                        >
                            <View style={styles.modalCenteredView}>
                                {/* Confirm order modal */}
                                <View style={styles.modalView}>
                                    <Text style={styles.modalText}>修改您的個人資料嗎?</Text>
                                    <View style={styles.modalButtonRow}>
                                        {isLoading == 1 && <SplashScreen />}
                                        <TouchableOpacity
                                            style={[styles.modalButton, styles.modalButtonClose]}
                                            onPress={async () => {
                                                setIsOrderLoading(1);
                                                await handleSubmit(onSubmit)();
                                                setIsOrderLoading(-1);
                                                setSuccessModalVisible(!successModalVisible)
                                            }}
                                        >
                                            <Text style={styles.modalTextStyle}>確認修改</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            style={[styles.modalButton, styles.modalButtonClose]}
                                            onPress={() => setConfirmModalVisible(!confirmModalVisible)}
                                        >
                                            <Text style={styles.modalTextStyle}>取消修改</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </Modal>

                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={successModalVisible}
                            onRequestClose={() => {
                                Alert.alert("Modal has been closed.");
                                setSuccessModalVisible(!successModalVisible);
                            }}
                        >
                            <View style={styles.modalCenteredView}>
                                {/* Success sent order Modal */}
                                <View style={styles.modalView}>
                                    <Text style={styles.modalText}>成功修改個人資料!</Text>
                                    <View style={styles.modalButtonRow}>
                                        <Pressable
                                            style={[styles.modalButton, styles.modalButtonClose]}
                                            onPress={() => {
                                                setSuccessModalVisible(!successModalVisible)
                                                goToSetting()
                                            }}>
                                            <Text style={styles.modalTextStyle}>跳轉至個人資料</Text>
                                        </Pressable>

                                    </View>
                                </View>
                            </View>
                        </Modal>

                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={failModalVisible}
                            onRequestClose={() => {
                                Alert.alert("Modal has been closed.");
                                setFailModalVisible(!failModalVisible);
                            }}
                        >
                            <View style={styles.modalCenteredView}>
                                {/* Fail sent order Modal */}
                                <View style={styles.modalView}>
                                    <Text style={styles.modalText}>無法修改個人資料</Text>
                                    <View style={styles.modalButtonRow}>
                                        <Pressable
                                            style={[styles.modalButton, styles.modalButtonClose]}
                                            onPress={() => {
                                                setFailModalVisible(!failModalVisible)
                                            }}>
                                            <Text style={styles.modalTextStyle}>返回個人資料</Text>
                                        </Pressable>
                                    </View>
                                </View>
                            </View>
                        </Modal>
                    </View >
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 10,
    },
    header: {
        backgroundColor: "#f1f2eb",
    },
    headerContent: {
        padding: '10%',
        alignItems: 'center',
    },
    name: {
        fontSize: 30,
        color: "#566246",
        fontWeight: 'bold',
        paddingVertical: 20,
    },
    userInfo: {
        fontSize: 16,
        color: "#566246",
        fontWeight: '600',
    },
    body: {
        // backgroundColor: "yellow",
        height: '100%',
        alignItems: 'center',
        paddingTop: 10,
    },
    changingSettingLogo: {
        width: 150,
        height: 150,
        resizeMode: 'contain',
        alignSelf: "center",
    },
    item: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: -10,
    },
    iconContent: {
        flex: 1,
        alignItems: 'center',
        paddingRight: 5,
    },
    input_row: {
        flexDirection: 'row'
    },
    infoContent: {
        flex: 2,
    },
    taxiTypeContent: {
        flex: 5,
        flexDirection: 'column',

        alignItems: 'center',
        marginTop: 10,
    },
    taxiTypesBox: {
        flexDirection: 'row',
    },
    inputTextBox: {
        marginTop: 20,
        flex: 5,
        alignItems: 'center',
    },
    inputTextContent: {
        backgroundColor: '#f1f2eb',
        borderRadius: 5,
        borderColor: "#566246",
        borderWidth: 1,
        justifyContent: 'center',
        textAlign: 'center',
        height: 40,
        width: 200,
    },
    booleanOption: {
        flex: 2,
    },
    inputBooleanContent: {
        flex: 1,
    },
    info: {
        color: "#4a4a48",
        justifyContent: 'center',
    },
    buttonText: {
        color: 'white',
        textAlign: 'center'
    },
    button: {
        backgroundColor: '#566246',
        borderRadius: 10,
        marginTop: 20,
        paddingVertical: 10,
        width: "90%"
    },
    categoryLabel: {
        fontWeight: "bold",
        marginHorizontal: 10
    },
    categoryContainer: {
        flexDirection: "row",
        marginVertical: 10,
        flexWrap: "wrap"
    },
    iconSelected: {
        opacity: 1,
        alignItems: 'flex-end',
    },
    iconDeselected: {
        opacity: 0.2,

        alignItems: 'flex-end',
    },
    icon: {
        width: 30,
        height: 30,
        marginTop: 15,
        fontSize: 20,
        color: "#4a4a48",
    },
    iconLabel: {
        fontWeight: "600",
        color: "black"
    },
    taxiType: {
        alignItems: 'center',
        marginHorizontal: 15,
    },
    modalCenteredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    modalButtonRow: {
        flexDirection: "row",
    },
    modalButton: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        margin: 5,
    },
    modalButtonOpen: {
        backgroundColor: "#F194FF",
    },
    modalButtonClose: {
        backgroundColor: "#566246",
    },
    modalTextStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    spinnerTextStyle: {
        color: 'white',
        fontSize: 20,
    },
    errText: {
        color: 'red',
    },
});
