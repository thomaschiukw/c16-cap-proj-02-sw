import * as React from 'react';
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import UserSetting from './UserSetting';
import UserSettingChange from './UserSettingChange';

export type UserSettingStackScreenParamList = {
    設定主頁: undefined;
    更改設定: undefined;
};

interface CustomTabBarLabelStyle {
    fontSize: number;
    fontWeight: "bold" | "normal" | "100" | "200" | "300" | "400" | "500" | "600" | "700" | "800" | "900" | undefined;
};

const customTabBarLabelStyle: CustomTabBarLabelStyle = {
    fontSize: 7.5,
    fontWeight: "500"
};

const Stack = createNativeStackNavigator<UserSettingStackScreenParamList>();

export function UserSettingStackScreen() {
    return (
        <Stack.Navigator
            initialRouteName="設定主頁"
        >
            <Stack.Screen
                name="設定主頁"
                component={UserSetting}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="更改設定"
                component={UserSettingChange}
                options={{ 
                    title: "更改設定" ,
                    // headerBackVisible: true,
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
};

