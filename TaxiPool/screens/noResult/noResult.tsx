import { StyleSheet, View, Text, Image } from 'react-native';
import React from "react";

export default function NoResult() {
    return (
        <View style={styles.container}>
            <Image style={styles.noResultLogo} source={require('../src/noResult.png')} />
            <Text>沒有紀錄</Text>
        </View>
    )

}

const styles = StyleSheet.create({
    container:{
        alignSelf: "center",
        justifyContent: "center",
        alignItems: 'center',
    },
    noResultLogo: {
        width: 200,
        height: 200,
        resizeMode: 'contain',
        alignSelf: "center",
    }
})