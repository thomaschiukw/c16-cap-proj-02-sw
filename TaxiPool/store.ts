import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk, { ThunkDispatch } from 'redux-thunk'
import { AuthActions } from "./redux/authentication/actions";
import { authReducer, AuthState } from "./redux/authentication/reducer";
import { LoadingActions } from "./redux/loading/action";
import { loadingReducer, LoadingState } from "./redux/loading/reducer";
import { MapActions } from "./redux/map/actions";
import { mapReducer, MapState } from "./redux/map/reducer";
import { RegisterActions } from "./redux/register/action";
import { registerReducer, RegisterState } from "./redux/register/reducer";

import { OrderActions } from "./redux/order/action";
import { OrderReducer, OrderState } from "./redux/order/reducer";

import { ViewOrderActions } from "./redux/driverOrder/action";
import { DriverOrderState, driverOrderReducer } from "./redux/driverOrder/reducer";

import { DriverInfoActions } from "./redux/driver/action";
import { driverInfoReducer, DriverInfoState } from "./redux/driver/reducer";

import { PoolActions } from "./redux/pool/action";
import { PoolOrderReducer, PoolOrderState } from "./redux/pool/reducer";
import { permissionReducer, PermissionState } from "./redux/permission/reducer";
import { PermissionActions } from "./redux/permission/action";

import { UserInfoActions } from "./redux/user/action";
import { userInfoReducer, UserInfoState } from "./redux/user/reducer";

export interface RootState {
    auth: AuthState;
    loading: LoadingState;
    register: RegisterState;
    map: MapState;
    order: OrderState;
    driverOrder: DriverOrderState;
    user: UserInfoState,
    driver: DriverInfoState;
    pool: PoolOrderState;
    permission: PermissionState;
};

export type RootAction = AuthActions | LoadingActions | RegisterActions | MapActions | OrderActions | ViewOrderActions | PoolActions | UserInfoActions | DriverInfoActions | PermissionActions;

export type RootThunkDispatch = ThunkDispatch<RootState, null, RootAction>;

const reducer = combineReducers<RootState>({
    auth: authReducer,
    loading: loadingReducer,
    register: registerReducer,
    map: mapReducer,
    order: OrderReducer,
    driverOrder: driverOrderReducer,
    user: userInfoReducer,
    driver: driverInfoReducer,
    pool: PoolOrderReducer,
    permission: permissionReducer
});

export const store = createStore<RootState, RootAction, {}, {}>(
    reducer,
    composeWithDevTools(
        applyMiddleware(thunk),
    )
);