//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import { UserRegisterFormInput } from '../../screens/users/authentication/RegisterForm';
import { DriverRegisterFormInput } from '../../screens/users/authentication/DriverRegisterForm';
import { RootThunkDispatch, RootState } from "../../store";
import { finishLoading, loading } from '../loading/action';
import { registerFailed, registerSuccess } from './action';

export function fetchRegister(formData: UserRegisterFormInput) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            dispatch(loading());
            const res = await fetch(`${REACT_APP_BACKEND_URL}/register`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(formData),
            });

            if (res.status != 200) {
                dispatch(registerFailed('Status code not 200 OK'));
            }

            const result = await res.json();

            if (result.success) {
                dispatch(registerSuccess());
            } else {
                dispatch(registerFailed(result.error));
            }
        } catch (error) {
            console.error(error);
            return;
        } finally {
            dispatch(finishLoading());
        }
    };
};

export function fetchDriverRegister(formData: DriverRegisterFormInput) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            dispatch(loading());
            const res = await fetch(`${REACT_APP_BACKEND_URL}/register/driver`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(formData),
            });

            if (res.status != 200) {
                dispatch(registerFailed('Status code not 200 OK'));
            }

            const result = await res.json();

            if (result.success) {
                dispatch(registerSuccess());
            } else {
                dispatch(registerFailed(result.error));
            }
        } catch (error) {
            console.error(error);
            return;
        } finally {
            dispatch(finishLoading());
        }
    };
};