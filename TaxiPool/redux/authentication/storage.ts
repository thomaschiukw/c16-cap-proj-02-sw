import AsyncStorage from '@react-native-async-storage/async-storage';

export const storeData = async (value: string) => {
    try {
        await AsyncStorage.setItem('@@auth_token', value);
        // console.log('Stored');
    } catch (e) {
        console.error('e =', e);
        return e;
    }
}

export const getData = async () => {
    try {
        const value = await AsyncStorage.getItem('@@auth_token');
        if (value !== null) {
            // value previously stored
            return value;
        } else {
            return null;
        }
    } catch (e) {
        // error reading value
        console.error(e);
    }
}

export const removeValue = async () => {
    try {
        await AsyncStorage.removeItem('@@auth_token');
        // console.log('Removed');
    } catch (e) {
        console.error(e);
    }
}
