//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import { LoginFormInput } from "../../screens/users/authentication/LoginForm";
import { RootThunkDispatch, RootState } from "../../store";
import { finishLoading, loading } from '../loading/action';
import { loginFailed, loginSuccess, logout } from './actions';
import { getData, removeValue, storeData } from './storage';

export function login(formData: LoginFormInput) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            dispatch(loading());
            const res = await fetch(`${REACT_APP_BACKEND_URL}/login`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(formData),
            });

            if (res.status != 200) {
                dispatch(loginFailed('HTTP REQUEST FAILED'));
            }

            const result = await res.json();

            if (result.token) {
                dispatch(loginSuccess(result.token));
                try {
                    await storeData(result.token);
                } catch (error) {
                    console.error(error);
                    dispatch(loginFailed('STORE TOKEN FAILED'));
                }
            } else {
                dispatch(loginFailed(result.error));
            }
        } catch (error) {
            console.error(error);
            return;
        } finally {
            dispatch(finishLoading());
        }
    };
}

export function autoLogin() {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            dispatch(loading());
            const token = await getData();
            // console.log('token =', token);

            if (token !== null && token !== undefined) {
                dispatch(loginSuccess(token));
            } else {
                dispatch(loginFailed('NO TOKEN FOUND'));
            }
        } catch (error) {
            console.error(error);
            return;
        } finally {
            // dispatch(finishLoading());
        }
    };
}

export function fetchLogout() {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            dispatch(loading());
            await removeValue();
            dispatch(logout());
        } catch (error) {
            console.error(error);
            return;
        } finally {
            dispatch(finishLoading());
        }
    };
}