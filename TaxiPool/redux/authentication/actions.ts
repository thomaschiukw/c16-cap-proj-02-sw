export function loginSuccess(token: string) {
    return {
        type: '@@auth/LOGIN_SUCCESS' as const,
        token
    }
};

export function loginFailed(error: string) {
    return {
        type: '@@auth/LOGIN_FAILED' as const,
        error
    }
};

export function logout() {
    return {
        type: '@@auth/LOGOUT' as const,
    }
};

// type FAILED_INTENT = "SIGN_IN_FAILED"

// export function failed(type: FAILED_INTENT, msg: string) {
//     return {
//         type,
//         msg
//     }
// };

export type AuthActions =
    ReturnType<typeof loginSuccess | typeof loginFailed | typeof logout>;
    // ReturnType<typeof failed>