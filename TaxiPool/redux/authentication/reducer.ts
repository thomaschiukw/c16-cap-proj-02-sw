import produce from "immer";
import jwt_decode from "jwt-decode";
import { AuthActions } from "./actions";

export type JWTPayload = {
    id: number;
    username: string;
    isDriver: boolean;
};

export interface AuthState {
    isAuthenticated: boolean;
    token: string;
    user?: JWTPayload;
};

const initialState: AuthState = {
    isAuthenticated: false,
    token: ""
};

export function authReducer(state: AuthState = initialState, action: AuthActions): AuthState {
    switch (action.type) {
        case '@@auth/LOGIN_SUCCESS':
            let payload: JWTPayload = jwt_decode(action.token)
            // console.log(payload)
            return produce(state, state => {
                state.isAuthenticated = true;
                state.token = action.token;
                state.user = payload;
            })
        case '@@auth/LOGIN_FAILED':
            return produce(state, state => {
                state.isAuthenticated = false;
                state.token = "";
                state.user = undefined;
            })
        case '@@auth/LOGOUT':
            return produce(state, state => {
                state.isAuthenticated = false;
                state.token = "";
                state.user = undefined;
            })
        default:
            return state;
    }
};