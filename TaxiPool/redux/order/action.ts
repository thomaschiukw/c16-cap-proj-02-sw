import { ActiveOrder } from "./reducer";

export function orderSuccess() {
    return {
        type: '@@order/ORDER_SUCCESS' as const,
    }
};

export function poolList(orders: ActiveOrder[]) {
    return {
        type: '@@order/POOL_LIST' as const,
        orders
    }
};

export function currentPool(orders: ActiveOrder[]) {
    return {
        type: '@@order/CURRENT_POOL' as const,
        orders
    }
};


export function orderFailed(error: string) {
    console.log('error: ', error);
    return {
        type: '@@order/ORDER_FAILED' as const,
        error
    }
};

export function gotActiveOrder(orders: ActiveOrder[]) {
    return {
        type: '@@order/GOT_ACTIVE_ORDER' as const,
        orders
    }
}

type FAILED_INTENT = "@@order/GOT_ACTIVE_ORDER_FAILED"

export function orderReduxFailed(type: FAILED_INTENT, error: string) {
    return {
        type,
        error
    }
};

export type OrderActions =
    ReturnType<
        typeof orderSuccess
        | typeof orderFailed
        | typeof gotActiveOrder
        | typeof orderReduxFailed
        | typeof poolList
        | typeof currentPool
    >