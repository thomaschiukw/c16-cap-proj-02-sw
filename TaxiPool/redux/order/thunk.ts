//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import { OrderFormInput } from '../../screens/users/booking/OrderSettingsScreen';
import { RootThunkDispatch, RootState } from "../../store";
import { finishLoading, loading } from '../loading/action';
import { PlaceAutocompletePrediction } from '../map/reducer';
import { orderFailed, orderSuccess, orderReduxFailed, gotActiveOrder, poolList, currentPool } from './action';

export function fetchCreateOrder(formData: OrderFormInput) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            dispatch(loading());

            const orderForm = {
                user_id: getState().auth.user?.id,
                origin_place_id: getState().map.selectedOrigin?.details?.place_id,
                origin_lat: getState().map.selectedOrigin?.coordinates?.lat,
                origin_lng: getState().map.selectedOrigin?.coordinates?.lng,
                origin_district: getState().map.selectedOrigin.details?.terms![((getState().map.selectedOrigin.details?.terms!).length) - 2].value,
                origin_description: getState().map.selectedOrigin?.details?.description,
                destination_place_id: getState().map.selectedDestination?.details?.place_id,
                destination_lat: getState().map.selectedDestination?.coordinates?.lat,
                destination_lng: getState().map.selectedDestination?.coordinates?.lng,
                destination_district: getState().map.selectedDestination.details?.terms![((getState().map.selectedDestination.details?.terms!).length) - 2].value,
                destination_description: getState().map.selectedDestination?.details?.description,
                distance: getState().map.route.distance,
                fare: formData.minFare,
                pool: formData.isPool,
                hybrid: formData.isHybrid,
                red_taxi: formData.isRedTaxi,
                green_taxi: formData.isGreenTaxi,
                blue_taxi: formData.isBlueTaxi,
                cross_harbour_tunnel: formData.isCentralTunnel,
                eastern_harbour_crossing: formData.isEastTunnel,
                western_harbour_crossing: formData.isWestTunnel,
                has_pet: formData.hasPet,
                smoke_free: formData.isSmokeFree,
                has_luggage: formData.hasLuggage,
                is_disabled: formData.isDisabled,
                urgent: formData.isUrgent,
                headcount: formData.headcount
            }
            console.log(orderForm);
            

            const res = await fetch(`${REACT_APP_BACKEND_URL}/order`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(orderForm),
            });

            if (res.status != 200) {
                dispatch(orderFailed('HTTP REQUEST FAILED'));
                return;
            }

            const result = await res.json();
            console.log(result);
            

            if (result.success) {
                dispatch(orderSuccess());
            } else {
                dispatch(orderFailed(result.error));
            }
        } catch (error) {
            console.error(error);
            return;
        } finally {
            dispatch(finishLoading());
        }
    };
}

export function fetchActiveOrder(userId: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${REACT_APP_BACKEND_URL}/order/active/user/${userId}`);
            const result = await res.json();
            // console.log('result =', result);

            if (res.status != 200) {
                dispatch(orderReduxFailed('@@order/GOT_ACTIVE_ORDER_FAILED', 'HTTP REQUEST FAILED'));
                return;
            }

            if (result.success === false) {
                dispatch(orderReduxFailed('@@order/GOT_ACTIVE_ORDER_FAILED', result.msg));
            } else if (result.length === 0) {
                dispatch(orderReduxFailed('@@order/GOT_ACTIVE_ORDER_FAILED', 'EMPTY RESULT'));
            } else {
                dispatch(gotActiveOrder(result));
            }
        } catch (error) {
            console.error(error);
            return;
        }
    };
}

export function fetchPoolList(userId: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            // dispatch(loading());
            // console.log('value =', value)
            const res = await fetch(`${REACT_APP_BACKEND_URL}/pool-list/${userId}`);
            const result = await res.json();

            dispatch(poolList(result))

        } catch (error) {
            console.error(error);
            return;
        }
    };
}

export function fetchCurrentPool(userId: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            // dispatch(loading());
            // console.log('value =', value)
            const res = await fetch(`${REACT_APP_BACKEND_URL}/current-pool/${userId}`);
            const result = await res.json();
            // console.log('thunk: ', result);

            await dispatch(currentPool(result))

        } catch (error) {
            console.error(error);
            return;
        }
    };
}