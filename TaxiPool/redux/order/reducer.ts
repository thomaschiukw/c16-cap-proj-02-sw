import produce from "immer";
import { OrderActions } from "./action";

export interface ActiveOrder {
    id: number;
    origin_place_id: string;
    origin_lat: number;
    origin_lng: number;
    origin_district: string;
    origin_description: string;
    destination_place_id: string;
    destination_lat: number;
    destination_lng: number;
    destination_district: string;
    destination_description: string;
    driver_session_id?: any;
    fare?: any;
    distance?: any;
    pool: boolean;
    pool_deadline?: any;
    hybrid: boolean;
    red_taxi: boolean;
    green_taxi: boolean;
    blue_taxi: boolean;
    cross_harbour_tunnel: boolean;
    eastern_harbour_crossing: boolean;
    western_harbour_crossing: boolean;
    has_pet: boolean;
    smoke_free: boolean;
    has_luggage: boolean;
    is_disabled: boolean;
    urgent: boolean;
    status: string;
    remarks?: any;
    created_at: Date;
    updated_at: Date;
    order_id: number;
    user_id: number;
    headcount: number;
    rating_polite: number;
    rating_clean: number;
    rating_punctual: number;
    rating_driving: number;
    comment?: any;
    name?: any;
    rego?: any;
    username?: string;
}

export interface OrderState {
    isOrderActive: boolean;
    activeOrder: ActiveOrder[];
    activePoolOrder: ActiveOrder[];
    error?: string;
};

const initialState: OrderState = {
    isOrderActive: false,
    activeOrder: [],
    activePoolOrder: [],
};

export function OrderReducer(state: OrderState = initialState, action: OrderActions): OrderState {
    switch (action.type) {
        case '@@order/ORDER_SUCCESS':
            return produce(state, state => {
                state.isOrderActive = true;
            });

        case '@@order/ORDER_FAILED':
            return produce(state, state => {
                state.error = action.error;
            });

        case '@@order/GOT_ACTIVE_ORDER':

            if (action.orders[0].pool_deadline == undefined) {
                return produce(state, state => {
                    state.activeOrder = action.orders;
                    state.isOrderActive = true;
                });
            }

        case '@@order/POOL_LIST':
            return produce(state, state => {
                state.activePoolOrder = action.orders;

            });

        case '@@order/CURRENT_POOL':
            return produce(state, state => {
                state.activePoolOrder = action.orders;
            });

        case '@@order/GOT_ACTIVE_ORDER_FAILED':
            return produce(state, state => {
                state.activeOrder = [];
                state.activePoolOrder = [];
                state.error = action.error;
                state.isOrderActive = false;
            });

        default:
            return state;
    }
};