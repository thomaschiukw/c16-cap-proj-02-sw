import { UserInfoState } from "./reducer";

export function userInfo(info: UserInfoState) {
    return {
        type: '@@user/USER_INFO' as const,
        info
    }
}

export function userInfoFetchFailed(error: string) {
    console.log('error: ', error);
    return {
        type: '@@user/USER_INFO_FETCH_FAILED' as const,
        error
    }
};

export type UserInfoActions = ReturnType<typeof userInfo | typeof userInfoFetchFailed>