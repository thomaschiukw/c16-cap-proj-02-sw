import produce from "immer";
import { UserInfoActions } from "./action";

export interface UserInfoState {
    username: string,
    email: string,
    phone: string,
    error?: string;
}

const initialState: UserInfoState = {
    username: "",
    email: "",
    phone: "",
    error: "",
};

export function userInfoReducer(state: UserInfoState = initialState, action: UserInfoActions): UserInfoState {
    switch (action.type) {
        case '@@user/USER_INFO':
            return produce(state, state => {
                state.username = action.info.username;
                state.email = action.info.email;
                state.phone = `${action.info.phone}`;
            });

        case '@@user/USER_INFO_FETCH_FAILED':
            return produce(state, state => {
                state.error = 'Failed to fetch user info.';
            })

        default:
            return state;
    }
};