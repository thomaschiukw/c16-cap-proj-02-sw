//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import { UserFormInput } from '../../screens/users/settings/UserSettingChange';
import { RootThunkDispatch, RootState } from "../../store";
import { finishLoading, loading } from '../loading/action';
import { userInfo, userInfoFetchFailed } from './action';

export function fetchUserInfo(userID: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            dispatch(loading());

            const res = await fetch(`${REACT_APP_BACKEND_URL}/user/${userID}`);

            if (res.status != 200) {
                dispatch(userInfoFetchFailed('HTTP REQUEST FAILED'));
                return;
            }
            const result = await res.json();

            if (result) {
                dispatch(userInfo(result))
                    ;
            } else {
                dispatch(userInfoFetchFailed(result.error));
            }
            return result;
        } catch (error) {
            console.error(error);
            return;
        } finally {
            dispatch(finishLoading());
        }
    };
};

export function changeUserInfo(formData: UserFormInput) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            dispatch(loading());
            const userID = (getState().auth.user?.id);

            if (userID) {
                const orderForm = {
                    userID: userID,
                    username: formData.name,
                    email: formData.email,
                    phone: formData.phone
                }

                const res = await fetch(`${REACT_APP_BACKEND_URL}/user`, {
                    method: "PUT",
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    body: JSON.stringify(orderForm),
                });

                if (res.status != 200) {
                    dispatch(userInfoFetchFailed('HTTP REQUEST FAILED'));
                    return;
                }

                const result = await res.json();
                if (result.success) {
                    dispatch(userInfo(result));
                } else {
                    dispatch(userInfoFetchFailed(result.error));
                }
            } else {
                dispatch(userInfoFetchFailed('USER ID UNDEFINED'));
            }

        } catch (error) {
            console.error(error);
            return;
        } finally {
            dispatch(finishLoading());
        }
    };
};