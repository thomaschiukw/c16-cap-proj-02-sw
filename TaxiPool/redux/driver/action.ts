import { DriverInfoState } from "./reducer";

export function driverInfo(info: DriverInfoState) {
    return {
        type: '@@driver/DRIVER_INFO' as const,
        info
    }
}

export function driverInfoFetchFailed(error: string) {
    console.log('error: ', error);
    return {
        type: '@@driver/DRIVER_INFO_FETCH_FAILED' as const,
        error
    }
};

export function activeSession(info: DriverInfoState) {
    return {
        type: '@@driver/ACTIVE_SESSION' as const,
        info
    }
};

export function deactiveSession(info: DriverInfoState) {
    return {
        type: '@@driver/DEACTIVE_SESSION' as const,
        info
    }
};

export type DriverInfoActions = ReturnType<typeof driverInfo | typeof driverInfoFetchFailed | typeof activeSession | typeof deactiveSession>