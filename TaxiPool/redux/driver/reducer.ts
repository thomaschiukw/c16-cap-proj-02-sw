import produce from "immer";
import { DriverInfoActions } from "./action";

export interface DriverInfoState {
    name: string,
    email: string,
    phone: string,
    license: string,
    type: string,
    rego: string,
    hybrid?: boolean,
    capacity: string,
    pet_ok?: boolean,
    smoke_free?: boolean,
    active_session?: boolean,
    error?: string;
}

const initialState: DriverInfoState = {
    name: "",
    email: "",
    phone: "",
    license: "",
    type: "",
    rego: "",
    capacity: "",
    error: '',
};

export function driverInfoReducer(state: DriverInfoState = initialState, action: DriverInfoActions): DriverInfoState {
    switch (action.type) {
        case '@@driver/DRIVER_INFO':
            return produce(state, state => {
                state.name = action.info.name;
                state.email = action.info.email;
                state.phone = action.info.phone;
                state.license = action.info.license;
                state.type = action.info.type;
                state.rego = action.info.rego;
                state.hybrid = action.info.hybrid;
                state.capacity = action.info.capacity;
                state.pet_ok = action.info.pet_ok;
                state.smoke_free = action.info.smoke_free;
                state.active_session = action.info.active_session;

            });

        case '@@driver/ACTIVE_SESSION':
            return state;

        case '@@driver/DEACTIVE_SESSION':
            return state;

        case '@@driver/DRIVER_INFO_FETCH_FAILED':
            return produce(state, state => {
                state.error = 'Failed to fetch driver info.';
            })

        default:
            return state;
    }
};