//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import { DeactiveSessionFormInput } from '../../screens/drivers/home/DriverMapScreen';
import { SessionFormInput } from '../../screens/drivers/setting/GoWork';
// import { RegoFormInput } from '../../screens/drivers/setting/ButtonTest';
import { DriverFormInput } from '../../screens/drivers/setting/DriverSettingChange';
import { RootThunkDispatch, RootState } from "../../store";
import { finishLoading, loading } from '../loading/action';
import { activeSession, deactiveSession, driverInfo, driverInfoFetchFailed, } from './action';

export function fetchDriverInfo(userID: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            // dispatch(loading());

            const res = await fetch(`${REACT_APP_BACKEND_URL}/driver/${userID}`);

            if (res.status != 200) {
                dispatch(driverInfoFetchFailed('Status code not 200, not OK'));
                return;
            }
            const result = await res.json();

            if (result) {
                dispatch(driverInfo(result))
                    ;
            } else {
                dispatch(driverInfoFetchFailed(result.error));
            }
            return result;
        } catch (error) {
            console.error(error);
            return;
        } finally {
            // dispatch(finishLoading());
        }
    };
};

export function ChangeDriverInfo(formData: DriverFormInput) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            dispatch(loading());

            const orderForm = {
                name: formData.name,
                email: formData.email,
                phone: formData.phone,
                license: formData.license,
                isRedTaxi: formData.isRedTaxi,
                isGreenTaxi: formData.isGreenTaxi,
                isBlueTaxi: formData.isBlueTaxi,
                rego: formData.rego,
                hybrid: formData.hybrid,
                capacity: formData.capacity,
                pet_ok: formData.pet_ok,
                smoke_free: formData.smoke_free,
                userID: getState().auth.user?.id
            }

            const res = await fetch(`${REACT_APP_BACKEND_URL}/driver/${orderForm.userID}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(orderForm),
            });

            if (res.status != 200) {
                dispatch(driverInfoFetchFailed('Status code not 200, not OK'));
                return;
            }

            const result = await res.json();
            if (result.success) {
                dispatch(driverInfo(result));
            } else {
                dispatch(driverInfoFetchFailed(result.error));
            }
        } catch (error) {
            console.error(error);
            return;
        } finally {
            dispatch(finishLoading());
        }
    };
};


export function ActivateSession(formData: SessionFormInput) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const userID = getState().auth.user?.id;
            // console.log(formData);
            dispatch(loading());
            const res = await fetch(`${REACT_APP_BACKEND_URL}/activate-session/${userID}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(formData),
            });

            if (res.status != 200) {
                dispatch(driverInfoFetchFailed('HTTP REQUEST FAILED'));
            }

            const result = await res.json();

            if (result.success) {
                if (userID) {
                    await dispatch(fetchDriverInfo(userID));
                }
                dispatch(activeSession(result));
            } else {
                dispatch(driverInfoFetchFailed(result.error));
            }
        } catch (error) {
            console.error(error);
            return;
        } finally {
            dispatch(finishLoading());
        }
    };
};

export function DectivateSession(formData: DeactiveSessionFormInput) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const userID = getState().auth.user?.id;
            // console.log(formData);

            dispatch(loading());
            const res = await fetch(`${REACT_APP_BACKEND_URL}/deactivate-session/${userID}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(formData),
            });

            if (res.status != 200) {
                dispatch(driverInfoFetchFailed('HTTP REQUEST FAILED'));
            }

            const result = await res.json();

            if (result.success) {
                if (userID) {
                    await dispatch(fetchDriverInfo(userID));
                }
                dispatch(deactiveSession(result));
            } else {
                dispatch(driverInfoFetchFailed(result.error));
            }
        } catch (error) {
            console.error(error);
            return;
        } finally {
            dispatch(finishLoading());
        }
    };
};