import { SearchingOrder } from "../../screens/drivers/orders/DriverEventStackScreen";
import { Order } from "./reducer";


export function userHistoryOrder(history: Order[]) {
    // console.log('action');
    return {
        type: '@@order/USER_HISTORY_ORDER' as const,
        history
    }
}

export function driverHistoryOrder(history: Order[]) {
    // console.log('action');
    return {
        type: '@@order/DRIVER_HISTORY_ORDER' as const,
        history
    }
}



// export function gotDriverPendingOrder(pendingOrder: Order[]) {
//     // console.log('action');
//     return {
//         type: '@@order/PENDING_ORDER' as const,
//         pendingOrder
//     }
// }




// export function gotOrder(orders: Order[]) {
//     return {
//         type: '@@driver_order/GOT_ORDER' as const,
//         orders
//     }
// }

export function gotSearchingOrder(searchingOrders: SearchingOrder[]) {
    // console.log('action');
    return {
        type: '@@driver_order/GOT_SEARCHING_ORDER' as const,
        searchingOrders
    }
}

export function searchingOrderAccepted() {
    return {
        type: '@@driver_order/ORDER_ACCEPTED' as const,
    }
}

export function gotDriverActiveOrder(activeOrder: Order[]) {
    return {
        type: '@@driver_order/GOT_ACTIVE_ORDER' as const,
        activeOrder
    }
}


type FAILED_INTENT =
    | '@@driver_order/GET_SEARCHING_ORDER_FAILED'
    | '@@driver_order/ACCEPT_ORDER_FAILED'
    | '@@driver_order/GET_ACTIVE_ORDER_FAILED'

export function failed(type: FAILED_INTENT, error: string | unknown) {
    return {
        type,
        error
    }
};

export type ViewOrderActions = ReturnType<typeof userHistoryOrder
    | typeof driverHistoryOrder
    | typeof searchingOrderAccepted
    | typeof gotSearchingOrder
    | typeof gotDriverActiveOrder
    | typeof failed>
