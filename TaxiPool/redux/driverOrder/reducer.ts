import produce from "immer";
import { SearchingOrder } from "../../screens/drivers/orders/DriverEventStackScreen";
import { ViewOrderActions } from "./action";

export interface Order {
    id: number;
    origin_place_id: string;
    origin_lat: number;
    origin_lng: number;
    origin_district: string;
    origin_description: string;
    destination_place_id: string;
    destination_lat: number;
    destination_lng: number;
    destination_district: string;
    destination_description: string;
    driver_session_id?: any;
    fare: string;
    distance: string;
    pool: boolean;
    pool_deadline?: any;
    hybrid: boolean;
    red_taxi: boolean;
    green_taxi: boolean;
    blue_taxi: boolean;
    cross_harbour_tunnel: boolean;
    eastern_harbour_crossing: boolean;
    western_harbour_crossing: boolean;
    has_pet: boolean;
    smoke_free: boolean;
    has_luggage: boolean;
    is_disabled: boolean;
    urgent: boolean;
    status: string;
    remarks?: any;
    created_at: Date;
    updated_at: Date;

}

export interface DriverOrderState {
    activeOrder: Order[]
    searchingOrders: SearchingOrder[]
    orders: Order[];
    history: Order[];
    error: string | unknown
}

const initialState: DriverOrderState = {
    activeOrder: [],
    searchingOrders: [],
    orders: [],
    history: [],
    error: ""
};

export function driverOrderReducer(state: DriverOrderState = initialState, action: ViewOrderActions): DriverOrderState {
    switch (action.type) {
        case '@@order/USER_HISTORY_ORDER':
            return produce(state, state => {
                state.history = action.history;
                state.error = "";
            });

        case '@@order/DRIVER_HISTORY_ORDER':
            return produce(state, state => {
                state.history = action.history;
                state.error = "";
            });

        // case '@@order/DRIVER_VIEW_SEARCHING_ORDER':
        //     return produce(state, state => {
        //         state.orders = action.orders;
        //         state.error = "";
        //     });

        case '@@driver_order/GOT_SEARCHING_ORDER':
            return produce(state, state => {
                state.searchingOrders = action.searchingOrders;
                state.error = "";
            });


        // case '@@order/DRIVER_VIEW_SEARCHING_ORDER_FAILED':
        //     return produce(state, state => {
        //         state.orders = [];
        //         state.error = action.error;
        //     });

        case '@@driver_order/ORDER_ACCEPTED':
            return state;

        case '@@driver_order/ACCEPT_ORDER_FAILED':
            return produce(state, state => {
                state.error = action.error;
            });

        //-------

        // case '@@driver_order/GOT_ORDER':
        //     return produce(state, state => {
        //         state.orders = action.orders
        //     });

        // case '@@driver_order/GET_ORDER_FAILED':
        //     return produce(state, state => {
        //         state.error = action.error;
        //     });

        case '@@driver_order/GOT_ACTIVE_ORDER':
            return produce(state, state => {
                state.activeOrder = action.activeOrder;
            });

        case '@@driver_order/GET_ACTIVE_ORDER_FAILED':
            return produce(state, state => {
                state.error = action.error;
            });

        default:
            return state;
    }
};