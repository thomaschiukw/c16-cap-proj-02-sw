//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import { RootThunkDispatch, RootState } from "../../store";
// import { finishLoading, loading } from '../loading/action';
import { failed, driverHistoryOrder, userHistoryOrder, gotDriverActiveOrder, searchingOrderAccepted, gotSearchingOrder } from './action';

// export function fetchOrderById(id: number) {
//     return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
//         try {
//             const res = await fetch(`${REACT_APP_BACKEND_URL}/order/:${id}`);

//             const result = await res.json();
//             if (result.length !== 0) {
//                 dispatch(gotOrder(result));
//             } else {
//                 dispatch(failed('@@driver_order/GET_ORDER_FAILED', "NO ORDER"));
//             }
//         } catch (error) {
//             console.error(error);
//             return;
//         }
//     };
// };

export function fetchSearchingOrder() {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            // const driverInfosRes = await fetch(`${REACT_APP_BACKEND_URL}/user/driver/${userid}/session-details`);
            // let driverInfosResult = await driverInfosRes.json();
            // if (driverInfosResult) {
            const userInfo = getState().driver;
            const res = await fetch(`${REACT_APP_BACKEND_URL}/order/searching/type/${userInfo.type}/capacity/${userInfo.capacity}/allowPet/${userInfo.pet_ok?.toString()}/disallowSmoke/${userInfo.smoke_free?.toString()}`);
            const result = await res.json();
            if (!result.success) {
                dispatch(gotSearchingOrder(result));
            } else {
                dispatch(failed("@@driver_order/GET_SEARCHING_ORDER_FAILED", result.msg));
            }
            // } else {
            //     dispatch(failed("@@order/DRIVER_VIEW_SEARCHING_ORDER_FAILED", "Failed to find result in driver sessions"));
            // }

        } catch (error) {
            console.error(error);
            return;
        }

    };
};

// export function fetchDriverPendingOrder(driverid: number) {
//     return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
//         try {
//             const res = await fetch(`${REACT_APP_BACKEND_URL}/order/pending/driver/${driverid}`);
//             const result = await res.json();
//             if (result) {
//                 dispatch(gotDriverPendingOrder(result));
//             }
//         } catch (error) {
//             console.error(error);
//             return;
//         }
//     };
// };

export function fetchDriverActiveOrder(driverid: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${REACT_APP_BACKEND_URL}/order/active/driver/${driverid}`);
            const result = await res.json();
            if (result.success === false) {
                dispatch(failed('@@driver_order/GET_ACTIVE_ORDER_FAILED', result.msg));
            } else {
                dispatch(gotDriverActiveOrder(result));
            }
        } catch (error) {
            console.error(error);
            dispatch(failed('@@driver_order/GET_ACTIVE_ORDER_FAILED', error));
            return;
        }
    };
};

export function fetchAcceptOrder(driverId: number, orderId: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            // const orderStatusRes = await fetch(`${REACT_APP_BACKEND_URL}/order/${orderId}`);
            // const orderStatusResult = await orderStatusRes.json();

            // if (orderStatusResult[0].status === "searching") {
            const res = await fetch(`${REACT_APP_BACKEND_URL}/order/accept/driver/${driverId}/order/${orderId}`, {
                method: "PUT",
            });
            const result = await res.json();
            // console.log(result);
            if (result.msg) {
                dispatch(failed('@@driver_order/ACCEPT_ORDER_FAILED', result.msg));
            } else {
                await dispatch(searchingOrderAccepted())
                await dispatch(fetchDriverActiveOrder(driverId))
            }

            // } else {
            //     console.log('order taken')
            // }

        } catch (error) {
            console.error(error);
            return;
        }
    };
};

export function fetchDriverStartOrder() {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const orderId = getState().driverOrder.activeOrder[0].id;
            const res = await fetch(`${REACT_APP_BACKEND_URL}/order/start/order/${orderId}`, {
                method: "PUT",
            });
            const result = await res.json();
            // console.log(result);

        } catch (error) {
            console.error(error);
            return;
        }
    };

}

export function fetchDriverCompleteOrder() {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const orderId = getState().driverOrder.activeOrder[0].id;
            const res = await fetch(`${REACT_APP_BACKEND_URL}/order/complete/order/${orderId}`, {
                method: "PUT",
            });
            const result = await res.json();
            // console.log(result);

        } catch (error) {
            console.error(error);
            return;
        }
    };

}

export function fetchUserHistoryOrder(id: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            const res = await fetch(`${REACT_APP_BACKEND_URL}/order/completed/user/${id}`);
            const result = await res.json();
            dispatch(userHistoryOrder(result));
        } catch (error) {
            console.error(error);
            return;
        }
    };
};

export function fetchDriverHistoryOrder(id: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {


            const res = await fetch(`${REACT_APP_BACKEND_URL}/order/completed/driver/${id}`);

            const result = await res.json();


            dispatch(driverHistoryOrder(result));

        } catch (error) {
            console.error(error);
            return;
        }
    };
};