//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import { OrderFormInput } from '../../screens/users/booking/OrderSettingsScreen';
import { RootThunkDispatch, RootState } from "../../store";
import { finishLoading, loading } from '../loading/action';
import { PlaceAutocompletePrediction } from '../map/reducer';
import { poolOrder } from './action';


export function fetchPoolOrder(userId: number) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            // dispatch(loading());
            // console.log('value =', value)
            const res = await fetch(`${REACT_APP_BACKEND_URL}/pool-order/${userId}`);
            const result = await res.json();
            dispatch(poolOrder(result))

        } catch (error) {
            console.error(error);
            return;
        } finally {
            dispatch(finishLoading());
        }
    };
}