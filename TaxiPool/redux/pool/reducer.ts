import produce from "immer";
// import { PlaceAutocompletePrediction } from "../map/reducer";
import { PoolActions } from "./action";



export interface PoolOrderState {
    orders: any
};

const initialState: PoolOrderState = {
    orders: []
};

export function PoolOrderReducer(state: PoolOrderState = initialState, action: PoolActions): PoolOrderState {
    switch (action.type) {
        case '@@pool/POOL_ORDER':
            // console.log('order success');
            return produce(state, state => {
                state.orders = action.orders;
            })
        default:
            return state;
    }
};