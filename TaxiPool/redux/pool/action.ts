// import { PlaceAutocompletePrediction } from "../map/reducer";
// import { LatLng } from "./reducer";

export function poolOrder(orders: any) {
    return {
        type: '@@pool/POOL_ORDER' as const,
        orders
    }
};



export type PoolActions =
    ReturnType<typeof poolOrder>