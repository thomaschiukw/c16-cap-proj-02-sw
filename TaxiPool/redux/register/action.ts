export function registerSuccess() {
    return {
        type: '@@register/REGISTER_SUCCESS' as const,
    }
};

export function registerFailed(error: string) {
    console.log('error: ', error);
    return {
        type: '@@register/REGISTER_FAILED' as const,
        error
    }
};

export type RegisterActions = ReturnType<typeof registerSuccess | typeof registerFailed>;