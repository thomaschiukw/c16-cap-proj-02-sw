import produce from "immer";
import { RegisterActions } from "./action";

export interface RegisterState {
    isRegistered: boolean,
    error: string
};

const initialState: RegisterState = {
    isRegistered: false,
    error: ""
};

export function registerReducer(state: RegisterState = initialState, action: RegisterActions): RegisterState {
    switch (action.type) {
        case '@@register/REGISTER_SUCCESS':
            return produce(state, state => {
                state.isRegistered = true;
                state.error = "";
            })

        case '@@register/REGISTER_FAILED':
            return produce(state, state => {
                state.isRegistered = false;
                state.error = action.error;
            })

        default:
            return state;
    }
};