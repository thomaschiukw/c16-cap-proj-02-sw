import { MapActions } from "./actions";
import produce from "immer";

export interface PlaceDetailsResult {
    error: any;
    status: string;
    result: {
        geometry: {
            location: LatLng;
        };
        address_components:
        {
            long_name: string;
            short_name: string;
            types: string[];
        }[]
    };
}

export interface PlaceDetails {
    html_attributions: []
    results: PlaceDetailsResult
    status: string
}

export interface PlaceAutocompletePrediction {
    description?: string;
    matched_substrings?: any[];
    place_id?: string;
    reference?: string;
    structured_formatting?: {};
    terms?: any[];
    types?: any[];
}

export interface PlaceAutocomplete {
    predictions: PlaceAutocompletePrediction[]
    status: string
}

export interface LatLng {
    lat: number;
    lng: number;
}

export interface SelectedPlace {
    details?: PlaceAutocompletePrediction;
    administrative_area?: string;
    coordinates?: LatLng;
    status?: string;
    error?: string
}
export interface RouteDetails {
    distance?: number
    duration?: number
    coordinates?: []
}

export interface MapState {
    originPredictions: PlaceAutocomplete;
    destinationPredictions: PlaceAutocomplete;
    selectedOrigin: SelectedPlace;
    selectedDestination: SelectedPlace;
    route: RouteDetails;
    error: string;
};

const initialState: MapState = {
    originPredictions: {
        predictions: [],
        status: ""
    },
    destinationPredictions: {
        predictions: [],
        status: ""
    },
    selectedOrigin: {},
    selectedDestination: {},
    route: {},
    error: ""
};

export function mapReducer(state: MapState = initialState, action: MapActions): MapState {
    switch (action.type) {
        case '@@map/FETCH_ORIGIN_SUCCESS':
            return produce(state, state => {
                state.originPredictions.predictions = action.predictions;
                state.originPredictions.status = action.status;
            });

        case '@@map/FETCH_ORIGIN_FAILED':
            return produce(state, state => {
                state.originPredictions.predictions = [];
                state.originPredictions.status = action.error;
            });

        case '@@map/FETCH_DESTINATION_SUCCESS':
            return produce(state, state => {
                state.destinationPredictions.predictions = action.predictions;
                state.destinationPredictions.status = action.status;
            });

        case '@@map/FETCH_DESTINATION_FAILED':
            return produce(state, state => {
                state.destinationPredictions.predictions = [];
                state.destinationPredictions.status = action.error;
            });

        case '@@map/CLEAR_SEARCH_INPUT':
            return produce(state, state => {
                state.originPredictions = {
                    predictions: [],
                    status: ""
                };
                state.destinationPredictions = {
                    predictions: [],
                    status: ""
                };
            });

        case '@@map/ORIGIN_SELECTED':
            if (action.detailsByPlaceId.result.address_components[(action.detailsByPlaceId.result.address_components).length - 2]?.short_name !== undefined) {
                return produce(state, state => {
                    state.selectedOrigin.details = action.detailsByDescription;
                    state.selectedOrigin.administrative_area = action.detailsByPlaceId.result.address_components[(action.detailsByPlaceId.result.address_components).length - 2].short_name;
                    state.selectedOrigin.coordinates = action.detailsByPlaceId.result.geometry.location;
                });
            } else {
                return produce(state, state => {
                    state.selectedOrigin.details = action.detailsByDescription;
                    state.selectedOrigin.administrative_area = action.detailsByPlaceId.result.address_components[0].short_name;
                    state.selectedOrigin.coordinates = action.detailsByPlaceId.result.geometry.location;
                });
            }

        case '@@map/DESTINATION_SELECTED':
            if (action.detailsByPlaceId.result.address_components[(action.detailsByPlaceId.result.address_components).length - 2]?.short_name !== undefined) {
                return produce(state, state => {
                    state.selectedDestination.details = action.detailsByDescription;
                    state.selectedDestination.administrative_area = action.detailsByPlaceId.result.address_components[(action.detailsByPlaceId.result.address_components).length - 2].short_name;
                    state.selectedDestination.coordinates = action.detailsByPlaceId.result.geometry.location;
                });
            } else {
                return produce(state, state => {
                    state.selectedDestination.details = action.detailsByDescription;
                    state.selectedDestination.administrative_area = action.detailsByPlaceId.result.address_components[0].short_name;
                    state.selectedDestination.coordinates = action.detailsByPlaceId.result.geometry.location;
                });
            }


        case '@@map/ORIGIN_CLEARED':
            return produce(state, state => {
                state.selectedOrigin.details = undefined;
                state.selectedOrigin.administrative_area = undefined;
                state.selectedOrigin.coordinates = undefined;
                state.selectedOrigin.status = action.status;
                state.selectedOrigin.error = action.error;
            });

        case '@@map/DESTINATION_CLEARED':
            return produce(state, state => {
                state.selectedDestination.details = undefined;
                state.selectedDestination.administrative_area = undefined;
                state.selectedDestination.coordinates = undefined;
                state.selectedDestination.status = action.status;
                state.selectedDestination.error = action.error;
            });

        case '@@map/ROUTE_DETAILS_CALCULATED':
            return produce(state, state => {
                state.route.distance = action.distance;
                state.route.duration = action.duration;
                state.route.coordinates = action.coordinates;
            })
        default:
            return state;
    }
};