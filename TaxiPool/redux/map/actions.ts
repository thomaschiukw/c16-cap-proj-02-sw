import { PlaceAutocompletePrediction, PlaceDetails, PlaceDetailsResult } from "./reducer"

export function fetchOriginSuccess(predictions: PlaceAutocompletePrediction[], status: string) {
    return {
        type: '@@map/FETCH_ORIGIN_SUCCESS' as const,
        predictions,
        status
    }
};

export function fetchDestinationSuccess(predictions: PlaceAutocompletePrediction[], status: string) {
    return {
        type: '@@map/FETCH_DESTINATION_SUCCESS' as const,
        predictions,
        status
    }
};

export function clearSearchInput() {
    return {
        type: '@@map/CLEAR_SEARCH_INPUT' as const,
    }
};

export function originSelected(detailsByDescription: PlaceAutocompletePrediction, detailsByPlaceId: PlaceDetailsResult) {
    // console.log(detailsByPlaceId)
    return {
        type: '@@map/ORIGIN_SELECTED' as const,
        detailsByDescription,
        detailsByPlaceId
    }
};

export function originCleared(status: string, error?: any) {
    return {
        type: '@@map/ORIGIN_CLEARED' as const,
        status,
        error
    }
};

export function destinationSelected(detailsByDescription: PlaceAutocompletePrediction, detailsByPlaceId: PlaceDetailsResult) {
    return {
        type: '@@map/DESTINATION_SELECTED' as const,
        detailsByDescription,
        detailsByPlaceId
    }
};

export function destinationCleared(status: string, error?: any) {
    return {
        type: '@@map/DESTINATION_CLEARED' as const,
        status,
        error
    }
};

export function routeDetailsCalculated(distance: number, duration: number, coordinates: []) {
    return {
        type: '@@map/ROUTE_DETAILS_CALCULATED' as const,
        distance,
        duration,
        coordinates
    }
};

type FAILED_INTENT =
    | "@@map/FETCH_ORIGIN_FAILED"
    | '@@map/FETCH_DESTINATION_FAILED'

export function mapFailed(type: FAILED_INTENT, error: string) {
    return {
        type,
        error
    }
}

export type MapActions =
    ReturnType<typeof fetchOriginSuccess
        | typeof fetchDestinationSuccess
        | typeof clearSearchInput
        | typeof originSelected
        | typeof originCleared
        | typeof destinationSelected
        | typeof destinationCleared
        | typeof routeDetailsCalculated
        | typeof mapFailed
    >