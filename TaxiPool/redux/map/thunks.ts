//@ts-expect-error
import { REACT_APP_BACKEND_URL } from '@env';
import { RootThunkDispatch, RootState } from "../../store";
import { finishLoading, loading } from '../loading/action';
import { destinationCleared, destinationSelected, fetchDestinationSuccess, fetchOriginSuccess, mapFailed, originCleared, originSelected } from './actions';
import { PlaceAutocompletePrediction } from './reducer';

export function fetchOrigin(value: string | undefined) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            // dispatch(loading());
            // console.log('value =', value)
            if (!value) {
                dispatch(mapFailed("@@map/FETCH_ORIGIN_FAILED", "INPUT_UNDEFINED"));
            } else {
                const res = await fetch(`${REACT_APP_BACKEND_URL}/autocomplete/input/${value}`);
                if (res.status != 200) {
                    dispatch(mapFailed("@@map/FETCH_ORIGIN_FAILED", "REQUEST_FAILED"));
                }

                const result = await res.json();
                // console.log(result)
                if (result.error) {
                    dispatch(mapFailed("@@map/FETCH_ORIGIN_FAILED", "INTERNAL_SERVER_ERROR"));
                }

                if (result.status === "OK") {
                    dispatch(fetchOriginSuccess(result.predictions, result.status));
                } else {
                    dispatch(mapFailed("@@map/FETCH_ORIGIN_FAILED", result.status));
                }
            }
        } catch (error) {
            console.error(error);
            return;
        } finally {
            // dispatch(finishLoading());
        }
    };
};

export function fetchDestination(value: string | undefined) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            // dispatch(loading());

            if (!value) {
                dispatch(mapFailed("@@map/FETCH_DESTINATION_FAILED", "INPUT_UNDEFINED"));
            } else {
                const res = await fetch(`${REACT_APP_BACKEND_URL}/autocomplete/input/${value}`);
                if (res.status != 200) {
                    dispatch(mapFailed("@@map/FETCH_DESTINATION_FAILED", "REQUEST_FAILED"));
                }

                const result = await res.json();

                if (result.error) {
                    dispatch(mapFailed("@@map/FETCH_DESTINATION_FAILED", "INTERNAL_SERVER_ERROR"));
                }

                if (result.status === "OK") {
                    dispatch(fetchDestinationSuccess(result.predictions, result.status));
                } else {
                    dispatch(mapFailed("@@map/FETCH_DESTINATION_FAILED", result.status));
                }
            }
        } catch (error) {
            console.error(error);
            return;
        } finally {
            // dispatch(finishLoading());
        }
    };
};

export function setOrigin(details: PlaceAutocompletePrediction) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            dispatch(loading());
            // console.log('value =', value)
            if (!details) {
                dispatch(originCleared("INVALID_SELECTION"));
            } else {
                const placeId = details.place_id;
                const res = await fetch(`${REACT_APP_BACKEND_URL}/placeid/input/${placeId}`);
                if (res.status != 200) {
                    dispatch(originCleared("HTTP_REQUEST_FAILED"));
                }

                const result = await res.json();
                // console.log(result)
                if (result.error) {
                    dispatch(originCleared(result.status, result.error));
                }

                if (result.status === "OK") {
                    dispatch(originSelected(details, result));
                } else {
                    dispatch(originCleared(result.status, result.error));
                }
            }
        } catch (error) {
            console.error(error);
            return;
        } finally {
            dispatch(finishLoading());
        }
    };
};

export function setDestination(details: PlaceAutocompletePrediction) {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            dispatch(loading());
            // console.log('value =', details)
            if (!details) {
                dispatch(destinationCleared("INVALID_SELECTION"));
            } else {
                const placeId = details.place_id;
                const res = await fetch(`${REACT_APP_BACKEND_URL}/placeid/input/${placeId}`);
                if (res.status != 200) {
                    dispatch(destinationCleared("HTTP_REQUEST_FAILED"));
                }

                const result = await res.json();
                if (result.error) {
                    dispatch(destinationCleared(result.status, result.error));
                }

                if (result.status === "OK") {
                    dispatch(destinationSelected(details, result));
                } else {
                    dispatch(destinationCleared(result.status, result.error));
                }
            }
        } catch (error) {
            console.error(error);
            return;
        } finally {
            dispatch(finishLoading());
        }
    };
};