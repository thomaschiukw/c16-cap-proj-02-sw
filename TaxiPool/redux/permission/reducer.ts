import produce from "immer";
import { PermissionStatus } from "react-native-permissions";
import { PermissionActions } from "./action";

export interface PermissionState {
    permissionStatus: PermissionStatus | string;
};

const initialState = {
    permissionStatus: ""
};

export function permissionReducer(state: PermissionState = initialState, action: PermissionActions): PermissionState {
    switch (action.type) {
        case '@@permission/UPDATED_PERMISSION_STATUS':
            return produce(state, state => {
                state.permissionStatus = action.permissionStatus;
            });

        default:
            return state;
    }
};