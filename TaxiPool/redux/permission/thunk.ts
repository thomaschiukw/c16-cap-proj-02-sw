import { RootThunkDispatch, RootState } from "../../store";
import { finishLoading, loading } from '../loading/action';
import { Platform } from 'react-native';
import RNPermissions, { check, checkNotifications, NotificationsResponse, Permission, PERMISSIONS, PermissionStatus, request, requestNotifications, RESULTS, } from 'react-native-permissions';
import { permissionStatusUpdated } from './action';

const PLATFORM_PERMISSIONS = Platform.select<
    typeof PERMISSIONS.ANDROID | typeof PERMISSIONS.IOS | typeof PERMISSIONS.WINDOWS | {}
>({
    android: PERMISSIONS.ANDROID,
    ios: PERMISSIONS.IOS,
    windows: PERMISSIONS.WINDOWS,
    default: {},
});

const PERMISSIONS_VALUES: Permission[] = Object.values(PLATFORM_PERMISSIONS);

export function permissionStatusCheck() {
    return async (dispatch: RootThunkDispatch, getState: () => RootState) => {
        try {
            let status;
            const hasNoti = await checkNotifications();
            // console.log("hasNotification", hasNoti);
            if (hasNoti.status !== RESULTS.GRANTED && Platform.OS === 'ios') {
                const requestNoti = await requestNotifications(['alert', 'sound']);
                // console.log("requestNoti", requestNoti);
            }

            let hasLocationPermission;
            if (Platform.OS === 'ios') {
                hasLocationPermission = await check(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
            } else if (Platform.OS === 'android') {
                hasLocationPermission = await check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
            }
            // console.log('hasLocationPermission =', hasLocationPermission);

            switch (hasLocationPermission) {
                case RESULTS.UNAVAILABLE:
                    status = RESULTS.UNAVAILABLE;
                    // console.log('This feature is not available (on this device / in this context)');
                    break;
                case RESULTS.DENIED:
                    status = RESULTS.DENIED;
                    // console.log('The permission has not been requested / is denied but requestable');
                    break;
                case RESULTS.LIMITED:
                    status = RESULTS.LIMITED;
                    // console.log('The permission is limited: some actions are possible');
                    break;
                case RESULTS.GRANTED:
                    status = RESULTS.GRANTED;
                    // console.log('The permission is granted');
                    break;
                case RESULTS.BLOCKED:
                    status = RESULTS.BLOCKED;
                    // console.log('The permission is denied and not requestable anymore');
                    break;
            }

            if (hasLocationPermission !== RESULTS.GRANTED) {
                let requestLocationPermission;
                if (Platform.OS === 'ios') {
                    requestLocationPermission = await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
                } else if (Platform.OS === 'android') {
                    requestLocationPermission = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
                }

                if (requestLocationPermission === RESULTS.GRANTED) {
                    dispatch(permissionStatusUpdated(RESULTS.GRANTED));
                } else {
                    dispatch(permissionStatusUpdated(status || 'FAILED'));
                }
                // console.log("requestLocationPermission =", requestLocationPermission);
            } else {
                dispatch(permissionStatusUpdated(RESULTS.GRANTED));
            }
        } catch (error) {
            console.error(error);
            return;
        } finally {
            dispatch(finishLoading());
        }
    };
}