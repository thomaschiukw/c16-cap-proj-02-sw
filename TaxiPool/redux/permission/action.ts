import { PermissionStatus } from "react-native-permissions"

export function permissionStatusUpdated(permissionStatus: PermissionStatus | string) {
    return {
        type: '@@permission/UPDATED_PERMISSION_STATUS' as const,
        permissionStatus
    }
};

export type PermissionActions = ReturnType<typeof permissionStatusUpdated>