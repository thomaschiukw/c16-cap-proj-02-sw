import produce from "immer";
import { LoadingActions } from "./action";

export interface LoadingState {
    isLoading: number;
};

const initialState = {
    isLoading: 0
};

export function loadingReducer(state: LoadingState = initialState, action: LoadingActions): LoadingState {
    switch (action.type) {
        case '@@loading/LOADING':
            return produce(state, state => {
                state.isLoading = 1;
            });

        case '@@loading/LOADING_COMPLETED':
            return produce(state, state => {
                state.isLoading = -1;
            });

        default:
            return state;
    }
};