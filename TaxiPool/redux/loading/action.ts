export function loading() {
    return {
        type: '@@loading/LOADING' as const
    }
};

export function finishLoading() {
    return {
        type: '@@loading/LOADING_COMPLETED' as const
    }
};

export type LoadingActions = ReturnType<typeof loading | typeof finishLoading>;